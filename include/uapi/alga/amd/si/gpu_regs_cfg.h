#ifndef ALGA_AMD_SI_GPU_REGS_CFG_H
#define ALGA_AMD_SI_GPU_REGS_CFG_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#ifndef __KERNEL__
#ifndef BIT
#define BIT(x) (1 << (x))
#endif
#endif

/* start of configuration register area: 0x8000-0xb000------------------------*/
#define	VGT_CACHE_INVALIDATION			0x88c4
#define		VCI_CACHE_INVALIDATION			0x00000003
#define			VCI_VC_ONLY				0
#define			VCI_TC_ONLY				1
#define			VCI_VC_AND_TC				2
#define		VCI_AUTO_INVLD_ENA			0x000000c0
#define			VCI_NO_AUTO				0
#define			VCI_ES_AUTO				1
#define			VCI_GS_AUTO				2
#define			VCI_ES_AND_GS_AUTO			3

#define	VGT_GS_VTX_REUSE			0x88d4
#define		VGVR_VGT_GS_VTX_REUSE			0x0000001f

#define VGT_PRIM_TYPE				0x8958
#define		VPT_PRIM_TYPE				0x0000003f
#define			VPT_NONE				0x00
#define			VPT_POINTLIST				0x01
#define			VPT_LINELIST				0x02
#define			VPT_LINESTRIP				0x03
#define			VPT_TRILIST				0x04
#define			VPT_TRIFAN				0x05
#define			VPT_TRISTRIP				0x06
#define			VPT_UNUSED_0				0x07
#define			VPT_UNUSED_1				0x08
#define			VPT_PATCH				0x09
#define			VPT_LINELIST_ADJ			0x0a
#define			VPT_LINESTRIP_ADJ			0x0b
#define			VPT_TRILIST_ADJ				0x0c
#define			VPT_TRISTRIP_ADJ			0x0d
#define			VPT_UNUSED_3				0x0e
#define			VPT_UNUSED_4				0x0f
#define			VPT_TRI_WITH_WFLAGS			0x10
#define			VPT_RECTLIST				0x11
#define			VPT_LINELOOP				0x12
#define			VPT_QUADLIST				0x13
#define			VPT_QUADSTRIP				0x14
#define			VPT_POLYGON				0x15
#define			VPT_2D_COPY_RECT_LIST_V0		0x16
#define			VPT_2D_COPY_RECT_LIST_V1		0x17
#define			VPT_2D_COPY_RECT_LIST_V2		0x18
#define			VPT_2D_COPY_RECT_LIST_V3		0x19
#define			VPT_2D_FILL_RECT_LIST			0x1a
#define			VPT_2D_LINE_STRIP			0x1b
#define			VPT_2D_TRI_STRIP			0x1c

#define	VGT_INSTS_N				0x8974

#define CC_GC_SHADER_ARRAY_CFG			0x89bc
#define		CGSAC_SH_CUS_N_MAX			16
#define		CGSAC_INACTIVE_CUS_VALID		BIT(0)
#define		CGSAC_INACTIVE_CUS			0xffff0000
#define GC_USER_SHADER_ARRAY_CFG		0x89c0

#define	PA_CL_ENHANCE				0x8a14
#define		PCE_CLIP_VTX_REORDER_ENA		BIT(0)
#define		PCE_CLIP_SEQ_N				0x00000006
#define		PCE_CLIPPED_PRIM_SEQ_STALL		BIT(3)
#define		PCE_VE_NAN_PROC_DIS			BIT(4)

#define	PA_SC_LINE_STIPPLE_STATE		0x8b10
#define		PSLSS_CUR_PTR 				0x0000000f
#define		PSLSS_CUR_CNT				0x0000ff00

#define	PA_SC_FORCE_EOV_MAX_CNTS		0x8b24
#define		PSFEMC_FORCE_EOV_MAX_CLK_CNT		0x0000ffff
#define		PSFEMC_FORCE_EOV_MAX_REZ_CNT		0xffff0000

#define	PA_SC_FIFO_SZ				0x8bcc
#define		PSFS_SC_FRONTEND_PRIM_FIFO_SZ		0xffffffff
#define		PSFS_SC_BACKEND_PRIM_FIFO_SZ		0xffffffc0
#define		PSFS_SC_HIZ_TILE_FIFO_SZ		0xffff8000
#define		PSFS_SC_EARLYZ_TILE_FIFO_SZ		0xff800000

#define	SQ_CFG					0x8c00

#define	SX_DEBUG_1				0x9060

#define	SPI_STATIC_THD_MGMT_2			0x90e8

#define SPI_CFG_CTL_0				0x9100
#define		SCC_GPR_WR_PRIORITY			0x001fffff
#define		SCC_EXP_PRIORITY_ORDER			0x00e00000
#define		SCC_ENA_SQG_TOP_EVENTS			BIT(24)
#define		SCC_EN_SQG_BOP_EVENTS			BIT(25)
#define		SCC_RSRC_MGMT_RESET			BIT(26)

#define	SPI_CFG_CTL_1				0x913c
#define		SCC_VTX_DONE_DELAY			0x0000000f
#define			SCC_DELAY_14_CLKS		0x0
#define			SCC_DELAY_16_CLKS		0x1
#define			SCC_DELAY_18_CLKS		0x2
#define			SCC_DELAY_20_CLKS		0x3
#define			SCC_DELAY_22_CLKS		0x4
#define			SCC_DELAY_24_CLKS		0x5
#define			SCC_DELAY_26_CLKS		0x6
#define			SCC_DELAY_28_CLKS		0x7
#define			SCC_DELAY_30_CLKS		0x8
#define			SCC_DELAY_32_CLKS		0x9
#define			SCC_DELAY_34_CLKS		0xa
#define			SCC_DELAY_4_CLKS		0xb
#define			SCC_DELAY_6_CLKS		0xc
#define			SCC_DELAY_8_CLKS		0xd
#define			SCC_DELAY_10_CLKS		0xe
#define			SCC_DELAY_12_CLKS		0xf
#define		SCC_INTERP_ONE_PRIM_PER_ROW	BIT(4)
#define		SCC_PC_LIMIT_ENA		BIT(6)
#define		SCC_PC_LIMIT_STRICT		BIT(7)
#define		SCC_PC_LIMIT_SZ			0xffff0000

#define	CGTS_SM_CTL				0x9150
#define		CSC_OVERRIDE				BIT(21)
#define		CSC_LS_OVERRIDE				BIT(22)

#define	SPI_LB_CU_MASK				0x9354

#define TA_CTL_AUX				0x9508

/* related to GC_USER_RB_BACKEND_DIS */
#define CC_RB_BACKEND_DIS			0x98f4
#define		CRBD_BACKEND_DIS_VALID			BIT(0)
#define		CRBD_TAHITI_RB_BITMAP_W_PER_SH		2
#define		CRBD_BACKEND_DIS			0x00ff0000
#define GB_ADDR_CFG				0x98f8
#define		GAC_PIPES_N				0x00000007
#define		GAC_PIPE_INTERLEAVE_SZ			0x00000070
#define		GAC_SES_N				0x00003000
#define		GAC_SE_TILE_SZ				0x00070000
#define		GAC_GPUS_N				0x00700000
#define		GAC_MULTI_GPU_TILE_SZ			0x03000000
#define		GAC_ROW_SZ				0x30000000

#define CB_PERF_CTR_0_SEL_0			0x9a20
#define CB_PERF_CTR_0_SEL_1			0x9a24
#define CB_PERF_CTR_1_SEL_0			0x9a28
#define CB_PERF_CTR_1_SEL_1			0x9a2c
#define CB_PERF_CTR_2_SEL_0			0x9a30
#define CB_PERF_CTR_2_SEL_1			0x9a34
#define CB_PERF_CTR_3_SEL_0			0x9a38
#define CB_PERF_CTR_3_SEL_1			0x9a3c

#define	CB_CGTT_SCLK_CTL			0x9a60

/* related to CC_RB_BACKEND_DIS */
#define	GC_USER_RB_BACKEND_DIS			0x9b7c
#define		GURBD_BACKEND_DIS			0x00ff0000

#define TCP_CHAN_STEER_LO			0xac0c
#define TCP_CHAN_STEER_HI			0xac10
/* end of configuration register area: 0x8000-0xb000--------------------------*/
#endif
