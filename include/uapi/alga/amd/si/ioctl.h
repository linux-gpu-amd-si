#ifndef ALGA_AMD_SI_IOCTL_H
#define ALGA_AMD_SI_IOCTL_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define SI_NOP				0
#define SI_CONTEXT_LOST_ACK		1/* for instance, back from suspend */

#define SI_DCE_DP_SET			2
#define SI_DCE_DP_DPM			3
#define SI_DCE_PF			4
#define SI_DCE_EDID			5
#define SI_MEM_ALLOC			6
#define SI_MEM_FREE			7
#define SI_DMA				8
#define SI_CPU_ADDR_TO_GPU_ADDR		9
#define SI_GPU_3D_IB			10
#define SI_GPU_3D_FENCE			11

#define SI_CONTEXT_LOST		1/* for instance, back from suspend */
#define SI_RING_TIMEOUT		2
#define SI_FENCE_TIMEOUT	3
#define SI_DCE_PF_PENDING	4

struct si_timeout_info {
	uint32_t n_max;
	uint32_t us;
};

struct si_timeouts_info {
	struct si_timeout_info ring;
	struct si_timeout_info fence;
};

struct si_dce_edid {
	uint8_t idx;
	void *edid;
	uint16_t edid_sz;
};

struct si_dce_dp_set {
	uint64_t primary;
	uint64_t secondary;
	uint32_t pitch;	/* if zero, horizontal pixels will be used */
	uint8_t idx;
	char mode[sizeof("hhhhxvvvv@vvv")];
	char pixel_fmt[sizeof("ARGB2101010")];
};

struct si_mem {
	union {
		uint64_t gpu_addr;	/* output */
		struct {		/* input */
			uint64_t sz;
			uint64_t align;
		};
	};
};

/* amd provides public full specs for their radeon dma engines */
#define SI_DMA_TYPE_L2L		1
#define SI_DMA_TYPE_U32_FILL	2

#define SI_DMA_TO_DEVICE	1
#define SI_DMA_FROM_DEVICE	2
#define SI_DMA_ON_DEVICE	3
#define SI_DMA_ON_HOST		4 /* linux dma-engine abstraction? */

struct si_dma_l2l {
	uint64_t src_addr;
	uint64_t dst_addr;
	uint64_t sz;
};

struct si_dma_u32_fill {
	uint64_t dst_addr;
	uint64_t dws_n;
	uint32_t constant;
};

union si_dma_params {
	struct si_dma_l2l l2l;
	struct si_dma_u32_fill u32_fill;
};

struct si_dma {
	uint8_t type;
	uint8_t dir;
	struct si_timeouts_info t_info;
	union si_dma_params params;
};

union si_cpu_addr_to_gpu_addr {
	void *cpu_addr;
	uint64_t gpu_addr;
};

struct si_gpu_3d_ib {
	uint64_t gpu_addr;
	uint64_t dws_n;
	struct si_timeout_info ring_t_info;
};

struct si_gpu_3d_fence {
	struct si_timeouts_info t_info;
};

/*----------------------------------------------------------------------------*/
/* events */
#define SI_EVT_PF 1
struct si_evt_pf {
	uint8_t idx;
	uint32_t vblanks_n;
	struct timespec monotonic_raw_tp;
};

union si_evt_params {
	struct si_evt_pf pf;
};

struct si_evt {
	uint8_t type;
	union si_evt_params params;
};
/*----------------------------------------------------------------------------*/

#ifndef __KERNEL__
#define SI_GPU_PAGE_SZ 4096
#endif
#endif
