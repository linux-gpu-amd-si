#ifndef ALGA_AMD_SI_GPU_REGS_SH_H
#define ALGA_AMD_SI_GPU_REGS_SH_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#ifndef __KERNEL__
#ifndef BIT
#define BIT(x) (1 << (x))
#endif
#endif

/* start of shader register area: 0xb000-0xc000 */
#define SPI_SH_USER_DATA_VS_0		0xb130
#define SPI_SH_USER_DATA_VS_1		0xb134
#define SPI_SH_USER_DATA_VS_2		0xb138
#define SPI_SH_USER_DATA_VS_3		0xb13c
#define SPI_SH_USER_DATA_VS_4		0xb140
#define SPI_SH_USER_DATA_VS_5		0xb144
#define SPI_SH_USER_DATA_VS_6		0xb148
#define SPI_SH_USER_DATA_VS_7		0xb14c
#define SPI_SH_USER_DATA_VS_8		0xb150
#define SPI_SH_USER_DATA_VS_9		0xb154
#define SPI_SH_USER_DATA_VS_A		0xb158
#define SPI_SH_USER_DATA_VS_B		0xb15c
#define SPI_SH_USER_DATA_VS_C		0xb160
#define SPI_SH_USER_DATA_VS_D		0xb164
#define SPI_SH_USER_DATA_VS_E		0xb168
#define SPI_SH_USER_DATA_VS_F		0xb16c
#define SPI_SH_PGM_LO_PS			0xb020
#define SPI_SH_PGM_HI_PS			0xb024
#define		SSPHP_MEM_BASE				0x000000ff
#define SPI_SH_PGM_RSRC_PS_0			0xb028
#define		SSPRP_VGPRS				0x0000003f
#define		SSPRP_SGPRS				0x000003c0
#define		SSPRP_PRIORITY				0x00000c00
#define		SSPRP_FLOAT_MODE			0x000ff000
#define		SSPRP_PRIV				BIT(20)
#define		SSPRP_DX10_CLAMP			BIT(21)
#define		SSPRP_DEBUG_MODE			BIT(22)
#define		SSPRP_IEEE_MODE				BIT(23)
#define		SSPRP_CU_GROUP_DIS			BIT(24)
#define SPI_SH_PGM_RSRC_PS_1			0xb02c
#define		SSPRP_SCRATCH_ENA			BIT(0)
#define		SSPRP_USER_SGPR				0x0000003e
#define		SSPRP_WAVE_CNT_ENA			BIT(7)
#define		SSPRP_EXTRA_LDS_SZ			0x0000ff00
#define		SSPRP_EXCP_ENA				0x007f0000

#define SPI_SH_PGM_LO_VS			0xb120
#define SPI_SH_PGM_HI_VS			0xb124
#define		SSPHV_MEM_BASE				0x000000ff
#define SPI_SH_PGM_RSRC_VS_0			0xb128
#define		SSPRV_VGPRS				0x0000003f
#define		SSPRV_SGPRS				0x000003c0
#define		SSPRV_PRIORITY				0x00000c00
#define		SSPRV_FLOAT_MODE			0x000ff000
#define		SSPRV_PRIV				BIT(20)
#define		SSPRV_DX10_CLAMP			BIT(21)
#define		SSPRV_DEBUG_MODE			BIT(22)
#define		SSPRV_IEEE_MODE				BIT(23)
#define		SSPRV_VGPR_COMP_CNT			0x003000000
#define		SSPRV_CU_GROUP_ENA			BIT(26)
#define SPI_SH_PGM_RSRC_VS_1			0xb12c
#define		SSPRV_SCRATCH_ENA			BIT(0)
#define		SSPRV_USER_SGPR				0x0000003e
#define		SSPRV_OC_LDS_ENA			BIT(7)
#define		SSPRV_SO_BASE_0_ENA			BIT(8)
#define		SSPRV_SO_BASE_1_ENA			BIT(9)
#define		SSPRV_SO_BASE_2_ENA			BIT(10)
#define		SSPRV_SO_BASE_3_ENA			BIT(11)
#define		SSPRV_SO_ENA				BIT(12)
#define		SSPRV_EXCP_ENA				0x000fe000
#endif
