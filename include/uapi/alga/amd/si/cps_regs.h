#ifndef ALGA_AMD_SI_CPS_REGS_H
#define ALGA_AMD_SI_CPS_REGS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#ifndef __KERNEL__
#ifndef BIT
#define BIT(x) (1 << (x))
#endif
#endif

/* start of configuration register area: 0x8000-0xb000------------------------*/
#define	CP_SEM_WAIT_TIMER			0x85bc

#define	CP_SEM_INCOMPLETE_TIMER_CTL		0x85c8

#define CP_COHER_CTL_1				0x85e8

#define CP_COHER_CTL_0				0x85f0
#define		CCC_DEST_BASE_0_ENA			BIT(0)
#define		CCC_DEST_BASE_1_ENA			BIT(1)
#define		CCC_CB0_DEST_BASE_ENA			BIT(6)
#define		CCC_CB1_DEST_BASE_ENA			BIT(7)
#define		CCC_CB2_DEST_BASE_ENA			BIT(8)
#define		CCC_CB3_DEST_BASE_ENA			BIT(9)
#define		CCC_CB4_DEST_BASE_ENA			BIT(10)
#define		CCC_CB5_DEST_BASE_ENA			BIT(11)
#define		CCC_CB6_DEST_BASE_ENA			BIT(12)
#define		CCC_CB7_DEST_BASE_ENA			BIT(13)
#define		CCC_DB_DEST_BASE_ENA			BIT(14)
#define		CCC_TCL1_VOL_ACTION_ENA			BIT(15)
#define		CCC_TCL2_VOL_ACTION_ENA			BIT(16)
#define		CCC_TCL2_WB_ACTION_ENA			BIT(18)
#define		CCC_DEST_BASE_2_ENA			BIT(19)
#define		CCC_DEST_BASE_3_ENA			BIT(21)
#define		CCC_TCL1_ACTION_ENA			BIT(22)
#define		CCC_TCL2_ACTION_ENA			BIT(23)
#define		CCC_CB_ACTION_ENA			BIT(25)
#define		CCC_DB_ACTION_ENA			BIT(26)
#define		CCC_SH_KCACHE_ACTION_ENA		BIT(27)
#define		CCC_SH_KCACHE_VOL_ACTION_ENA		BIT(28)
#define		CCC_SH_ICACHE_ACTION_ENA		BIT(29)
#define CP_COHER_SZ				0x85f4
#define CP_COHER_BASE				0x85f8

#define CP_ME_CTL				0x86d8
#define		CMC_CP_CE_HALT				BIT(24)
#define		CMC_CP_PFP_HALT				BIT(26)
#define		CMC_CP_ME_HALT				BIT(28)

#define	CP_RB_2_RPTR				0x86f8
#define	CP_RB_1_RPTR				0x86fc
#define	CP_RB_0_RPTR				0x8700
#define	CP_RBS_WPTR_DELAY			0x8704

#define	CP_QUEUE_THRESHOLDS			0x8760
#define		CQT_ROQ_IB_0_START			0xffffffff
#define		CQT_ROQ_IB_1_START			0xffffff00
#define CP_MEQ_THRESHOLDS			0x8764
#define		CMT_MEQ_0_START				0xffffffff
#define		CMT_MEQ_1_START				0xffffff00

#define	CP_PERFMON_CTL				0x87fc
/* end of configuration register area: 0x8000-0xb000--------------------------*/

#define	CP_RB_0_BASE				0xc100
#define	CP_RB_0_CTL				0xc104
#define		CRC_RB_BUF_LOG2_QWS			0xffffffff
#define		CRC_RB_BLK_LOG2_QWS			0xffffff00
#define		CRC_BUF_SWAP				0x00030000				
#define			CRC_BUF_SWAP_32BITS			2
#define		CRC_RB_NO_UPDATE			BIT(27)
#define		CRC_RB_RPTR_WR_ENA			BIT(31)

#define	CP_RB_0_RPTR_ADDR_LO			0xc10c
#define	CP_RB_0_RPTR_ADDR_HI			0xc110
#define	CP_RB_0_WPTR				0xc114

#define	CP_PFP_UCODE_ADDR			0xc150
#define	CP_PFP_UCODE_DATA			0xc154
#define	CP_ME_RAM_RADDR				0xc158
#define	CP_ME_RAM_WADDR				0xc15C
#define	CP_ME_RAM_DATA				0xc160

#define	CP_CE_UCODE_ADDR			0xc168
#define	CP_CE_UCODE_DATA			0xc16c

#define	CP_RB_1_BASE				0xc180
#define	CP_RB_1_CTL				0xc184
#define	CP_RB_1_RPTR_ADDR_LO			0xc188
#define	CP_RB_1_RPTR_ADDR_HI			0xc18c
#define	CP_RB_1_WPTR				0xc190
#define	CP_RB_2_BASE				0xc194
#define	CP_RB_2_CTL				0xc198
#define	CP_RB_2_RPTR_ADDR_LO			0xc19c
#define	CP_RB_2_RPTR_ADDR_HI			0xc1a0
#define	CP_RB_2_WPTR				0xc1a4
#define CP_INT_CTL_RING_0			0xc1a8
#define CP_INT_CTL_RING_1			0xc1ac
#define CP_INT_CTL_RING_2			0xc1b0
#define		CICR_CNTX_BUSY_INT_ENA			BIT(19)
#define		CICR_CNTX_EMPTY_INT_ENA			BIT(20)
#define		CICR_WAIT_MEM_SEM_INT_ENA		BIT(21)
#define		CICR_TIME_STAMP_INT_ENA			BIT(26)
#define		CICR_CP_RING_ID_2_INT_ENA		BIT(29)
#define		CICR_CP_RING_ID_1_INT_ENA		BIT(30)
#define		CICR_CP_RING_ID_0_INT_ENA		BIT(31)
#define CP_INT_STATUS_RING_0			0xc1b4
#define CP_INT_STATUS_RING_1			0xc1b8
#define CP_INT_STATUS_RING_2			0xc1bc
#define		CISR_WAIT_MEM_SEM_INT_STAT		BIT(21)
#define		CISR_TIME_STAMP_INT_STAT		BIT(26)
#define		CISR_CP_RING_ID_2_INT_STAT		BIT(29)
#define		CISR_CP_RING_ID_1_INT_STAT		BIT(30)
#define		CISR_CP_RING_ID_0_INT_STAT		BIT(31)

#define	CP_MEM_SLP_CTL				0xc1e4
#define		CMSC_CP_MEM_LS_ENA			BIT(0)

#define	CP_DEBUG				0xc1fc
#endif
