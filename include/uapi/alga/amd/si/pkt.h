#ifndef ALGA_AMD_SI_PKT_H
#define ALGA_AMD_SI_PKT_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#ifndef __KERNEL__
#ifndef BIT
#define BIT(x) (1 << (x))
#endif
#endif

#define CP_RING_PFP_DWS 16
#define CP_RING_PFP_DW_MASK (CP_RING_PFP_DWS - 1)

#define	PKT_TYPE2	2
#define	PKT_TYPE3	3
#define PKT2		(PKT_TYPE2 << 30)
#define PKT3(op,n)	((PKT_TYPE3 << 30) \
			| (((op) & 0xff) << 8) \
			| (((n - 1) & 0x3fff) << 16))
#define PKT3_COMPUTE(op,n) (PKT3(op,n) | (1 << 1))

/* packet 3 operations */

#define	PKT3_SET_BASE				0x11
#define		PKT3_BASE_IDX				0xffffffff
#define			PKT3_GDS_PARTITION_BASE			2
#define			PKT3_CE_PARTITION_BASE			3
#define	PKT3_CLR_STATE				0x12
#define PKT3_MODE_CTL				0x18
#define PKT3_SET_PREDICATION			0x20
#define		PRED_DRAW				0x00000100
#define			PRED_DRAW_NOT_VISIBLE			0
#define			PRED_DRAW_VISIBLE			1
#define		PRED_HINT				0x00001000
#define			PRED_HINT_WAIT				0
#define			NOWAIT_DRAW				1
#define		PRED_OP					0xffff0000
#define			PRED_OP_CLR				0
#define			PRED_OP_ZPASS				1
#define			PRED_OP_PRIM_CNT			2
#define		PRED_CONTINUE				BIT(31)
#define PKT3_CTX_CTL				0x28
#define PKT3_IDX_TYPE				0x2a
#define		PKT3_SZ					0x00000001
#define			PKT3_16BITS				0
#define			PKT3_32BITS				1
#define		PKT3_SWAP_MODE				0x0000000c
#define PKT3_DRAW_IDX_AUTO			0x2d
#define PKT3_INST_N				0x2f
#define PKT3_IB					0x32
#define	PKT3_SURF_SYNC				0x43
#define	PKT3_ME_INIT				0x44
#define		PKT3_ME_INIT_DEV_ID			0xffff0000
#define	PKT3_EVENT_WR				0x46
#define		PKT3_EVENT_IDX 				0x00000700
                /*
		 * 0 - any non-TS event
		 * 1 - ZPASS_DONE
		 * 2 - SAMPLE_PIPELINESTAT
		 * 3 - SAMPLE_STREAMOUTSTAT*
		 * 4 - *S_PARTIAL_FLUSH
		 * 5 - EOP events
		 * 6 - EOS events
		 * 7 - CACHE_FLUSH, CACHE_FLUSH_AND_INV_EVENT
		 */
#define		PKT3_INV_L2				BIT(20)
                /* INV TC L2 cache when PKT3_EVENT_IDX = 7 */
#define	PKT3_EVENT_WR_EOP			0x47
#define		PKT3_DATA_SEL				0x60000000
                /*
		 * 0 - discard
		 * 1 - send low 32bit data
		 * 2 - send 64bit data
		 * 3 - send 64bit counter value
		 */
#define		PKT3_INT_SEL				0x03000000
                /*
		 * 0 - none
		 * 1 - interrupt only (DATA_SEL = 0)
		 * 2 - interrupt when data write is confirmed
		 */
#define	PKT3_PREAMBLE_CTL			0x4a
#define		PKT3_PREAMBLE_BEGIN_CLR_STATE		(2 << 28)
#define		PKT3_PREAMBLE_END_CLR_STATE		(3 << 28)
#define	PKT3_SET_CFG_REG			0x68
#define		PKT3_SET_CFG_REG_START			0x00008000
#define		PKT3_SET_CFG_REG_END			0x0000b000
#define CFG_REG_IDX(x) ((x - PKT3_SET_CFG_REG_START) >> 2)
#define	PKT3_SET_CTX_REG			0x69
#define		PKT3_SET_CTX_REG_START			0x00028000
#define		PKT3_SET_CTX_REG_END			0x00029000
#define CTX_REG_IDX(x) ((x - PKT3_SET_CTX_REG_START) >> 2)
#define PKT3_SET_SH_REG				0x76
#define		PKT3_SET_SH_REG_START			0x0000b000
#define		PKT3_SET_SH_REG_END			0x0000c000
#define SH_REG_IDX(x) ((x - PKT3_SET_SH_REG_START) >> 2)

#endif
