#ifndef ALGA_PIXEL_FMTS_H
#define ALGA_PIXEL_FMTS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#define ALGA_PIXEL_FMTS_MAX 64

#define ALGA_PIXEL_FMT_INVALID	0
#define	ALGA_ARGB6666		1
#define ALGA_ARGB8888		2
#define ALGA_ARGB2101010	3
#define ALGA_PIXEL_FMTS_N	4

static uint8_t alga_pixel_fmts_sz[] __attribute__ ((unused)) = {
	[ALGA_PIXEL_FMT_INVALID]= 0,
	[ALGA_ARGB6666]		= 4,
	[ALGA_ARGB8888]		= 4,
	[ALGA_ARGB2101010]	= 4 
};

static uint8_t alga_pixel_fmts_bpc[] __attribute__ ((unused)) = {
	[ALGA_PIXEL_FMT_INVALID]= 0,
	[ALGA_ARGB6666]		= 6,
	[ALGA_ARGB8888]		= 8,
	[ALGA_ARGB2101010]	= 10
};

static const char *alga_pixel_fmts_str[] __attribute__ ((unused)) = {
	[ALGA_PIXEL_FMT_INVALID]= "invalid",
	[ALGA_ARGB6666]		= "ARGB6666",
	[ALGA_ARGB8888]		= "ARGB8888",
	[ALGA_ARGB2101010]	= "ARGB2101010"
};
#endif
