#ifndef ALGA_DP_H
#define ALGA_DP_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#define DP_MAX_LANES_N 4

#define DP_AUX_NATIVE_WRITE	0x8 
#define DP_AUX_NATIVE_READ	0x9 
#define DP_AUX_I2C_WRITE	0x0
#define DP_AUX_I2C_READ		0x1
#define DP_AUX_I2C_STATUS	0x2
#define DP_AUX_I2C_MOT		0x4

#define DP_AUX_NATIVE_REPLY_ACK		(0x0 << 4)
#define DP_AUX_NATIVE_REPLY_NACK	(0x1 << 4)
#define DP_AUX_NATIVE_REPLY_DEFER	(0x2 << 4)
#define DP_AUX_NATIVE_REPLY_MASK	(0x3 << 4)

#define DP_AUX_I2C_REPLY_ACK	(0x0 << 6)
#define DP_AUX_I2C_REPLY_NACK	(0x1 << 6)
#define DP_AUX_I2C_REPLY_DEFER	(0x2 << 6)
#define DP_AUX_I2C_REPLY_MASK	(0x3 << 6)

#define DP_LINK_RATE_UNIT_KHZ 270000
#define DP_LINK_RATE_UNIT_MHZ 270

/* DPCD registers */
#define DPCD_LINK_STATUS_SZ 6 /* 6 dpcd bytes */

#define DPCD_REV			0x0000
#define DPCD_MAX_LINK_RATE		0x0001
#define DPCD_MAX_LANE_COUNT		0x0002
#define		DPCD_LANE_COUNT_MASK			0x0f
#define		DPCD_LANE_COUNT_ENHANCED_FRAME_EN	BIT(7)
#define DPCD_MAX_DOWNSPREAD		0x0003
#define		DPCD_MAX_DOWNSPREAD_SUPPORT		BIT(0)

#define DPCD_LINK_BW_SET		0x0100
/* factor of 270 MHz units */
#define		DPCD_LINK_BW_1_62			0x06
#define		DPCD_LINK_BW_2_7			0x0a
#define		DPCD_LINK_BW_3_24			0x10
#define		DPCD_LINK_BW_5_4			0x14
/* same definition than DPCD_MAX_LANE_COUNT */
#define DPCD_LANE_COUNT_SET		0x0101
#define DPCD_TRAINING_PATTERN_SET	0x0102
#define		DPCD_TRAINING_PATTERN_DISABLE		0
#define		DPCD_TRAINING_PATTERN_1			1
#define		DPCD_TRAINING_PATTERN_2			2
#define		DPCD_TRAINING_PATTERN_3			3
#define		DPCD_TRAINING_PATTERN_MASK		0x3

#define DPCD_TRAINING_LANE0_SET		0x0103
#define DPCD_TRAINING_LANE1_SET		0x0104
#define DPCD_TRAINING_LANE2_SET		0x0105
#define DPCD_TRAINING_LANE3_SET		0x0106
#define		DPCD_VOLT_SWING_SHIFT			0
#define		DPCD_VOLT_SWING_MASK			(0x3 << \
							DPCD_VOLT_SWING_SHIFT)
#define		DPCD_MAX_VOLT_SWING_REACHED		BIT(2 + \
							DPCD_VOLT_SWING_SHIFT)
#define		DPCD_VOLT_SWING_400			(0 << \
							DPCD_VOLT_SWING_SHIFT)
#define		DPCD_VOLT_SWING_600			(1 << \
							DPCD_VOLT_SWING_SHIFT)
#define		DPCD_VOLT_SWING_800			(2 << \
							DPCD_VOLT_SWING_SHIFT)
#define		DPCD_VOLT_SWING_1200			(3 << \
							DPCD_VOLT_SWING_SHIFT)

#define		DPCD_PRE_EMPHASIS_SHIFT			3
#define		DPCD_PRE_EMPHASIS_MASK			(0x3 << \
						DPCD_PRE_EMPHASIS_SHIFT)
#define		DPCD_MAX_PRE_EMPHASIS_REACHED		BIT(2 + \
						DPCD_PRE_EMPHASIS_SHIFT)
#define		DPCD_PRE_EMPHASIS_0			(0 << \
						DPCD_PRE_EMPHASIS_SHIFT)
#define		DPCD_PRE_EMPHASIS_3_5			(1 << \
						DPCD_PRE_EMPHASIS_SHIFT)
#define		DPCD_PRE_EMPHASIS_6			(2 << \
						DPCD_PRE_EMPHASIS_SHIFT)
#define		DPCD_PRE_EMPHASIS_9_5			(3 << \
						DPCD_PRE_EMPHASIS_SHIFT)

#define		DPCD_DOWNSPREAD_CTL	0x0107
#define			DPCD_SPREAD_AMP_0_5		BIT(4)

#define DPCD_LANE0_1_STATUS		0x0202
#define DPCD_LANE2_3_STATUS		0x0203
#define		DPCD_LANE_CR_DONE			BIT(0)
#define		DPCD_LANE_CHANNEL_EQ_DONE		BIT(1)
#define		DPCD_LANE_SYMBOL_LOCKED			BIT(2)
#define		DPCD_CHANNEL_EQ_BITS			(DPCD_LANE_CR_DONE | \
						DPCD_LANE_CHANNEL_EQ_DONE | \
						DPCD_LANE_SYMBOL_LOCKED)
#define DPCD_LANE_ALIGN_STATUS_UPDATED	0x0204
#define		DPCD_INTERLANE_ALIGN_DONE		BIT(0)
#define		DPCD_DOWNSTREAM_PORT_STATUS_CHANGED	BIT(6)
#define		DPCD_LINK_STATUS_UPDATED		BIT(7)

#define DPCD_SINK_STATUS		0x0205
#define		DPCD_RECEIVE_PORT_0_STATUS		BIT(0)
#define		DPCD_RECEIVE_PORT_1_STATUS		BIT(1)

#define DPCD_ADJUST_REQUEST_LANE0_1	0x0206
#define DPCD_ADJUST_REQUEST_LANE2_3	0x0207
#define		DPCD_ADJUST_VOLT_SWING_LANEX_MASK	0x03
#define		DPCD_ADJUST_VOLT_SWING_LANEX_SHIFT	0
#define		DPCD_ADJUST_PRE_EMPHASIS_LANEX_MASK	0x0c
#define		DPCD_ADJUST_PRE_EMPHASIS_LANEX_SHIFT	2
#define		DPCD_ADJUST_VOLT_SWING_LANEY_MASK	0x30
#define		DPCD_ADJUST_VOLT_SWING_LANEY_SHIFT	4
#define		DPCD_ADJUST_PRE_EMPHASIS_LANEY_MASK	0xc0
#define		DPCD_ADJUST_PRE_EMPHASIS_LANEY_SHIFT	6

#define DPCD_SET_PWR			0x0600
#define		DPCD_SET_PWR_D0				0x1
#define		DPCD_SET_PWR_D3				0x2

#define DPCD_MAX_LANE_COUNT_MASK 0x0f

void alga_dp_safe_timing(struct alga_timing (*ts)[ALGA_TIMINGS_MAX]);

static inline char *dp_link_rate_names(uint8_t link_rate)
{
	switch (link_rate) {
	case DPCD_LINK_BW_1_62:
		return "1.62 GHz";
	case DPCD_LINK_BW_2_7:
		return "2.70 GHz";
	case DPCD_LINK_BW_3_24:
		return "3.24 GHz";
	case DPCD_LINK_BW_5_4:
		return "5.40 GHz";
	default:
		return "unregistered GHz";
	}
}
#endif
