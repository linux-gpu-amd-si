#ifndef ALGA_AMD_ATOMBIOS_CM_H
#define ALGA_AMD_ATOMBIOS_CM_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/*============================================================================*/
/* spread spectrum related */
/*----------------------------------------------------------------------------*/
#define ATB_DONT_HAVE_MEM_CLK_SS	0
#define ATB_HAVE_MEM_CLK_SS		1
long atb_have_mem_clk_ss(struct atombios *atb);
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
#define ATB_DONT_HAVE_ENG_CLK_SS	0
#define ATB_HAVE_ENG_CLK_SS		1
long atb_have_eng_clk_ss(struct atombios *atb);
/*----------------------------------------------------------------------------*/

struct atb_ss {
	u16 percentage;	/* 0.01 % units */
	/*
	 * [0]=0: down spread
	 * [0]=1: center spread
	 * [1]=0: internal ss
	 * [1]=1: external ss
	 */
	u8 mode;
	u16 rate;	/* 10 kHz units */
};
long atb_eng_clk_ss_get(struct atombios *atb, u32 eng_clk,
							struct atb_ss *atb_ss);
long atb_mem_clk_ss_get(struct atombios *atb, u32 mem_clk,
							struct atb_ss *atb_ss);
/*============================================================================*/

long atb_core_ref_clk_get(struct atombios *atb, u16 *core_ref_clk);
long atb_mem_ref_clk_get(struct atombios *atb, u16 *mem_ref_clk);

long atb_eng_mem_timings_program(struct atombios *atb, u32 eng_clk,
								u32 mem_clk);

/*----------------------------------------------------------------------------*/
#define ATB_ENG_PLL_POST_DIV_ENA	BIT(0)
#define ATB_ENG_PLL_DITHEN_ENA		BIT(1)
#define ATB_ENG_PLL_VCO_MODE		BIT(2)
struct atb_eng_pll {
	u16 fb_integer;
	u16 fb_frac;
	u8 flgs;
	u8 ref_div;
	u8 post_div;
};
long atb_eng_pll_compute(struct atombios *atb, u32 eng_clk,
					struct atb_eng_pll *atb_eng_pll);

#define ATB_MEM_PLL_Y_CLK_SEL		BIT(0)
#define ATB_MEM_PLL_QDR_ENA		BIT(1)
#define ATB_MEM_PLL_AD_HALF_RATE	BIT(2)
struct atb_mem_pll {
	u16 fb_integer;
	u16 fb_frac;
	u8 post_div;
	u8 bw_ctl;
	u8 dll_speed;
	u8 vco_mode;
	u8 flgs;
};
long atb_mem_pll_compute(struct atombios *atb, u32 mem_clk, u8 strobe_mode,
					struct atb_mem_pll *atb_mem_pll);
/*----------------------------------------------------------------------------*/
#endif
