#ifndef ALGA_AMD_ATOMBIOS_ATB_DEV_H
#define ALGA_AMD_ATOMBIOS_ATB_DEV_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct atb_dev {
	struct device *dev;
	void *rom;
	u32 (*rr32)(struct device *dev, u32 of);
	void (*wr32)(struct device *dev, u32 val, u32 of);
};
#endif

