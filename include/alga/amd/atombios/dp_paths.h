#ifndef DP_PATHS_H
#define DP_PATHS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct atb_dp_path
{
	u8 i;
	u8 dfp;	/* 0~6, for atombios DFP0~6 */
	u8 edp;
	u8 hpd;
	u8 aux_i2c_id;
	u8 uniphy_link_hbr2;
};
#endif
