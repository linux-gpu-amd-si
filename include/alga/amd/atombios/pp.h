#ifndef ALGA_AMD_ATOMBIOS_PP_H
#define ALGA_AMD_ATOMBIOS_PP_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct atb_pp_defaults {
	u32 eng_clk;	/* 10 kHz units */
	u32 mem_clk;	/* 10 kHz units */
	u16 vddc_mv;
	u16 vddci_mv;
	u16 mvddc_mv;
};
long atb_pp_defaults_get(struct atombios *atb, struct atb_pp_defaults *d);

struct atb_pp_lvl {
	u32 eng_clk;
	u32 mem_clk;
	u16 vddc_id;	/* XXX:can be a lkge idx or mV units */
	u16 vddci_mv;
	u8 pcie_gen;
};

#define ATB_PP_STATE_LVLS_N_MAX 5
struct atb_pp_state {
	u8 lvls_n;
	struct atb_pp_lvl lvls[ATB_PP_STATE_LVLS_N_MAX];
};
/* boot state in entirely build from other tables than powerplay tables */
long atb_pp_emergency_state_get(struct atombios *atb,
						struct atb_pp_state *emergency);
long atb_pp_ulv_state_get(struct atombios *atb, struct atb_pp_state *ulv);
long atb_pp_performance_state_get(struct atombios *atb,
					struct atb_pp_state *performance);
#define ATB_VOLT_TYPE_VDDC	1
#define ATB_VOLT_TYPE_MVDDC	2
#define ATB_VOLT_TYPE_MVDDQ	3
#define ATB_VOLT_TYPE_VDDCI	4
long atb_volt_set(struct atombios *atb, u8 type, u16 lvl);
long atb_volt_get(struct atombios *atb, u8 type, u16 lkge_idx, u16 *lvl);
long atb_eng_clk_set(struct atombios *atb, u32 clk);
long atb_mem_clk_set(struct atombios *atb, u32 clk);

#define ATB_DONT_HAVE_THERMAL_PROTECTION	0
#define ATB_HAVE_THERMAL_PROTECTION		1
long atb_have_thermal_protection(struct atombios *atb);

#define ATB_PP_PLATFORM_CAPS_BACKBIAS				BIT(0)
#define ATB_PP_PLATFORM_CAPS_PP					BIT(1)
#define ATB_PP_PLATFORM_CAPS_SBIOS_PWR_SRC			BIT(2)
#define ATB_PP_PLATFORM_CAPS_ASPM_L0				BIT(3)
#define ATB_PP_PLATFORM_CAPS_ASPM_L1				BIT(4)
#define ATB_PP_PLATFORM_CAPS_HW_DC				BIT(5)
#define ATB_PP_PLATFORM_CAPS_GEMINI_PRIMARY			BIT(6)
#define ATB_PP_PLATFORM_CAPS_STEP_VDDC				BIT(7)
#define ATB_DO_NOT_USE_0					BIT(8)
#define ATB_PP_PLATFORM_CAPS_SIDEPORT_CTL			BIT(9)
#define ATB_PP_PLATFORM_CAPS_TURNOFFPLL_ASPML1			BIT(10)
#define ATB_PP_PLATFORM_CAPS_HTLINK_CTL				BIT(11)
#define ATB_DO_NOT_USE_1					BIT(12)
/* ho to boot state on alerts (e.g. on an AC->DC transition) */
#define ATB_PP_PLATFORM_CAPS_GOTO_BOOT_ON_ALERT			BIT(13)
/* Do not wait for vblank during an alert (e.g. AC->DC transition) */
#define ATB_PP_PLATFORM_CAPS_DONT_WAIT_FOR_VBLANK_ON_ALERT	BIT(14)
#define ATB_DO_NOT_USE_2					BIT(15)
/* enable the 'regulator hot' feature */
#define ATB_PP_PLATFORM_CAPS_REGULATOR_HOT			BIT(16)
/* does the driver supports baco state */
#define ATB_PP_PLATFORM_CAPS_BACO				BIT(17)
/* does the driver supports new cac volt tbl */
#define ATB_PP_PLATFORM_CAPS_NEW_CAC_VOLT			BIT(18)
/* does the driver supports revert gpio5 polarity */
#define ATB_PP_PLATFORM_CAPS_REVERT_GPIO5_POLARITY		BIT(19)
/* does the driver supports thermal2gpio17 */
#define ATB_PP_PLATFORM_CAPS_OUTPUT_THERMAL2GPIO17		BIT(20)
/* does the driver supports vr hot gpio Configurable */
#define ATB_PP_PLATFORM_CAPS_VRHOT_GPIO_CONFIGURABLE		BIT(21)
/* does the driver supports temp tnversion feature */
#define ATB_PP_PLATFORM_CAPS_TEMP_INVERSION			BIT(22)
#define ATB_PP_PLATFORM_CAPS_EVV				BIT(23)
long atb_pp_platform_caps_get(struct atombios *atb, u32 *platform_caps);

long atb_pp_back_bias_time_get(struct atombios *atb, u16 *back_bias_time);

long atb_pp_tdp_limits_get(struct atombios *atb, u32 *tdp_limit,
							u32 *near_tdp_limit);
long atb_pp_sq_ramping_threshold_get(struct atombios *atb,
						u32 *sq_ramping_threshold);
#endif
