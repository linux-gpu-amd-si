#ifndef ALGA_AMD_ATOMBIOS_VRAM_INFO_H
#define ALGA_AMD_ATOMBIOS_VRAM_INFO_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#define ATB_MC_REGS_N_MAX		32
#define ATB_MC_REG_SETS_N_MAX		20

struct atb_mc_reg_set {
	u32 mem_clk_max;
	u32 vals[ATB_MC_REGS_N_MAX];
};

struct atb_mc_reg_tbl {
	u8 addrs_n;
	u32 addrs[ATB_MC_REGS_N_MAX];

	u8 sets_n;
	struct atb_mc_reg_set sets[ATB_MC_REG_SETS_N_MAX];
};
long atb_vram_info(struct atombios *atb, struct atb_mc_reg_tbl *atb_mc_reg_tbl);
#endif
