#ifndef ALGA_AMD_ATOMBIOS_ATB_H
#define ALGA_AMD_ATOMBIOS_ATB_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define ATB_ERR 800

struct atombios;
#include <alga/amd/atombios/atb_dev.h>
struct atombios *atb_alloc(struct atb_dev *adev);
long atb_init(struct atombios *atb);
void atb_cleanup(struct atombios *atb);

long atb_asic_init(struct atombios *atb, u32 eng_clk, u32 mem_clk);
#endif
