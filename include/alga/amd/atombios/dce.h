#ifndef ALGA_AMD_ATOMBIOS_DCE_H
#define ALGA_AMD_ATOMBIOS_DCE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
void atb_lock(struct atombios *atb, u8 lock);
long atb_trans_link_init(struct atombios *atb, u8 i, u8 edp);
long atb_trans_link_off(struct atombios *atb, u8 i);
long atb_trans_link_on(struct atombios *atb, u8 i, u8 dp_link_rate_units,
								u8 dp_lanes_n);
long atb_trans_link_pwr(struct atombios *atb, u8 i, u8 on);
long atb_trans_link_vs_pre_emph(struct atombios *atb, u8 i, u8 vs_pre_emph);
long atb_enc_crtc_src(struct atombios *atb, u8 i);
long atb_enc_video(struct atombios *atb, u8 i, u8 hpd, u8 on);
long atb_enc_setup(struct atombios *atb, u8 i, u8 hpd, u8 dp_lanes_n,
     				u8 dp_link_rate, u8 bpc, u32 pixel_clk);
long atb_enc_setup_panel_mode(struct atombios *atb, u8 i, u8 hpd);
long atb_crtc_blank(struct atombios *atb, u8 i, u8 on);
long atb_crtc(struct atombios *atb, u8 i, u8 on);
long atb_crtc_lock(struct atombios *atb, u8 i, u8 lock);
long atb_crtc_virtual_pixel_clk(struct atombios *atb, u8 i, u32 clk);
long atb_crtc_timing(struct atombios *atb, u8 i, struct alga_timing *t);
long atb_crtc_dcpll(struct atombios *atb);
long atb_crtc_overscan(struct atombios *atb, u8 i);
long atb_crtc_scaler(struct atombios *atb, u8 i);

long atb_crtc_pair_pwr_gate(struct atombios *atb, u8 i, u8 on);

long atb_enc_dp_training_start(struct atombios *atb, u8 i, u8 hpd);
long atb_enc_dp_training_complete(struct atombios *atb, u8 i, u8 hpd);
long atb_enc_dp_tp(struct atombios *atb, u8 i, u8 hpd, u8 link_rate, u8 lanes_n,
									u8 tp);

#include <alga/amd/atombios/dp_paths.h>
long atb_dp_paths(struct atombios *atb, struct atb_dp_path **dp_paths,
								u8 *dp_paths_n);
void atb_dp_state(struct atombios *atb, u8 dfp, u8 connected);

long atb_dp_aux_native_read(struct atombios *atb, u8 aux_i2c_id, u8 hpd,
				u32 addr, u8 *recv_buf, u16 recv_buf_sz);
long atb_dp_aux_native_write(struct atombios *atb, u8 aux_i2c_id, u8 hpd,
				u32 addr, u8 *send_buf, u16 send_buf_sz);

#define ATB_MODE_I2C_READ	1
#define ATB_MODE_I2C_WRITE	2
#define ATB_MODE_I2C_START	4
#define ATB_MODE_I2C_STOP	8
long atb_dp_aux_i2c(struct atombios *atb, u8 aux_i2c_id, u8 hpd, u16 addr,
					u8 mode, u8 byte_send, u8 *byte_recv);
#endif
