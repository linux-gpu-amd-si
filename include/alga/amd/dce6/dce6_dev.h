#ifndef ALGA_AMD_DCE6_DCE6_DEV_H
#define ALGA_AMD_DCE6_DCE6_DEV_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct dce6_dev {/* provides device specific services for dce6 */
	struct device *dev;
	u8 crtcs_n;
	u32 (*rr32)(struct device *dev, u32 of);
	void (*wr32)(struct device *dev, u32 val, u32 of);
	void (*dyn_pm_new_display_notify)(struct device *dev, u8 dps_active_cnt,
							u8 dps_active_first);
	struct atombios *atb;
};
#endif
