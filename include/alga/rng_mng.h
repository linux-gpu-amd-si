#ifndef ALGA_RNG_MNG_H
#define ALGA_RNG_MNG_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct rng {
	u64 s;
	u64 sz;
	struct list_head n;
};
void rng_mng_init(struct rng *mng, u64 s, u64 sz);
void rng_mng_destroy(struct rng *mng);
/* sz must be aligned or smaller than align */
int rng_alloc_align(u64 *s_aligned, struct rng *mng, u64 sz, u64 align);
#define rng_alloc(p, mng, sz) rng_alloc_align((p),(mng), (sz), 1)
void rng_free(struct rng *mng, u64 s);

static inline u64 rng_align(u64 p, u64 align)
{
	u64 adjust;

	adjust = p % align;
	return p + (adjust ? align - adjust : 0);
}
#endif
