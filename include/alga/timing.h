#ifndef ALGA_TIMING_H
#define ALGA_TIMING_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define ALGA_TIMINGS_MAX 32
struct alga_timing {
	/* pixels */
	u32 h;
	u32 h_bl; 
	u32 h_so;
	u32 h_spw;
	u32 h_sp;		/* 0: high/positive, 1: low/negative */

	/* lines */
	u32 v;
	u32 v_bl; 
	u32 v_so;
	u32 v_spw;
	u32 v_sp;		/* 0: high/positive, 1: low/negative */

	u32 pixel_clk;		/* kHz */

	char mode[sizeof("hhhhxvvvv@rrr")];	/* want a terminating 0 */
};
#endif
