#ifndef ALGA_EDID_H
#define ALGA_EDID_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define EDID_BLK_SZ 128
/* max size for a sysfs attribute is PAGE_SIZE - 1 */
#if EDID_BLK_SZ >= PAGE_SIZE
	#error "sysfs attribute on this architecture won't fit the edid block 0"
#endif
#define EDID_DESCRIPTOR_STR_SZ_MAX (13 + 1)
long alga_i2c_edid_fetch(struct device *dev, struct i2c_adapter *a,
								void **edid);
long alga_edid_timings(struct device *dev, void *edid,
				struct alga_timing (*ts)[ALGA_TIMINGS_MAX]);
u8 alga_edid_bpc(struct device *dev, void *edid);
u16 alga_edid_manufacturer(struct device *dev, void *edid);
u16 alga_edid_product_code(struct device *dev, void *edid);
void alga_edid_monitor_name(struct device *dev, void *edid, u8 *name);
void alga_edid_monitor_serial(struct device *dev, void *edid, u8 *serial);
#endif
