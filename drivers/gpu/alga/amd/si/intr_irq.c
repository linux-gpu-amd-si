/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#include <linux/pci.h>
#include <linux/irqreturn.h>
#include <linux/cdev.h>

#include <alga/rng_mng.h>
#include <uapi/alga/pixel_fmts.h>
#include <alga/timing.h>
#include <alga/amd/atombios/atb.h>
#include <uapi/alga/amd/dce6/dce6.h>

#include "mc.h"
#include "rlc.h"
#include "ih.h"
#include "fence.h"
#include "ring.h"
#include "dmas.h"
#include "ba.h"
#include "cps.h"
#include "gpu.h"
#include "drv.h"

#include "regs.h"

void intrs_reset(struct pci_dev *dev)
{
	struct dev_drv_data *dd;
	dd = pci_get_drvdata(dev);

	cps_intr_reset(dev);

	dmas_intr_reset(dev);

	gpu_grdbm_int_reset(dev);

	dce6_intrs_reset(dd->dce);
}

irqreturn_t irq_thd(int irq, void *dev_id)
{
	struct pci_dev *dev;
	struct dev_drv_data *dd;

	dev = dev_id;
	dd = pci_get_drvdata(dev);

	dce6_irqs_thd(dd->dce);
	return IRQ_HANDLED;
}

irqreturn_t irq(int irq, void *dev_id)
{
	struct pci_dev *dev;
	dev = dev_id;
	if (ih_parse(dev) == IRQ_THD_ENA)
		return IRQ_WAKE_THREAD;
	else
		return IRQ_HANDLED;
}
