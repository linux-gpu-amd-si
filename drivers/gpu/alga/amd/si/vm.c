/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#include <linux/pci.h>
#include <asm/byteorder.h>
#include <linux/interrupt.h>
#include <linux/firmware.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/idr.h>
#include <linux/pm.h>
#include <linux/list.h>

#include <alga/alga.h>
#include <alga/rng_mng.h>
#include <uapi/alga/pixel_fmts.h>
#include <alga/timing.h>
#include <alga/amd/atombios/atb.h>
#include <alga/amd/atombios/pp.h>
#include <uapi/alga/amd/dce6/dce6.h>

#include "mc.h"
#include "rlc.h"
#include "ih.h"
#include "fence.h"
#include "ring.h"
#include "dmas.h"
#include "ba.h"
#include "cps.h"
#include "gpu.h"
#include "drv.h"

#include "regs.h"

void vg_ena(struct pci_dev *dev)
{
	struct dev_drv_data *dd;

	dd = pci_get_drvdata(dev);

	/*
	 * XXX: only verde chips have volt/pw gating, and the only block stable
	 * enough to be enabled is the one of the dma discret engines.
	 */

	wr32(dev, dd->rlc.save_restore >> 8, RLC_SAVE_RESTORE_BASE);
	wr32(dev, dd->rlc.clr_restore >> 8, RLC_CLR_RESTORE_BASE);
}

void vg_dis(struct pci_dev *dev)
{
	/* XXX: see note above */
}
