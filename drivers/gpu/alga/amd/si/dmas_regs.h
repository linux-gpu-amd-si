#ifndef REGS_DMA_H
#define REGS_DMA_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/* ASYNC DMA - first instance at 0xd000, second at 0xd800 */
#define DMA0_REG_OF 0x0 
#define DMA1_REG_OF 0x800

#define DMA_RB_CTL				0xd000
#define		DRC_RB_ENA				BIT(0)
/* log2 */
#define		DRC_RB_SZ 				0x000001fe
#define		DRC_RB_SWAP_ENA				BIT(9)
#define		DRC_RPTR_WRBACK_ENA			BIT(12)
#define		DRC_RPTR_WRBACK_SWAP_ENA		BIT(13)
/* log2 */
#define		DRC_RPTR_WRBACK_TIMER			0xffff0000 
#define DMA_RB_BASE				0xd004
#define DMA_RB_RPTR				0xd008
#define DMA_RB_WPTR				0xd00c

#define DMA_RB_RPTR_ADDR_HI			0xd01c
#define DMA_RB_RPTR_ADDR_LO			0xd020
#define DMA_IB_CTL				0xd024
#define		DIC_DMA_IB_ENA				BIT(0)
#define		DIC_DMA_IB_SWAP_ENA			BIT(4)
#define		DIC_CMD_VMID_FORCE			BIT(31)
#define DMA_IB_RPTR				0xd028
#define DMA_CTL					0xd02c
#define		DC_TRAP_ENA				BIT(0)
#define		DC_SEM_INCOMPLETE_INT_ENA		BIT(1)
#define		DC_SEM_WAIT_INT_ENA			BIT(2)
#define		DC_DATA_SWAP_ENA			BIT(3)
#define		DC_FENCE_SWAP_ENA			BIT(4)
#define		DC_CTX_EMPTY_INT_ENA			BIT(28)
#define DMA_STATUS				0xd034
#define		DSR_DMA_IDLE				BIT(0)
#define DMA_SEM_INCOMPLETE_TIMER_CTL		0xd044
#define DMA_SEM_WAIT_FAIL_TIMER_CTL		0xd048

#define DMA_TILING_CFG				0xd0b8
#define	DMA_PWR_CTL				0xd0bc
#define		DPC_MEM_PWR_OVERRIDE			BIT(8)
#define	DMA_CLK_CTL				0xd0c0
#define	DMA_PG					0xd0d4
#define		DP_PG_CTL_ENA				BIT(0)

#define	DMA_PGFSM_CFG				0xd0d8
#define	DMA_PGFSM_WR				0xd0dc

#define DMA_PKT_SZ_MAX 0xfffff 
#define DMA_PKT(cmd, b, t, s, n)	((((cmd) & 0xf) << 28) | \
					 (((b) & 0x1) << 26) | \
					 (((t) & 0x1) << 23) | \
					 (((s) & 0x1) << 22) | \
					 (((n) & 0xfffff) << 0))

#define DMA_IB_PKT(cmd, vmid, n)	((((cmd) & 0xf) << 28) | \
					 (((vmid) & 0xf) << 20) | \
					 (((n) & 0xfffff) << 0))

#define DMA_PTE_PDE_PKT(n)		((2 << 28) | \
					 (1 << 26) | \
					 (1 << 21) | \
					 (((n) & 0xfffff) << 0))

/* async DMA Packet types */
#define	DMA_PKT_WR		0x2
#define	DMA_PKT_CPY		0x3
#define	DMA_PKT_INDIRECT_BUF	0x4
#define	DMA_PKT_SEM		0x5
#define	DMA_PKT_FENCE		0x6
#define	DMA_PKT_TRAP		0x7
#define	DMA_PKT_SRBM_WR		0x9
#define	DMA_PKT_U32_FILL	0xd
#define	DMA_PKT_NOP		0xf

/* used the upstream driver location in wb page */

/* resulting wb address must be dword aligned */
static u32 wb_dma_rptr_of[DMAS_N] __maybe_unused = {
	WB_DMA_0_RPTR_OF,
	WB_DMA_1_RPTR_OF
};

static u32 wb_dma_fence_of[DMAS_N] __maybe_unused = {
	WB_DMA_0_FENCE_OF,
	WB_DMA_1_FENCE_OF
};

/* back to registers */

static u32 regs_dma_rb_ctl[DMAS_N] __maybe_unused = {
	DMA_RB_CTL + DMA0_REG_OF,
	DMA_RB_CTL + DMA1_REG_OF
};

static u32 regs_dma_rb_base[DMAS_N] __maybe_unused = {
	DMA_RB_BASE + DMA0_REG_OF,
	DMA_RB_BASE + DMA1_REG_OF
};

static u32 regs_dma_rb_rptr[DMAS_N] __maybe_unused = {
	DMA_RB_RPTR + DMA0_REG_OF,
	DMA_RB_RPTR + DMA1_REG_OF
};

static u32 regs_dma_rb_wptr[DMAS_N] __maybe_unused = {
	DMA_RB_WPTR + DMA0_REG_OF,
	DMA_RB_WPTR + DMA1_REG_OF
};

static u32 regs_dma_rb_rptr_addr_hi[DMAS_N] __maybe_unused = {
	DMA_RB_RPTR_ADDR_HI + DMA0_REG_OF,
	DMA_RB_RPTR_ADDR_HI + DMA1_REG_OF
};

static u32 regs_dma_rb_rptr_addr_lo[DMAS_N] __maybe_unused = {
	DMA_RB_RPTR_ADDR_LO + DMA0_REG_OF,
	DMA_RB_RPTR_ADDR_LO + DMA1_REG_OF
};

static u32 regs_dma_ib_ctl[DMAS_N] __maybe_unused = {
	DMA_IB_CTL + DMA0_REG_OF,
	DMA_IB_CTL + DMA1_REG_OF
};

static u32 regs_dma_ib_rptr[DMAS_N] __maybe_unused = {
	DMA_IB_RPTR + DMA0_REG_OF,
	DMA_IB_RPTR + DMA1_REG_OF
};

static u32 regs_dma_ctl[DMAS_N] __maybe_unused = {
	DMA_CTL + DMA0_REG_OF,
	DMA_CTL + DMA1_REG_OF
};

static u32 regs_dma_status[DMAS_N] __maybe_unused = {
	DMA_STATUS + DMA0_REG_OF,
	DMA_STATUS + DMA1_REG_OF
};

static u32 regs_dma_sem_incomplete_timer_ctl[DMAS_N] __maybe_unused = {
	DMA_SEM_INCOMPLETE_TIMER_CTL + DMA0_REG_OF,
	DMA_SEM_INCOMPLETE_TIMER_CTL + DMA1_REG_OF
};

static u32 regs_dma_sem_wait_fail_timer_ctl[DMAS_N] __maybe_unused = {
	DMA_SEM_WAIT_FAIL_TIMER_CTL + DMA0_REG_OF,
	DMA_SEM_WAIT_FAIL_TIMER_CTL + DMA1_REG_OF
};

static u32 regs_dma_tiling_cfg[DMAS_N] __maybe_unused = {
	DMA_TILING_CFG + DMA0_REG_OF,
	DMA_TILING_CFG + DMA1_REG_OF
};

static u32 regs_dma_pg[DMAS_N] __maybe_unused = {
	DMA_PG + DMA0_REG_OF,
	DMA_PG + DMA1_REG_OF
};

static u32 regs_dma_pgfsm_cfg[DMAS_N] __maybe_unused = {
	DMA_PGFSM_CFG + DMA0_REG_OF,
	DMA_PGFSM_CFG + DMA1_REG_OF
};

static u32 regs_dma_pgfsm_wr[DMAS_N] __maybe_unused = {
	DMA_PGFSM_WR + DMA0_REG_OF,
	DMA_PGFSM_WR + DMA1_REG_OF
};

static u32 regs_dma_pwr_ctl[DMAS_N] __maybe_unused = {
	DMA_PWR_CTL + DMA0_REG_OF,
	DMA_PWR_CTL + DMA1_REG_OF
};

static u32 regs_dma_clk_ctl[DMAS_N] __maybe_unused = {
	DMA_CLK_CTL + DMA0_REG_OF,
	DMA_CLK_CTL + DMA1_REG_OF
};
#endif
