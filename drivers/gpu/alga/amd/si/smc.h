#ifndef SMC_H
#define SMC_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
u32 smc_r32(struct pci_dev *dev, u32 addr);
void smc_w32(struct pci_dev *dev, u32 val, u32 addr);
u8 smc_is_running(struct pci_dev *dev);
void smc_initial_jmp_setup(struct pci_dev *dev);
void smc_start(struct pci_dev *rdev);
void smc_reset(struct pci_dev *dev);
void smc_clk_start(struct pci_dev *rdev);
void smc_clk_stop(struct pci_dev *dev);
long smc_ucode_load(struct pci_dev *dev);
void smc_ucode_program(struct pci_dev *dev);
u8 smc_tbls_cur_arb_set_get(struct pci_dev *dev);
u32 smc_sw_r32(struct pci_dev *dev, u16 of);
void smc_sw_wr32(struct pci_dev *dev, u32 val, u16 of);
void smc_auto_increment_ena(struct pci_dev *dev);
void smc_auto_increment_dis(struct pci_dev *dev);
void smc_memcpy_to(struct pci_dev *dev, u32 dest, u8 *src, u32 bytes_n);
void smc_memcpy_from(struct pci_dev *dev, u8 *dest, u32 src, u32 bytes_n);
void smc_halt_wait(struct pci_dev *dev);

#define SMC_MSG_HALT			0x0010
#define SMC_MSG_RESUME			0x0011
#define SMC_MSG_SW_STATE_SWITCH 	0x0020
#define SMC_MSG_NO_FORCE_LVL		0x0041
#define SMC_MSG_CAC_ENA			0x0053
#define SMC_MSG_CAC_DIS			0x0054
#define SMC_TDP_CLAMPING_ON 		0x0059
#define SMC_TDP_CLAMPING_OFF		0x005a
#define SMC_MSG_NO_DISPLAY		0x005d
#define SMC_MSG_HAS_DISPLAY		0x005e
#define SMC_MSG_UVD_PWR_OFF		0x0060
#define SMC_MSG_UVD_PWR_ON		0x0061
#define SMC_MSG_ULV_ENA			0x0062
#define SMC_MSG_ULV_DIS			0x0063
#define SMC_MSG_ULV_ENTER		0x0064
#define SMC_MSG_ULV_EXIT		0x0065
#define SMC_MSG_CAC_LONG_TERM_AVG_ENA	0x006e
#define SMC_MSG_CAC_LONG_TERM_AVG_DIS	0x006f
#define SMC_MSG_DATA_CACHE_FLUSH	0x0080
#define SMC_MSG_ENA_LVLS_N_SET		0x0082
#define SMC_MSG_DTE_ENA			0x0087
#define SMC_MSG_DTE_DIS			0x0088
long smc_msg(struct pci_dev *dev, u16 msg);
long smc_msg_param(struct pci_dev *dev, u16 msg, u32 param);
#endif
