#ifndef RCL_RCL_H
#define RLC_RCL_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct rlc {
	u64 save_restore;	/* gpu address */
	u64 clr_restore;	/* gpu address */
};

long rlc_ucode_load(struct pci_dev *dev);
void rlc_ucode_program(struct pci_dev *dev);
void rlc_shutdown(struct pci_dev *dev);
void rlc_ena(struct pci_dev *dev);
long rlc_init(struct pci_dev *dev);
void rlc_cleanup(struct pci_dev *dev);
void rlc_serdes_wait(struct pci_dev *dev);
void rlc_wait(struct pci_dev *dev);
void rlc_reset(struct pci_dev *dev);
void rlc_update_ctl(struct pci_dev *dev, u32 prev_rlc_ctl);

void rlc_lb_pw_ena(struct pci_dev *dev);
void rlc_lb_pw_dis(struct pci_dev *dev);

void rlc_mgcg_ena(struct pci_dev *dev);
void rlc_mgcg_dis(struct pci_dev *dev);
void rlc_cgcg_ena(struct pci_dev *dev);

void rlc_serdes_mgcg_dis(struct pci_dev *dev);
void rlc_serdes_mgcg_ena(struct pci_dev *dev);
void rlc_serdes_cgcg_ena(struct pci_dev *dev);
#endif
