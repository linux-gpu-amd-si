/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#include <linux/pci.h>
#include <linux/cdev.h>

#include <alga/rng_mng.h>
#include <uapi/alga/pixel_fmts.h>
#include <alga/timing.h>
#include <alga/amd/atombios/atb.h>
#include <uapi/alga/amd/dce6/dce6.h>

#include <uapi/alga/amd/si/pkt.h>

#include "mc.h"
#include "rlc.h"
#include "ih.h"
#include "fence.h"
#include "ring.h"
#include "dmas.h"
#include "ba.h"
#include "cps.h"
#include "gpu.h"
#include "drv.h"

#include "regs.h"

#include "tiling.h"

void tiling_modes_tbl_init(struct pci_dev *dev, u32 mem_row_sz_kb)
{
	struct dev_drv_data *dd;
	u32 reg_of;
	u32 gb_tile_mode_xx;
	u32 split_equal_to_row_sz;

	dd = pci_get_drvdata(dev);

	switch (mem_row_sz_kb) {
	case 1:
		split_equal_to_row_sz = GTM_ADDR_SURF_TILE_SPLIT_1KB;
		break;
	case 2:
	default:
		split_equal_to_row_sz = GTM_ADDR_SURF_TILE_SPLIT_2KB;
		break;
	case 4:
		split_equal_to_row_sz = GTM_ADDR_SURF_TILE_SPLIT_4KB;
		break;
	}

	if ((dd->family == TAHITI) || (dd->family == PITCAIRN)) {
		for (reg_of = 0; reg_of < TILE_MODE_STATES_N; ++reg_of) {
			switch (reg_of) {
			/* non-aa compressed depth or any compressed stencil */
			case 0:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DEPTH_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_64B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* 2xaa/4xaa compressed depth only */
			case 1:
			/*
			 * 2xaa/4xaa compressed depth with stencil (for depth
			 * buffer)
			 */
			case 3:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DEPTH_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_128B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* 8xaa compressed depth only */
			case 2:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DEPTH_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/*
			 * maps w/ a dimension less than the 2d macro-tile
			 * dimensions (for mipmapped depth textures)
			 */
			case 4:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_1D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DEPTH_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_64B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/*
			 * uncompressed 16bpp depth - and stencil buffer
			 * allocated with it
			 */
			case 5:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DEPTH_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, split_equal_to_row_sz)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/*
			 * uncompressed 32bpp depth - and stencil buffer
			 * allocated with it
			 */
			case 6:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DEPTH_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, split_equal_to_row_sz)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_1)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_1);
				break;
			/*
			 * uncompressed 8bpp stencil without depth (drivers
			 * typically do not use)
			 */
			case 7:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DEPTH_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, split_equal_to_row_sz)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* 1d and 1d array surfaces */
			case 8:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_LINEAR_ALIGNED)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DISP_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_64B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* displayable maps */
			case 9:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_1D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DISP_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_64B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* display 8bpp */
			case 10:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DISP_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* display 16bpp */
			case 11:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DISP_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* display 32bpp */
			case 12:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DISP_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_512B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_1)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_1);
				break;
			/* thin */
			case 13:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_1D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_64B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* thin 8 bpp */
			case 14:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_1);
				break;
			/* thin 16 bpp */
			case 15:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_1);
				break;
			/* thin 32 bpp */
			case 16:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_512B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_1)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_1);
				break;
			/* thin 64 bpp */
			case 17:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, split_equal_to_row_sz)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_1)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_1);
				break;
			/* 8 bpp prt */
			case 21:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_2)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* 16 bpp prt */
			case 22:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_4);
				break;
			/* 32 bpp prt */
			case 23:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* 64 bpp prt */
			case 24:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_512B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_1)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* 128 bpp prt */
			case 25:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_1KB)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_08_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_1)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_1);
				break;
			default:
				gb_tile_mode_xx = 0;
				break;
			}
			wr32(dev, gb_tile_mode_xx, GB_TILE_MODE_00
								+ reg_of * 4);
		}
	} else if ((dd->family == VERDE) || (dd->family == OLAND)) {
		for (reg_of = 0; reg_of < TILE_MODE_STATES_N; ++reg_of) {
			switch (reg_of) {
			/* non-aa compressed depth or any compressed stencil */
			case 0:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DEPTH_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_64B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_4);
				break;
			/* 2xaa/4xaa compressed depth only */
			case 1:  
			/*
			 * 2xaa/4xaa compressed depth with stencil (for depth
			 * buffer)
			 */
			case 3:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DEPTH_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_128B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_4);
				break;
			/* 8xaa compressed depth only */
			case 2:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DEPTH_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_4);
				break;
			/*
			 * maps w/ a dimension less than the 2D macro-tile
			 * dimensions (for mipmapped depth textures)
			 */
			case 4:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_1D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DEPTH_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_64B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/*
			 * uncompressed 16bpp depth - and stencil bufferxi
			 * allocated with it
			 */
			case 5:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DEPTH_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, split_equal_to_row_sz)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/*
			 * uncompressed 32bpp depth - and stencil buffer
			 * allocated with it
			 */
			case 6:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DEPTH_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, split_equal_to_row_sz)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_1)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/*
			 * uncompressed 8bpp stencil without depth (drivers
			 * typically do not use)
			 */
			case 7:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DEPTH_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, split_equal_to_row_sz)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_4);
				break;
			/* 1d and 1d array surfaces */
			case 8:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_LINEAR_ALIGNED)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DISP_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_64B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* displayable maps */
			case 9:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_1D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DISP_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_64B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* display 8bpp */
			case 10:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_1D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DISP_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_4);
				break;
			/* display 16bpp */
			case 11:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DISP_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* display 32bpp */
			case 12:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_DISP_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_512B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_1)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* thin */
			case 13:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_1D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_64B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* thin 8 bpp */
			case 14:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* thin 16 bpp */
			case 15:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* thin 32 bpp */
			case 16:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_512B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_1)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* thin 64 bpp */
			case 17:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P4_8x16)
		| set(GTM_TILE_SPLIT, split_equal_to_row_sz)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_1)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* 8 bpp prt */
			case 21:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_2)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* 16 bpp prt */
			case 22:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_4)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_4);
				break;
			/* 32 bpp prt */
			case 23:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_256B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_2)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* 64 bpp prt */
			case 24:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_512B)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_16_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_1)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_2);
				break;
			/* 128 bpp prt */
			case 25:
				gb_tile_mode_xx =
		set(GTM_ARRAY_MODE, GTM_ARRAY_2D_TILED_THIN1)
		| set(GTM_MICRO_TILE_MODE, GTM_ADDR_SURF_THIN_MICRO_TILING)
		| set(GTM_PIPE_CFG, GTM_ADDR_SURF_P8_32x32_8x16)
		| set(GTM_TILE_SPLIT, GTM_ADDR_SURF_TILE_SPLIT_1KB)
		| set(GTM_BANKS_N, GTM_ADDR_SURF_08_BANK)
		| set(GTM_BANK_W, GTM_ADDR_SURF_BANK_W_1)
		| set(GTM_BANK_H, GTM_ADDR_SURF_BANK_H_1)
		| set(GTM_MACRO_TILE_ASPECT, GTM_ADDR_SURF_MACRO_ASPECT_1);
				break;
			default:
				gb_tile_mode_xx = 0;
				break;
			}
			wr32(dev, gb_tile_mode_xx, GB_TILE_MODE_00
								+ reg_of * 4);
		}
	}
}
