#ifndef RLC_REGS_H
#define RLC_REGS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define RLC_CTL					0xc300
#define		RC_RLC_ENA				BIT(0)
#define RLC_RL_BASE				0xc304
#define RLC_RL_SZ				0xc308
#define RLC_LB_CTL				0xc30c
#define		RLC_LB_ENA				BIT(0)
#define RLC_SAVE_RESTORE_BASE			0xc310
#define RLC_LB_CNTR_MAX				0xc314
#define RLC_LB_CNTR_INIT			0xc318

#define RLC_CLR_RESTORE_BASE			0xc320

#define RLC_UCODE_ADDR				0xc32c
#define RLC_UCODE_DATA				0xc330

#define RLC_GPU_CLK_CNT_LSB			0xc338
#define RLC_GPU_CLK_CNT_MSB			0xc33c
#define RLC_CAPTURE_GPU_CLK_CNT			0xc340
#define RLC_MC_CTL				0xc344
#define RLC_UCODE_CTL				0xc348
#define RLC_STAT				0xc34c
#define		RS_RLC_BUSY_STATUS			BIT(0)
#define		RS_GFX_PWR_STATUS			BIT(1)
#define		RS_GFX_CLK_STATUS			BIT(2)
#define		RS_GFX_LS_STATUS			BIT(3)

#define	RLC_CGTT_MGCG_OVERRIDE			0xc400
#define	RLC_CGCG_CGLS_CTL			0xc404
#define		RCCC_CGCG_ENA				BIT(0)
#define		RCCC_CGLS_ENA				BIT(1)

#define RLC_SERDES_WR_MASTER_MASK_0		0xc454
#define RLC_SERDES_WR_MASTER_MASK_1		0xc458
#define RLC_SERDES_WR_CTL			0xc45c

#define RLC_SERDES_MASTER_BUSY_0		0xc464
#define RLC_SERDES_MASTER_BUSY_1		0xc468

#define RLC_GCPM_GENERAL_3			0xc478
#endif
