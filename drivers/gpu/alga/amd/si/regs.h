#ifndef REGS_H
#define REGS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/*
 * Many masks are too large because we need the info from AMD
 * or need to deduce it from other values in the register.
 * Components of a register are prefix with the initals of that register in
 * order to get a kind of unicity
 */

static inline u32 set(u32 mask, u32 v)
{
	u8 shift;

	shift = ffs(mask) - 1;
	return (v << shift) & mask;
}

static inline u32 get(u32 mask, u32 v)
{
	u8 shift;

	shift = ffs(mask) - 1;
	return (v & mask) >> shift;
}

#define MM_IDX					0x0
#define		MI_VRAM					BIT(31)
#define MM_DATA					0x4

#define MM_IDX_HI				0x18

#define DCCG_DISP_SLOW_SELECT			0x4fc
#define		DDSSR_DISP1_SLOW_SELECT			0x00000007
#define		DDSSR_DISP2_SLOW_SELECT			0x00000070

#define	CG_ENG_PLL_FUNC_CTL_0			0x600
#define		CEPFC0_RESET				BIT(0)
#define		CEPFC0_SLEEP				BIT(1)
#define		CEPFC0_BYPASS_ENA			BIT(3)
#define		CEPFC0_REF_DIV				0x000003f0
#define		CEPFC0_PDIV_A				0x07f00000
#define	CG_ENG_PLL_FUNC_CTL_1			0x604
#define		CEPFC1_MUX_SEL				0x000001ff
#define	CG_ENG_PLL_FUNC_CTL_2			0x608
#define		CEPFC2_FB_DIV				0x03ffffff
#define		CEPFC2_DITH_ENA				BIT(28)
#define	CG_ENG_PLL_FUNC_CTL_3			0x60c

#define	CG_ENG_PLL_SS_0				0x620
#define		CEPS0_SS_ENA				BIT(0)
#define		CEPS0_CLK_S				0x0000fff0
#define	CG_ENG_PLL_SS_1				0x624
#define		CEPS1_CLK_V				0x03ffffff

#define	CG_ENG_PLL_AUTOSCALE_CTL		0x62c
#define		CEPAC_AUTOSCALE_ON_SS_CLR		BIT(9)

#define CG_CLK_PIN_CTL_0			0x660
#define		CCC0_XTAL_IN_DIVIDE			BIT(1)
#define		CCC0_BCLK_AS_XCLK			BIT(2)
#define CG_CLK_PIN_CTL_1			0x664
#define		CCC1_FORCE_BIF_REFCLK_ENA		BIT(3)
#define		CCC1_MUX_TCLK_TO_XCLK			BIT(8)

#define	MISC_CLK_CTL				0x670
#define		MCC_DEEP_SLEEP_CLK_SEL			0x000000ff
#define		MCC_ZCLK_SEL				0x0000ff00

#define	CG_THERMAL_CTL				0x700
#define		CTC_DYN_PM_EVT_SRC			0x00000007
#define			CTC_EVT_SRC_ANALOG			0
#define			CTC_EVT_SRC_EXTERNAL			1
#define			CTC_EVT_SRC_DIGITAL			2
#define			CTC_EVT_SRC_ANALOG_OR_EXTERNAL		3
#define			CTC_EVT_SRC_DIGITAL_OR_EXTERNAL		4
#define		CTC_DYN_PM_DIGITAL_TEMP_HIGH		0x003fc000

#define	CG_THERMAL_INT				0x708
#define		CTI_TEMP_HIGH				0x0000ff00
#define		CTI_TEMP_LOW				0x00ff0000
#define		CTI_TEMP_HIGH_INT_ENA			BIT(24)
#define		CTI_TEMP_LOW_INT_ENA			BIT(25)

#define GENERAL_PM				0x780
#define		GP_GLOBAL_PM_ENA			BIT(0)
#define		GP_STATIC_PM_ENA			BIT(1)
#define		GP_THERMAL_PROTECTION_DIS		BIT(2)
#define		GP_THERMAL_PROTECTION_TYPE		BIT(3)
/* XXX: only one bit? */
#define		GP_SW_SMIO_IDX				BIT(6)
#define		GP_VOLT_PM_ENA	 			BIT(10)
#define		GP_DYN_SPREAD_SPECTRUM_ENA		BIT(23)
#define CG_TPC					0x784
#define ENG_CLK_PM_CTL				0x788
#define		ECPC_ENG_CLK_PM_OFF			BIT(0)
#define		ECPC_ENG_CLK_LOW_D1			BIT(1)
#define		ECPC_FIR_RESET				BIT(4)
#define		ECPC_FIR_FORCE_TREND_SEL		BIT(5)
#define		ECPC_FIR_TREND_MODE			BIT(6)
#define		ECPC_DYN_GFX_CLK_OFF_ENA		BIT(7)
#define		ECPC_GFX_CLK_FORCE_ON			BIT(8)
#define		ECPC_GFX_CLK_REQ_OFF			BIT(9)
#define		ECPC_GFX_CLK_FORCE_OFF			BIT(10)
#define		ECPC_GFX_CLK_OFF_ACPI_D1		BIT(11)
#define		ECPC_GFX_CLK_OFF_ACPI_D2		BIT(12)
#define		ECPC_GFX_CLK_OFF_ACPI_D3		BIT(13)
#define		ECPC_DYN_LIGHT_SLEEP_ENA		BIT(14)

#define CG_FTV					0x7bc
#define CG_FFCT_00				0x7c0
#define		CF_UTC_0				0x000003ff
#define		CF_DTC_0				0x000ffc00
#define CG_FFCT_01				0x7c4
#define CG_FFCT_02				0x7c8
#define CG_FFCT_03				0x7cc
#define CG_FFCT_04				0x7d0
#define CG_FFCT_05				0x7d4
#define CG_FFCT_06				0x7d8
#define CG_FFCT_07				0x7dc
#define CG_FFCT_08				0x7e0
#define CG_FFCT_09				0x7e4
#define CG_FFCT_10				0x7e8
#define CG_FFCT_11				0x7ec
#define CG_FFCT_12				0x7f0
#define CG_FFCT_13				0x7c4
#define CG_FFCT_14				0x7c8
#define CG_B_SP					0x7fc
#define		CB_BS_P					0x0000ffff
#define		CB_BS_U					0x000f0000
#define CG_A_T					0x800
#define		CAT_CG_R				0x0000ffff
#define		CAT_CG_L				0xffff0000
#define CG_GIT					0x804
#define		CG_GICST				0x0000ffff
#define		CG_GIPOT				0xffff0000

#define CG_SSP					0x80c
#define		CS_SST					0x0000ffff
#define		CS_SSTU					0x000f0000

#define CG_DISP_GAP_CTL				0x828
#define		CDGC_DISP1_GAP				0x00000003
#define		CDGC_DISP2_GAP				0x0000000c
#define			CDGC_VBLANK_OR_WATERMARK		0
#define			CDGC_VBLANK				1
#define			CDGC_WATERMARK				2
#define			CDGC_IGNORE				3
#define		CDGC_VBI_TIMER_CNT			0x0003fff0
#define		CDGC_VBI_TIMER_UNIT			0x00700000
#define		CDGC_DISP1_GAP_MCHG			0x03000000
#define		CDGC_DISP2_GAP_MCHG			0x0c000000

#define	CG_ULV_CTL				0x878
#define	CG_ULV_PARAM				0x87c

#define	CG_CAC_CTL				0x8b8
#define		CCC_CAC_WND				0x00ffffff

#define	SRBM_STATUS				0xe50
#define 	SS_MC_STATUS				0x00001f00

#define	SRBM_SOFT_RESET				0xe60
#define		SSR_SOFT_RESET_BIF			BIT(1)
#define		SSR_SOFT_RESET_DC			BIT(5)
#define		SSR_SOFT_RESET_DMA1			BIT(6)
#define		SSR_SOFT_RESET_GRBM			BIT(8)
#define		SSR_SOFT_RESET_HDP			BIT(9)
#define		SSR_SOFT_RESET_IH			BIT(10)
#define		SSR_SOFT_RESET_MC			BIT(11)
#define		SSR_SOFT_RESET_ROM			BIT(14)
#define		SSR_SOFT_RESET_SEM			BIT(15)
#define		SSR_SOFT_RESET_VMC			BIT(17)
#define		SSR_SOFT_RESET_DMA0			BIT(20)
#define		SSR_SOFT_RESET_TST			BIT(21)
#define		SSR_SOFT_RESET_REGBB			BIT(22)
#define		SSR_SOFT_RESET_ORB			BIT(23)

#define VM_L2_CTL_0				0x1400
#define		VLC_ENA_L2_CACHE			BIT(0)
#define		VLC_ENA_L2_FRAG_PROCESSING		BIT(1)
#define		VLC_L2_CACHE_PTE_ENDIAN_SWAP_MODE	0xfffffffc
#define		VLC_L2_CACHE_PDE_ENDIAN_SWAP_MODE	0xfffffff0
#define		VLC_ENA_L2_PTE_CACHE_LRU_UPDATE_BY_WR	BIT(9)
#define		VLC_ENA_L2_PDE0_CACHE_LRU_UPDATE_BY_WR	BIT(10)
#define		VLC_EFFECTIVE_L2_QUEUE_SZ		0x00038000
#define		VLC_CTX1_IDENTITY_ACCESS_MODE		0x00180000
#define VM_L2_CTL_1				0x1404
#define		VLC_INVALIDATE_ALL_L1_TLBS		BIT(0)
#define		VLC_INVALIDATE_L2_CACHE			BIT(1)
#define		VLC_INVALIDATE_CACHE_MODE		0xfc000000
#define			VLC_INVALIDATE_PTE_AND_PDE_CACHES		0
#define			VLC_INVALIDATE_ONLY_PTE_CACHES			1
#define			VLC_INVALIDATE_ONLY_PDE_CACHES			2
#define VM_L2_CTL_2				0x1408
#define		VLC_BANK_SELECT				0xffffffff
#define		VLC_L2_CACHE_UPDATE_MODE		0xffffffc0
#define		VLC_L2_CACHE_BIGK_FRAGMENT_SZ		0xffff8000
#define		VLC_L2_CACHE_BIGK_ASSOCIATIVITY		BIT(20)

#define	VM_L2_STATUS				0x140c
#define		VLS_L2_BUSY				BIT(0)

#define VM_CTX_0_CTL_0				0x1410
#define		VCC_ENA_CTX					BIT(0)
#define		VCC_PT_DEPTH					0x00000006
#define		VCC_RANGE_PROTECTION_FAULT_ENA_INT		BIT(3)
#define		VCC_RNG_PROTECTION_FAULT_ENA_DEFAULT		BIT(4)
#define		VCC_DUMMY_PAGE_PROTECTION_FAULT_ENA_INT		BIT(6)
#define		VCC_DUMMY_PAGE_PROTECTION_FAULT_ENA_DEFAULT	BIT(7)
#define		VCC_PDE0_PROTECTION_FAULT_ENA_INT		BIT(9)
#define		VCC_PDE0_PROTECTION_FAULT_ENA_DEFAULT		BIT(10)
#define		VCC_VALID_PROTECTION_FAULT_ENA_INT		BIT(12)
#define		VCC_VALID_PROTECTION_FAULT_ENA_DEFAULT		BIT(13)
#define		VCC_RD_PROTECTION_FAULT_ENA_INT			BIT(15)
#define		VCC_RD_PROTECTION_FAULT_ENA_DEFAULT		BIT(16)
#define		VCC_WR_PROTECTION_FAULT_ENA_INT			BIT(18)
#define		VCC_WR_PROTECTION_FAULT_ENA_DEFAULT		BIT(19)
#define VM_CTX_1_CTL_0				0x1414
#define VM_CTX_0_CTL_1				0x1430
#define VM_CTX_1_CTL_1				0x1434
#define	VM_CTX_8_PT_BASE_ADDR			0x1438
#define	VM_CTX_9_PT_BASE_ADDR			0x143c
#define	VM_CTX_A_PT_BASE_ADDR			0x1440
#define	VM_CTX_B_PT_BASE_ADDR			0x1444
#define	VM_CTX_C_PT_BASE_ADDR			0x1448
#define	VM_CTX_D_PT_BASE_ADDR			0x144c
#define	VM_CTX_E_PT_BASE_ADDR			0x1450
#define	VM_CTX_F_PT_BASE_ADDR			0x1454

#define VM_INVALIDATE_REQ			0x1478
#define VM_INVALIDATE_RESP			0x147c

#define VM_CTX_0_PROTECTION_FAULT_DEFAULT_ADDR	0x1518
#define VM_CTX_1_PROTECTION_FAULT_DEFAULT_ADDR	0x151c
#define	VM_CTX_0_PT_BASE_ADDR			0x153c
#define	VM_CTX_1_PT_BASE_ADDR			0x1540
#define	VM_CTX_2_PT_BASE_ADDR			0x1544
#define	VM_CTX_3_PT_BASE_ADDR			0x1548
#define	VM_CTX_4_PT_BASE_ADDR			0x154c
#define	VM_CTX_5_PT_BASE_ADDR			0x1550
#define	VM_CTX_6_PT_BASE_ADDR			0x1554
#define	VM_CTX_7_PT_BASE_ADDR			0x1558
#define	VM_CTX_0_PT_START_ADDR			0x155c
#define	VM_CTX_1_PT_START_ADDR			0x1560
#define	VM_CTX_2_PT_START_ADDR			0x1564
#define	VM_CTX_3_PT_START_ADDR			0x1568
#define	VM_CTX_4_PT_START_ADDR			0x156c
#define	VM_CTX_5_PT_START_ADDR			0x1570
#define	VM_CTX_6_PT_START_ADDR			0x1574
#define	VM_CTX_7_PT_START_ADDR			0x1578
#define	VM_CTX_0_PT_END_ADDR			0x157c
#define	VM_CTX_1_PT_END_ADDR			0x1580
#define	VM_CTX_2_PT_END_ADDR			0x1584
#define	VM_CTX_3_PT_END_ADDR			0x1588
#define	VM_CTX_4_PT_END_ADDR			0x158c
#define	VM_CTX_5_PT_END_ADDR			0x1590
#define	VM_CTX_6_PT_END_ADDR			0x1594
#define	VM_CTX_7_PT_END_ADDR			0x1598
#define	VM_CTX_8_PT_END_ADDR			0x159c
#define	VM_CTX_9_PT_END_ADDR			0x15a0
#define	VM_CTX_A_PT_END_ADDR			0x15a4
#define	VM_CTX_B_PT_END_ADDR			0x15a8
#define	VM_CTX_C_PT_END_ADDR			0x15ac
#define	VM_CTX_D_PT_END_ADDR			0x15b0
#define	VM_CTX_E_PT_END_ADDR			0x15b4
#define	VM_CTX_F_PT_END_ADDR			0x15b8

#define VM_L2_CG				0x15c0
/* bits common to many other cg related registers */
#define		VLC_MC_CG_ENA				BIT(18)
#define		VLC_MC_LS_ENA				BIT(19)

/* ??? */
/*0x15d4*/
/*0x15d8*/
/*0x15dc*/

#define MC_SHARED_CHMAP				0x2004
#define		MSC_CHANS_N				0x0000f000

#define	MC_VRAM_LOCATION			0x2024 /* 16MB aligned */
#define	MC_AGP_TOP				0x2028 /* 4MB aligned */
#define	MC_AGP_BOT				0x202c /* 4MB aligned */
#define	MC_AGP_BASE				0x2030
#define	MC_VM_SYS_APER_LOW_ADDR			0x2034
#define	MC_VM_SYS_APER_HIGH_ADDR		0x2038
#define	MC_VM_SYS_APER_DEFAULT_ADDR		0x203c

/* MX stands probably for MB and MD */
#define	MC_VM_MX_L1_TLB_CTL			0x2064
#define		MVMLTC_ENA_L1_TLB			BIT(0)
#define		MVMLTC_ENA_L1_FRAG_PROCESSING		BIT(1)
#define		MVMLTC_SYS_ACCESS_MODE			0x00000018
#define			MVMLTC_PA_ONLY				0
#define			MVMLTC_ALWAYS_USE_SYS_MAP		1
#define			MVMLTC_MAPPED_ACCESS_IS_IN_SYS_APER	2
#define			MVMLTC_MAPPED_ACCESS_NOT_IN_SYS		3	
#define		MVMLTC_SYS_APER_UNMAPPED_ACCESS		0xffffffe0
#define			MVMLTC_PASS_THRU			0
#define			MVMLTC_DISCARD_WR_RD_DEFAULT		1
#define		MVMLTC_ENA_ADVANCED_DRIVER_MODEL	BIT(6)

#define MC_SHARED_BLACKOUT_CTL			0x20ac
#define		MSBC_MODE				0x00000007
#define			MSBC_MODE_DIS				0
#define			MSBC_MODE_ENA				1

#define MC_HUB_MISC_HUB_CG			0x20b8
#define MC_HUB_MISC_VM_CG			0x20bc

#define MC_HUB_MISC_SIP_CG			0x20c0

#define MC_XPB_CG				0x2478

#define MC_CG_CFG				0x25bc
#define		MCC_MCDW_WR_ENA				BIT(0)
#define		MCC_MCDX_WR_ENA				BIT(1)
#define		MCC_MCDY_WR_ENA				BIT(2)
#define		MCC_MCDZ_WR_ENA				BIT(3)
#define		MCC_MC_RD_ENA				0x00000030
#define		MCC_IDX					0x0003ffc0

#define MC_CITF_MISC_RD_CG			0x2648
#define MC_CITF_MISC_WR_CG			0x264c
#define MC_CITF_MISC_VM_CG			0x2650

#define	MC_ARB_RAM_CFG				0x2760
#define		MARC_BANKS_N				0x00000003
#define		MARC_RANKS_N				0x00000004
/* log2 of 2^10 units, capped at 4 units (16384 rows) */
#define		MARC_ROWS_N				0x00000038
#define		MARC_COLS_N				0x000000c0
#define		MARC_CHAN_SZ				0x00000100
#define		MARC_BURST_LEN				0x00000200
#define		MARC_CHAN_SZ_OVERRIDE			BIT(11)
#define		MARC_GROUPS_N				0x00001000

#define	MC_ARB_DRAM_TIMING_0_0			0x2774
#define	MC_ARB_DRAM_TIMING_0_1			0x2778

#define MC_ARB_CG				0x27e8
#define		MAC_CG_ARB_REQ				0x000000ff
#define			MAC_MC_CG_ARB_FREQ_F0			0x0a
#define			MAC_MC_CG_ARB_FREQ_F1			0x0b
#define			MAC_MC_CG_ARB_FREQ_F2			0x0c
#define			MAC_MC_CG_ARB_FREQ_F3			0x0d
#define		MAC_CG_ARB_RESP				0x0000ff00
#define		MAC_ARB_CG_REQ				0x00ff0000
#define		MAC_ARB_CG_RESP				0xff000000

#define	MC_ARB_DRAM_TIMING_1_0			0x27f0
#define	MC_ARB_DRAM_TIMING_2_0			0x27f4
#define	MC_ARB_DRAM_TIMING_3_0			0x27f8
#define	MC_ARB_DRAM_TIMING_1_1			0x27fc
#define	MC_ARB_DRAM_TIMING_2_1			0x2800
#define	MC_ARB_DRAM_TIMING_3_1			0x2804
#define MC_ARB_BURST_TIME			0x2808
#define		MABT_STATE_0				0x0000001f
#define		MABT_STATE_1				0x000003e0
#define		MABT_STATE_2				0x00007c00
#define		MABT_STATE_3				0x000f8000

#define MC_SEQ_RAS_TIMING			0x28a0
#define MC_SEQ_CAS_TIMING			0x28a4
#define MC_SEQ_MISC_TIMING_0			0x28a8
#define MC_SEQ_MISC_TIMING_1			0x28ac
#define MC_SEQ_PMG_TIMING			0x28b0
#define MC_SEQ_RD_CTL_D0			0x28b4
#define MC_SEQ_RD_CTL_D1			0x28b8
#define MC_SEQ_WR_CTL_D0			0x28bc
#define MC_SEQ_WR_CTL_D1			0x28c0

#define MC_SEQ_SUP_CTL				0x28c8
#define		MSSC_RUN_MASK				BIT(0)
#define MC_SEQ_SUP_PGM				0x28cc
#define MC_SEQ_PMG_AUTO_CMD			0x28d0

#define	MC_SEQ_TRAIN_WAKEUP_CTL			0x28e8
#define		MSTWC_TRAIN_DONE_D0			BIT(30)
#define		MSTWC_TRAIN_DONE_D1			BIT(31)

#define MC_IO_PAD_CTL_D0			0x29d0
#define		MIPCD_MEM_FALL_OUT_CMD			BIT(8)

#define MC_SEQ_MISC_0				0x2a00
/* log2 of 2^3 units */
#define		MSM0_DRAM_REFRESH_RATE			0x00000003
#define		MSM0_VEN_ID				0x00000f00
#define			MSM0_VEN_ID_VAL				3
#define		MSM0_REV_ID				0x0000f000
#define			MSM0_REV_ID_VAL				1
#define		MSM0_GDDR5				0xf0000000
#define			MSM0_GDDR5_VAL				5
#define MC_SEQ_MISC_1				0x2a04
#define MC_SEQ_RESERVE_M			0x2a08
#define MC_SEQ_PMG_CMD_EMRS			0x2a0c

#define MC_SEQ_IO_DBG_IDX			0x2a44
#define MC_SEQ_IO_DBG_DATA			0x2a48

#define MC_SEQ_MISC_5				0x2a54
#define MC_SEQ_MISC_6				0x2a58

#define MC_SEQ_MISC_7				0x2a64

#define MC_SEQ_RAS_TIMING_LP			0x2a6c
#define MC_SEQ_CAS_TIMING_LP			0x2a70
#define MC_SEQ_MISC_TIMING_0_LP			0x2a74
#define MC_SEQ_MISC_TIMING_1_LP			0x2a78
#define MC_SEQ_WR_CTL_D0_LP			0x2a7c
#define MC_SEQ_WR_CTL_D1_LP			0x2a80
#define MC_SEQ_PMG_CMD_EMRS_LP			0x2a84
#define MC_SEQ_PMG_CMD_MRS_0_LP			0x2a88

#define MC_SEQ_PMG_CMD_MRS_0			0x2aac

#define MC_SEQ_RD_CTL_D0_LP			0x2b1c
#define MC_SEQ_RD_CTL_D1_LP			0x2b20

#define MC_SEQ_PMG_CMD_MRS_1			0x2b44
#define MC_SEQ_PMG_CMD_MRS_1_LP			0x2b48
#define MC_SEQ_PMG_TIMING_LP			0x2b4c

#define MC_SEQ_WR_CTL_2				0x2b54
#define MC_SEQ_WR_CTL_2_LP			0x2b58
#define MC_SEQ_PMG_CMD_MRS_2			0x2b5c
#define MC_SEQ_PMG_CMD_MRS_2_LP			0x2b60

#define	MEM_CLK_PM_CTL				0x2ba0
#define		MCPC_DLL_SPEED				0x0000001f
#define		MCPC_DLL_READY				BIT(6)
#define		MCPC_MC_INT_CTL				BIT(7)
#define		MCPC_MRDCK0_PDNB			BIT(8)
#define		MCPC_MRDCK1_PDNB			BIT(9)
#define		MCPC_MRDCK0_RESET			BIT(16)
#define		MCPC_MRDCK1_RESET			BIT(17)
#define DLL_READY_READ                           (1 << 24)
#define	DLL_CTL					0x2ba4
#define		DC_MRDCK0_BYPASS			BIT(24)
#define		DC_MRDCK1_BYPASS			BIT(25)

#define	MEM_PLL_FUNC_CTL_0			0x2bb4
#define		MPFC0_BW_CTL				0x0ff00000
#define	MEM_PLL_FUNC_CTL_1				0x2bb8
#define		MPFC1_VCO_MODE				0x00000003
#define		MPFC1_CLK_FRAC				0x0000fff0
#define		MPFC1_CLK_INTEGER			0x0fff0000
#define	MEM_PLL_FUNC_CTL_2			0x2bbc

#define	MEM_PLL_AD_FUNC_CTL			0x2bc0
#define		MPAFC_Y_CLK_POST_DIV			0x00000007
#define	MEM_PLL_DQ_FUNC_CTL			0x2bc4
#define		MPDFC_Y_CLK_POST_DIV			0x00000007
#define		MPDFC_Y_CLK_SEL				BIT(4)

#define	MEM_PLL_SS_0				0x2bcc
#define		MPS0_CLK_V				0x03ffffff
#define	MEM_PLL_SS_1				0x2bd0
#define		MPS1_CLK_S				0x00000fff

#define ATC_MISC_CG				0x3350

#define IH_RB_CTL				0x3e00
#define		IRC_IH_RB_ENA				BIT(0)
#define		IRC_IH_IB_LOG2_DWS			0xfffffffe
#define		IRC_IH_RB_FULL_DRAIN_ENA		BIT(6)
#define		IRC_IH_WPTR_WRITEBACK_ENA		BIT(8)
#define		IRC_IH_WPTR_WRITEBACK_TIMER		0xfffffe00
#define		IRC_IH_WPTR_OVERFLOW_ENA		BIT(16)
#define		IRC_IH_WPTR_OVERFLOW_CLR		BIT(31)
#define IH_RB_BASE				0x3e04
#define IH_RB_RPTR				0x3e08
#define IH_RB_WPTR				0x3e0c
#define		IRW_RB_OVERFLOW				BIT(0)
#define		IRW_WPTR_OF_MASK			0x3fffc
#define IH_RB_WPTR_ADDR_HI			0x3e10
#define IH_RB_WPTR_ADDR_LO			0x3e14
#define IH_CTL					0x3e18
#define		IC_ENA_INTR				BIT(0)
#define		IC_IH_MC_SWAP_MASK			0x00000006
#define		IC_IH_MC_SWAP_SHIFT			1
#define			IC_IH_MC_SWAP_NONE			0
#define			IC_IH_MC_SWAP_16BIT			1
#define			IC_IH_MC_SWAP_32BIT			2
#define			IC_IH_MC_SWAP_64BIT			3
#define		IC_RPTR_REARM				BIT(4)
#define		IC_MC_WR_REQ_CREDIT			0xffff8000
#define		IC_MC_WR_CLEAN_CNT			0xfff00000
#define		IC_MC_VM_ID				0xfe000000

#define	CFG_MEM_SZ				0x5428 /* unit is MB */

#define INTR_CTL_0				0x5468
#define		IC_IH_DUMMY_RD_OVERRIDE			BIT(0)
#define		IC_IH_DUMMY_RD_ENA			BIT(1)
#define		IC_IH_REQ_NONSNOOP_ENA			BIT(3)
#define		IC_GEN_IH_INT_ENA			BIT(8)
#define INTR_CTL_1				0x546c

#define DPG_PIPE_STUTTER_CTL			0x6cd4
#define		DPSC_STUTTER_ENA			BIT(0)

/* start of configuration register area: 0x8000-0xb000----------------------------------*/
#define	GRBM_CTL				0x8000
#define		GC_RD_TIMEOUT				0xffffffff

#define	GRBM_STATUS_1				0x8008
#define		GS_RLC_RQ_PENDING			BIT(0)
#define		GS_RLC_BUSY				BIT(8)
#define		GS_TC_BUSY				BIT(9)

#define	GRBM_STATUS_0				0x8010
#define		GS_CMDFIFO_AVAIL			0x0000000f
#define		GS_RING2_RQ_PENDING			BIT(4)
#define		GS_SRBM_RQ_PENDING			BIT(5)
#define		GS_RING1_RQ_PENDING			BIT(6)
#define		GS_CF_RQ_PENDING			BIT(7)
#define		GS_PF_RQ_PENDING			BIT(8)
#define		GS_GDS_DMA_RQ_PENDING			BIT(9)
#define		GS_GRBM_EE_BUSY				BIT(10)
#define		GS_DB_CLEAN				BIT(12)
#define		GS_CB_CLEAN				BIT(13)
#define		GS_TA_BUSY				BIT(14)
#define		GS_GDS_BUSY				BIT(15)
#define		GS_VGT_BUSY_NO_DMA			BIT(16)
#define		GS_VGT_BUSY				BIT(17)
#define		GS_IA_BUSY_NO_DMA			BIT(18)
#define		GS_IA_BUSY				BIT(19)
#define		GS_SX_BUSY				BIT(20)
#define		GS_SPI_BUSY				BIT(22)
#define		GS_BCI_BUSY				BIT(23)
#define		GS_SC_BUSY				BIT(24)
#define		GS_PA_BUSY				BIT(25)
#define		GS_DB_BUSY				BIT(26)
#define		GS_CP_COHERENCY_BUSY			BIT(28)
#define		GS_CP_BUSY				BIT(29)
#define		GS_CB_BUSY				BIT(30)
#define		GS_GUI_ACTIVE				BIT(31)
#define	GRBM_STATUS_SE_0			0x8014
#define	GRBM_STATUS_SE_1			0x8018
#define		GSS_DB_CLEAN				BIT(1)
#define		GSS_CB_CLEAN				BIT(2)
#define		GSS_BCI_BUSY				BIT(22)
#define		GSS_VGT_BUSY				BIT(23)
#define		GSS_PA_BUSY				BIT(24)
#define		GSS_TA_BUSY				BIT(25)
#define		GSS_SX_BUSY				BIT(26)
#define		GSS_SPI_BUSY				BIT(27)
#define		GSS_SC_BUSY				BIT(29)
#define		GSS_DB_BUSY				BIT(30)
#define		GSS_CB_BUSY				BIT(31)

#define	GRBM_SOFT_RESET				0x8020
#define		GSR_SOFT_RESET_CP			BIT(0)
#define		GSR_SOFT_RESET_CB			BIT(1)
#define		GSR_SOFT_RESET_RLC			BIT(2)
#define		GSR_SOFT_RESET_DB			BIT(3)
#define		GSR_SOFT_RESET_GDS			BIT(4)
#define		GSR_SOFT_RESET_PA			BIT(5)
#define		GSR_SOFT_RESET_SC			BIT(6)
#define		GSR_SOFT_RESET_BCI			BIT(7)
#define		GSR_SOFT_RESET_SPI			BIT(8)
#define		GSR_SOFT_RESET_SX			BIT(10)
#define		GSR_SOFT_RESET_TC			BIT(11)
#define		GSR_SOFT_RESET_TA			BIT(12)
#define		GSR_SOFT_RESET_VGT			BIT(14)
#define		GSR_SOFT_RESET_IA			BIT(15)

#define GRBM_GFX_IDX				0x802c
#define		GGI_INST_IDX				0xffffffff
#define		GGI_SH_IDX				0xffffff00
#define		GGI_SE_IDX				0xffff0000
#define		GGI_SH_BROADCAST_WRS			BIT(29)
#define		GGI_INST_BROADCAST_WRS			BIT(30)
#define		GGI_SE_BROADCAST_WRS			BIT(31)

#define GRBM_INT_CTL				0x8060
#define		GIC_RD_ERR_INT_ENA			BIT(0)
#define		GIC_GUI_IDLE_INT_ENA			BIT(19)

#define	SCRATCH_REG_0				0x8500
#define	SCRATCH_REG_1				0x8504
#define	SCRATCH_REG_2				0x8508
#define	SCRATCH_REG_3				0x850c
#define	SCRATCH_REG_4				0x8510
#define	SCRATCH_REG_5				0x8514
#define	SCRATCH_REG_6				0x8518
#define	SCRATCH_REG_7				0x851c

#define	SCRATCH_UMSK				0x8540
#define	SCRATCH_ADDR				0x8544

#define SQ_PWR_THROTTLE_0			0x8e58
#define		SPT0_PWR_MIN				0x00003fff
#define		SPT0_PWR_MAX				0x3fff0000
#define SQ_PWR_THROTTLE_1			0x8e5c
#define		SPT1_PWR_DELTA_MAX			0x00003fff
#define		SPT1_STI_SZ				0x03ff0000
#define		SPT1_LTI_RATIO				0x78000000
/* end of configuration register area: 0x8000-0xb000--------------------------*/

/*----------------------------------------------------------------------------*/
#define UVD_IDX					0xf4a0
#define		UI_ADDR					0x000001ff
#define UVD_DATA				0xf4a4

#define	UVD_CGC_CTL_0				0xf4b0
#define		UCC0_DCM				BIT(0)
#define		UCC0_CG_DT				0x0000003c
#define		UCC0_CLK_OD				0x000007c0

/* uvd indirect register space */
#define	UVD_CGC_MEM_CTL				0xc0
#define	UVD_CGC_CTL_1				0xc1
#define		UCC_DYN_OR_ENA				BIT(0)
#define		UCC_DYN_RR_ENA				BIT(1)
#define		UCC_G_DIV_ID				0x0000008c
/*----------------------------------------------------------------------------*/

#include "rlc_regs.h"
#include "dmas_regs.h"
#include "hdp_regs.h"
#include "tiling_regs.h"
#include "pcie_regs.h"
#include "bif_regs.h"
#include "smc_regs.h"

#include <alga/amd/si/cps_regs.h>
#include <alga/amd/si/gpu_regs_cfg.h>
#include <alga/amd/si/gpu_regs_sh.h>
#include <alga/amd/si/gpu_regs_ctx.h>

#define VM_CTXS_N 16
static u32 regs_vm_ctx_pt_base_addr[VM_CTXS_N] __attribute__ ((unused)) = {
	VM_CTX_0_PT_BASE_ADDR,
	VM_CTX_1_PT_BASE_ADDR,
	VM_CTX_2_PT_BASE_ADDR,
	VM_CTX_3_PT_BASE_ADDR,
	VM_CTX_4_PT_BASE_ADDR,
	VM_CTX_5_PT_BASE_ADDR,
	VM_CTX_6_PT_BASE_ADDR,
	VM_CTX_7_PT_BASE_ADDR,
	VM_CTX_8_PT_BASE_ADDR,
	VM_CTX_9_PT_BASE_ADDR,
	VM_CTX_A_PT_BASE_ADDR,
	VM_CTX_B_PT_BASE_ADDR,
	VM_CTX_C_PT_BASE_ADDR,
	VM_CTX_D_PT_BASE_ADDR,
	VM_CTX_E_PT_BASE_ADDR,
	VM_CTX_F_PT_BASE_ADDR
};
#endif
