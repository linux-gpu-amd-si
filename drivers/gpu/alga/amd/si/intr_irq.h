#ifndef INTR_IRQ_H
#define INTR_IRQ_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
void intrs_reset(struct pci_dev *dev);
irqreturn_t irq_thd(int irq, void *dev_id);
irqreturn_t irq(int irq, void *dev_id);
#endif
