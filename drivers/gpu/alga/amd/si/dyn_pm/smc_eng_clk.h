#ifndef DYN_PM_SMC_ENG_CLK_H
#define DYN_PM_SMC_ENG_CLK_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#ifdef CONFIG_ALGA_AMD_SI_DYN_PM_LOG
void smc_eng_clk_dump(struct smc_eng_clk *tbl);
#endif
long smc_eng_clk_from_atb_pp(struct ctx *ctx, struct atb_pp_lvl *atb_lvl,
						struct smc_lvl *smc_lvl);
#endif

