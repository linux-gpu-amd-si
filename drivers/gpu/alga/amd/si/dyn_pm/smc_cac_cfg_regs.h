#ifndef DYN_PM_SMC_CAC_CFG_REGS_H
#define DYN_PM_SMC_CAC_CFG_REGS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
void smc_cac_cfg_regs_init(struct pci_dev *dev);
#endif
