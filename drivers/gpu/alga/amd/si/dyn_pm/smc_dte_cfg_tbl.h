#ifndef DYN_PM_SMC_DTE_CFG_TBL_H
#define DYN_PM_SMC_DTE_CFG_TBL_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#ifdef CONFIG_ALGA_AMD_SI_DYN_PM_LOG
void smc_dte_cfg_tbl_dump(struct smc_dte_cfg_tbl *tbl);
#endif

void smc_dte_cfg_tbl_init(struct ctx *ctx, struct smc_dte_cfg_tbl *dte);
#endif 
