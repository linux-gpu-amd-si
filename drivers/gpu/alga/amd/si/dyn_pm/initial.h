#ifndef DYN_PM_INITIAL_STATE_H
#define DYN_PM_INITIAL_STATE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
void smc_state_tbl_initial_init(struct ctx *ctx, struct smc_state_tbl *tbl);
long smc_mc_arb_tbl_initial_init(struct ctx *ctx, 
					struct smc_state_tbl *smc_state_tbl,
					struct smc_mc_arb_tbl *smc_mc_arb_tbl);
#endif
