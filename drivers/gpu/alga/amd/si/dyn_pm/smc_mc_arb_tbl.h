#ifndef DYN_PM_SMC_MC_ARB_TBL_H
#define DYN_PM_SMC_MC_ARB_TBL_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#ifdef CONFIG_ALGA_AMD_SI_DYN_PM_LOG
void smc_mc_arb_tbl_dump(struct smc_mc_arb_tbl *tbl);
#endif

long smc_mc_arb_tbl_set_compute(struct ctx *ctx, struct smc_mc_arb_reg_set *set,
						u32 eng_clk, u32 mem_clk);
long smc_mc_arb_tbl_init(struct ctx *ctx, struct smc_state_tbl *smc_state_tbl,
					struct smc_mc_arb_tbl *smc_mc_arb_tbl);
#endif
