#ifndef DYN_PM_SMC_MEM_CLK_H
#define DYN_PM_SMC_MEM_CLK_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#ifdef CONFIG_ALGA_AMD_SI_DYN_PM_LOG
void smc_mem_clk_dump(struct smc_mem_clk *tbl);
#endif

void smc_mem_clk_displays_adjust(struct ctx *ctx, struct smc_lvl *smc_lvl,
								u8 displays_n);
long smc_mem_clk_from_atb_pp(struct ctx *ctx, struct atb_pp_lvl *atb_lvl,
					struct smc_lvl *smc_lvl, u8 dll_state);
#endif
