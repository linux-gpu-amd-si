#ifndef DYN_PM_DRIVER_STATE_H
#define DYN_PM_DRIVER_STATE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long driver_set_performance(struct ctx *ctx);
#endif
