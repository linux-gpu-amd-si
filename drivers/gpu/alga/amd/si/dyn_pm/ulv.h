#ifndef DYN_PM_ULV_STATE_H
#define DYN_PM_ULV_STATE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long smc_state_tbl_ulv_init(struct ctx *ctx, struct smc_state_tbl *tbl);
void ulv_program(struct pci_dev *dev);
void smc_sw_regs_ulv_init(struct ctx *ctx);
void smc_mc_reg_tbl_ulv_init(struct ctx *ctx,
					struct smc_mc_reg_tbl *smc_mc_reg_tbl);
long smc_mc_arb_tbl_ulv_init(struct ctx *ctx,
					 struct smc_mc_arb_tbl *smc_mc_arb_tbl);
#endif
