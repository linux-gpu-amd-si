#ifndef DYN_PM_SMC_MC_REG_TBL_H
#define DYN_PM_SMC_MC_REG_TBL_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#ifdef CONFIG_ALGA_AMD_SI_DYN_PM_LOG
void smc_mc_reg_tbl_dump(struct smc_mc_reg_tbl *tbl);
#endif

void smc_mc_reg_tbl_init(struct ctx *ctx, struct smc_state_tbl *smc_state_tbl,
						struct smc_mc_reg_tbl *tbl);
void smc_mc_reg_set_load(struct ctx *ctx, u8 atb_mc_reg_set_idx,
					struct smc_mc_reg_set *smc_mc_reg_set);
void smc_mc_reg_tbl_sw_regs_init(struct ctx *ctx);
#endif
