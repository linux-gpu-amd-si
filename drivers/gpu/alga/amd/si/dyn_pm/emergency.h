#ifndef DYN_PM_EMERGENCY_STATE_H
#define DYN_PM_EMERGENCY_STATE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long smc_state_tbl_emergency_init(struct ctx *ctx, struct smc_state_tbl *tbl);
#endif
