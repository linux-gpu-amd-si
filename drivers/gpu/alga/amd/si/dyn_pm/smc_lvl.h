#ifndef DYN_PM_SMC_LVL_H
#define DYN_PM_SMC_LVL_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#ifdef CONFIG_ALGA_AMD_SI_DYN_PM_LOG
void smc_lvl_dump(struct smc_lvl *lvl);
#endif
u8 gddr5_strobe_mode_compute(struct ctx *ctx, u32 mem_clk);
long smc_lvl_from_atb(struct ctx *ctx, struct atb_pp_lvl *atb_lvl,
						struct smc_lvl *smc_lvl);
#endif
