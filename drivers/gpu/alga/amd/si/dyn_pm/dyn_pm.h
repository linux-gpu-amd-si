#ifndef DYN_PM_H
#define DYN_PM_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long dyn_pm_ena(struct pci_dev *dev);
void dyn_pm_dis(struct pci_dev *dev);
void dyn_pm_new_display_notify(struct device *dev, u8 dps_active_cnt,
							u8 dps_active_first);
#endif
