/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#include <linux/pci.h>
#include <linux/cdev.h>
#include <asm/unaligned.h>

#include <alga/rng_mng.h>
#include <uapi/alga/pixel_fmts.h>
#include <alga/timing.h>
#include <alga/amd/atombios/atb.h>
#include <uapi/alga/amd/dce6/dce6.h>
#include <alga/amd/atombios/vm.h>
#include <alga/amd/atombios/cm.h>
#include <alga/amd/atombios/pp.h>
#include <alga/amd/atombios/vram_info.h>

#include "../mc.h"
#include "../rlc.h"
#include "../ih.h"
#include "../fence.h"
#include "../ring.h"
#include "../dmas.h"
#include "../ba.h"
#include "../cps.h"
#include "../gpu.h"
#include "../drv.h"

#include "pwrtune.h"
static struct pwrtune pwrtune_tahiti = {
	((1 << 16) | 27027),
	6,
	0,
	4,
	95,
	{
		0UL,
		0UL,
		4521550UL,
		309631529UL,
		-1270850L,
		4513710L,
		40
	},
	595000000UL,
	12,
	{
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	}
};

static struct pwrtune pwrtune_pitcairn = {
	((1 << 16) | 27027),
	5,
	0,
	6,
	100,
	{
		51600000UL,
		1800000UL,
		7194395UL,
		309631529UL,
		-1270850L,
		4513710L,
		100
	},
	117830498UL,
	12,
	{
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	}
};

static struct pwrtune pwrtune_verde = {
	((1 << 16) | 0x6993),
	5,
	0,
	7,
	105,
	{
		0UL,
		0UL,
		7194395UL,
		309631529UL,
		-1270850L,
		4513710L,
		100
	},
	117830498UL,
	12,
	{
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	}
};

static struct pwrtune pwrtune_oland = {
	((1 << 16) | 0x6993),
	5,
	0,
	7,
	105,
	{
		0UL,
		0UL,
		7194395UL,
		309631529UL,
		-1270850L,
		4513710L,
		100
	},
	117830498UL,
	12,
	{
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	}
};

static struct pwrtune pwrtune_mars_pro = {
	((1 << 16) | 0x6993),
	5,
	0,
	7,
	105,
	{
		0UL,
		0UL,
		7194395UL,
		309631529UL,
		-1270850L,
		4513710L,
		100
	},
	117830498UL,
	12,
	{
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	}
};

struct pwrtune *pwrtune_get(struct pci_dev *dev)
{
	struct dev_drv_data *dd;
	dd = pci_get_drvdata(dev);

	switch (dd->family) {
	case TAHITI:
		return &pwrtune_tahiti;
	case PITCAIRN:
		return &pwrtune_pitcairn;
	case VERDE:
		return &pwrtune_verde;
	case OLAND:
		switch (dev->device) {
		case 0x6601:
		case 0x6621:
		case 0x6603:
		case 0x6600:
		case 0x6606:
		case 0x6620:
		case 0x6611:
		case 0x6610:
			return &pwrtune_mars_pro;
		default:
			return &pwrtune_oland;
		}
	}
	unreachable();
}
