#ifndef DYN_PM_PRIVATE_H
#define DYN_PM_PRIVATE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#ifdef CONFIG_ALGA_AMD_SI_DYN_PM_LOG
  #define LOG(fmt,...) printk(KERN_INFO "dyn_pm:" fmt "\n", ##__VA_ARGS__) 
#else
  #define LOG(fmt,...)
#endif

#define MEM_CLK_EDC_RD_ENA_THRESHOLD	40000	/* 10kHz units */
#define MEM_CLK_EDC_WR_ENA_THRESHOLD	40000	/* 10kHz units */
#define MEM_CLK_STROBE_MODE_THRESHOLD	40000	/* 10kHz units */
/* 10 kHz units, in the case of the "special" 1GB platform it's 0 */
#define MEM_CLK_STUTTER_MODE_THRESHOLD	MEM_CLK_STROBE_MODE_THRESHOLD

/* this is our layout for the smc mc_arb_tbl */
#define MC_ARB_SET_IDX_INITIAL_EMERGENCY	0
#define MC_ARB_SET_IDX_ULV			1
/* third slot is not used */
/* index of the set of the first current pwr state lvl */
#define MC_ARB_SET_IDX_DRIVER			3 

/* this is our layout for the smc mc_reg_tbl */
#define MC_REG_SET_IDX_INITIAL_EMERGENCY	0
#define MC_REG_SET_IDX_ULV			1 
/* the third slot is not used */
/* index of the set of the first current pwr state lvl */
#define MC_REG_SET_IDX_DRIVER			3

u8 pcie_speed_cap(struct ctx *ctx, u8 pcie_gen);
struct eng_pll {
	u32 cg_eng_pll_func_ctl_0;
	u32 cg_eng_pll_func_ctl_1;
	u32 cg_eng_pll_func_ctl_2;
	u32 cg_eng_pll_func_ctl_3;
	u32 cg_eng_pll_ss_0;
	u32 cg_eng_pll_ss_1;
};
long eng_pll_compute(struct ctx *ctx, u32 eng_clk, struct eng_pll *eng_pll);
#endif
