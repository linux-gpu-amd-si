#ifndef DYN_PM_SMC_VOLT_H
#define DYN_PM_SMC_VOLT_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define IS_VDDC_LKGE_IDX(x) (((x) & 0xff00) == 0xff00)

#ifdef CONFIG_ALGA_AMD_SI_DYN_PM_LOG
void smc_volt_dump(struct smc_volt *tbl);
#endif

long smc_volt_vddc_set_from_atb_id(struct ctx *ctx, struct smc_volt *vddc,
								u32 vddc_id);
void smc_volt_vddc_set_from_atb_mv(struct ctx *ctx, struct smc_volt *vddc,
								u32 vddc_mv);
void smc_volt_vddci_set_from_atb_mv(struct ctx *ctx, struct smc_volt *vddci,
								u32 vddci_mv);
void smc_volt_mvdd_set_from_atb_mv(struct ctx *ctx, struct smc_volt *mvdd,
								u32 mvdd_mv);
void smc_volt_mvdd_set_from_atb_mem_clk(struct ctx *ctx, struct smc_volt *mvdd,
								u32 mem_clk);
void smc_volt_std_vddc_compute(struct ctx *ctx, struct smc_volt *std_vddc,
							struct smc_volt *vddc);
#endif
