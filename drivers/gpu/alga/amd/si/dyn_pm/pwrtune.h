#ifndef DYN_PM_PWRTUNE_H
#define DYN_PM_PWRTUNE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define DC_CAC_LVL_0		0
#define DC_CAC_LVL_1		1
#define DC_CAC_LVL_2		2
#define DC_CAC_LVL_3		3
#define DC_CAC_LVL_4		4
#define DC_CAC_LVL_5		5
#define DC_CAC_LVL_6		6
#define DC_CAC_LVL_7		7
#define DC_CAC_LVLS_N_MAX	8

struct lkge_coefs
{
	u32 at;
	u32 bt;
	u32 av;
	u32 bv;
	s32 temp_slope;
	s32 temp_intercept;
	u32 temp_ref;
};

struct pwrtune {
	u32 cac_wnd;
	u32 l2_lta_wnd_sz_default;
	u8 lts_truncate_n_default;
	u8 shift_n_default;
	u8 operating_temp;
	struct lkge_coefs lkge_coefs;
	u32 fixed_kt;
	u32 lkge_lut_v0_percent;
	u8 dc_cac[DC_CAC_LVLS_N_MAX];
};

struct pwrtune *pwrtune_get(struct pci_dev *dev);
#endif
