#ifndef DYN_PM_SMC_SW_REGS_H
#define DYN_PM_SMC_SW_REGS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#ifdef CONFIG_ALGA_AMD_SI_DYN_PM_LOG
void smc_sw_regs_dump(struct pci_dev *dev);
#endif

void smc_sw_regs_init(struct ctx *ctx);
#endif
