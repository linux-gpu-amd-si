#ifndef DYN_PM_SMC_TBLS_H
#define DYN_PM_SMC_TBLS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long smc_tbls_init(struct ctx *ctx);
#endif
