#ifndef GOLDEN_H
#define GOLDEN_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
void golden_regs(struct pci_dev *dev);
#endif
