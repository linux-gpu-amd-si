#ifndef CPS_H
#define CPS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/* do get raw u32 value from a 32 bits float */
union f2u {
	float f;
	u32 u;
};
static inline u32 f2u(float f)
{
	union f2u tmp;

	tmp.f = f;
	return tmp.u;
}

#define CP_RING_LOG2_QWS 17
#define CP_RING_LOG2_DWS (CP_RING_LOG2_QWS + 1)
#define CP_RING_DW_MASK ((1 << CP_RING_LOG2_QWS) * 2 - 1)

struct cp_ring
{
	struct fence fence;
	struct ring ring;
	spinlock_t lock;
	u32 wptr; /* dword index in ring buffer: accounted by CPU */
};

void cps_intr_ena(struct pci_dev *dev);
void cps_intr_reset(struct pci_dev *dev);
long cps_engines_ucode_load(struct pci_dev *dev);
void cps_engines_ucode_program(struct pci_dev *dev);
void cps_engines_stop(struct pci_dev *dev);
void cps_init_once(struct pci_dev *dev);
void cps_init(struct pci_dev *dev);
void cps_me_init(struct pci_dev *dev);
void cps_enable(struct pci_dev *dev);
void cps_ctx_clr(struct pci_dev *dev);

void cps_ls_dis(struct pci_dev *dev);
void cps_ls_ena(struct pci_dev *dev);

void gpu_3d_ring_intr_idle_ena(struct pci_dev *dev);
void gpu_3d_ring_intr_idle_dis(struct pci_dev *dev);
void gpu_3d_ring_wr(struct pci_dev *dev, u32 v);
void gpu_3d_ring_commit(struct pci_dev *dev);
void gpu_c0_ring_wr(struct pci_dev *dev, u32 v);
void gpu_c0_ring_commit(struct pci_dev *dev);
void gpu_c1_ring_wr(struct pci_dev *dev, u32 v);
void gpu_c1_ring_commit(struct pci_dev *dev);
#endif
