#ifndef RLC_CLEARSTATE_H
#define RLC_CLEARSTATE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long clearstate_init(struct pci_dev *dev);
#endif
