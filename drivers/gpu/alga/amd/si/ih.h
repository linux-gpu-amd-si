#ifndef IH_H
#define IH_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define IH_RING_LOG2_DWS 14
#define IH_RING_MASK (((1 << IH_RING_LOG2_DWS) * 4) - 1)

struct ih {
	spinlock_t lock;
	u32 rp;		/* the index of the next vector to process */
};
void ih_dis(struct pci_dev *dev);
void ih_reset(struct pci_dev *dev);
void ih_ena(struct pci_dev *dev);
void ih_init(struct pci_dev *dev);
#define IRQ_THD_DIS 0
#define IRQ_THD_ENA 1
u8 ih_parse(struct pci_dev *dev);
#endif
