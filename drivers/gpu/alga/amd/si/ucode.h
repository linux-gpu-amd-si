#ifndef UCODE_H
#define UCODE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long ucode_load(struct pci_dev *dev, const struct firmware **fw,
					const char *fw_str, u32 fw_dws);
#endif
