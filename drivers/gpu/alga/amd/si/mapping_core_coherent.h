#ifndef BUS_CORE_COHERENT_H
#define BUS_CORE_COHERENT_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long core_coherent_map(struct pci_dev *dev, u64 sz, struct ba_map **m);
void core_coherent_cleanup(struct pci_dev *dev, struct ba_map *m, u8 flgs);
#endif
