#ifndef BUS_SG_USER_H
#define BUS_SG_USER_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long sg_user_map(struct pci_dev *dev, void __iomem *cpu_addr,
					struct sg_table *sg_table, int nents);
void sg_user_cleanup(struct pci_dev *dev, struct ba_map *m, u8 flgs);
#endif
