#ifndef BUS_REGS_PCIE_H
#define BUS_REGS_PCIE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#define PCIE_IDX					0x0030
#define 	PI_PCIE_IDX					0x000007ff
#define PCIE_DATA					0x0034
#define	PCIE_PORT_IDX					0x0038
#define	PCIE_PORT_DATA					0x003c

/*----------------------------------------------------------------------------*/
/* the following are the indirect indexes of the pcie registers */
#define PCIE_CTL_1					0x01c
#define		PC_SLV_MEM_LS_ENA 				BIT(16)
#define		PC_SLV_MEM_AGGRESSIVE_LS_ENA 			BIT(17)
#define		PC_MST_MEM_LS_ENA				BIT(18)
#define		PC_REPLAY_MEM_LS_ENA				BIT(19)
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/* the following are the indirect indexex of the pcie **port** registers */
#define PCIE_LC_LINK_WIDTH_CTL				0xa2
#define		PLLWC_LC_LINK_WIDTH				0x00000007
#define			PLLWC_LC_LINK_WIDTH_X0				0
#define			PLLWC_LC_LINK_WIDTH_X1				1
#define			PLLWC_LC_LINK_WIDTH_X2				2
#define			PLLWC_LC_LINK_WIDTH_X4				3
#define			PLLWC_LC_LINK_WIDTH_X8				4
#define			PLLWC_LC_LINK_WIDTH_X12				5
#define			PLLWC_LC_LINK_WIDTH_X16				6
#define		PLLWC_LC_LINK_WIDTH_RD				0x00000070
#define		PLLWC_LC_RECFG_NOW				BIT(8)
#define		PLLWC_LC_RECFG_LATER				BIT(9)
#define		PLLWC_LC_SHORT_RECFG_ENA			BIT(10)

#define PCIE_LC_SPEED_CTL				0xa4
#define		PLSC_LC_GEN2_ENA_STRAP				BIT(0)
#define		PLSC_LC_GEN3_ENA_STRAP				BIT(1)
#define		PLSC_LC_TARGET_LINK_SPEED_OVERRIDE_ENA		BIT(2)
#define		PLSC_LC_TARGET_LINK_SPEED_OVERRIDE		0x00000018
#define		PLSC_LC_FORCE_ENA_SW_SPEED_CHANGE		BIT(5)
#define		PLSC_LC_FORCE_DIS_SW_SPEED_CHANGE		BIT(6)
#define		PLSC_LC_FORCE_ENA_HW_SPEED_CHANGE		BIT(7)
#define		PLSC_LC_FORCE_DIS_HW_SPEED_CHANGE		BIT(8)
#define		PLSC_LC_INITIATE_LINK_SPEED_CHANGE		BIT(9)
#define		PLSC_LC_SPEED_CHANGE_ATTEMPTS_ALLOWED		0x00000c00
/* 0/1/2 = gen1/2/3 */
#define		PLSC_LC_CURRENT_DATA_RATE			0x00006000
#define		PLSC_LC_CLR_FAILED_SPD_CHANGE_CNT		BIT(16)
#define		PLSC_LC_OTHER_SIDE_EVER_SENT_GEN2		BIT(18)
#define		PLSC_LC_OTHER_SIDE_SUPPORTS_GEN2		BIT(19)
#define		PLSC_LC_OTHER_SIDE_EVER_SENT_GEN3		BIT(20)
#define		PLSC_LC_OTHER_SIDE_SUPPORTS_GEN3		BIT(21)
/*----------------------------------------------------------------------------*/
#endif
