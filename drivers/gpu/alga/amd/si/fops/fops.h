#ifndef FOPS_FOPS_H
#define FOPS_FOPS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct file_private_data {
	struct list_head n;
	u8 context_lost;
	struct pci_dev *d;

	/*
	 * the evts like the pf evts are added from a irq handler, then protect
	 * the related data with a spinlock
	 */
	spinlock_t evts_lock;
	struct list_head evts;
	wait_queue_head_t evts_wq;
};

#ifndef FOPS_C
extern void fops_init_once(void);
extern struct file_operations fops;
extern struct list_head files_private_data;
extern dev_t devt_region;
#endif
#endif
