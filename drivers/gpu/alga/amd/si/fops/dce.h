#ifndef FOPS_DCE_H
#define FOPS_DCE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long fops_dce_dp_set(struct pci_dev *dev, void __user *user);
long fops_dce_dp_dpm(struct pci_dev *dev, u8 __user *user_i);
long fops_dce_pf(struct pci_dev *dev, u8 __user *user_i, void *data);
long fops_dce_edid(struct pci_dev *dev,
				struct si_dce_edid  __user *user_ioctl_edid);
#endif
