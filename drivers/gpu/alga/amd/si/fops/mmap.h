#ifndef FOPS_MMAP_H
#define FOPS_MMAP_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct vma_private_data {
	atomic_t refs_n;
	unsigned long cpu_ps_n;
	struct page **cpu_ps;
	struct sg_table sg_tbl;
	int sg_tbl_list_nents;
};

int fops_mmap(struct file *f, struct vm_area_struct *vma);
#define NO_VMA_FOUND	1
#define NO_BA_MAP_FOUND	2
long cpu_addr_to_aperture_gpu_addr(struct pci_dev *dev,
				u64 *aperture_gpu_addr, void __iomem *cpu_addr,
						struct vm_area_struct **vma);
#endif
