#ifndef FOPS_DMA_H
#define FOPS_DMA_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long fops_dma_l2l(struct pci_dev *dev, struct si_dma *dma);
long fops_dma_u32_fill(struct pci_dev *dev, struct si_dma *dma);
#endif
