#ifndef FOPS_PRIVATE_H
#define FOPS_PRIVATE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/* we derive the si_evt from the ioctls */
struct fops_evt {
	struct si_evt base;
	struct list_head n;
};
#endif
