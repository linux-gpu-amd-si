#ifndef VM_H
#define VM_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
void vg_ena(struct pci_dev *dev);
void vg_dis(struct pci_dev *dev);
#endif
