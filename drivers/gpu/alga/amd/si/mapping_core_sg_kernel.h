#ifndef BUS_CORE_SG_KERNEL_H
#define BUS_CORE_SG_KERNEL_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long core_sg_kernel_map(struct pci_dev *dev, u64 sz, struct ba_map **m);
void core_sg_kernel_cleanup(struct pci_dev *dev, struct ba_map *m, u8 flgs);
#endif
