#ifndef MC_H
#define MC_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define GPU_PAGE_SZ			4096
#define GPU_PAGE_SHIFT			12
#define GPU_PAGE_LOG2_QWS		9
#define GPU_PAGE_MASK(gpu_addr)		((gpu_addr) & 0xfffffffffffff000)
#define GPU_PAGE_IDX(addr)		((addr) >> 12)
#define IS_GPU_PAGE_ALIGNED(addr)	(!((addr) & 0xfff))

#if (PAGE_SIZE % GPU_PAGE_SZ) != 0
#error GPU_PAGE_SZ is not a multiple of CPU PAGE_SIZE
#endif
struct vram {
	/* the range should be protected for user space // access */
	struct rng mng;
	u64 scratch_page;
};
void mc_addr_cfg_compute(struct pci_dev *dev);
long mc_ucode_load(struct pci_dev *dev);
void mc_ucode_program(struct pci_dev *dev);
long mc_wait_for_idle(struct pci_dev *dev);
long mc_program(struct pci_dev *dev);

u32 mc_clients_blackout_mode(struct pci_dev *dev);
void mc_clients_blackout_dis(struct pci_dev *dev);
void mc_clients_blackout_ena(struct pci_dev *dev);

void mc_mgcg_dis(struct pci_dev *dev);
void mc_mgcg_ena(struct pci_dev *dev);
void mc_ls_dis(struct pci_dev *dev);
void mc_ls_ena(struct pci_dev *dev);

u8 mc_lb_pwr_supported(struct pci_dev *dev);

u8 mc_vram_is_gddr5(struct pci_dev *dev);
#endif
