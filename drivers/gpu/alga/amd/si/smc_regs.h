#ifndef SMC_REGS_H
#define SMC_REGS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/* start of the xRBM register area--------------------------------------------*/
#define SMC_IDX				0x200
#define SMC_DATA			0x204

#define SMC_ACCESS_CTL			0x228
#define		SAC_AUTO_INCREMENT		BIT(0)
#define SMC_MSG_SEND  			0x22c
#define		SMS_MSG				0x0000ffff
#define SMC_MSG_RESP  			0x230
#define		SMR_RESP			0x0000ffff

#define	SMC_SCRATCH_0			0x884
/* end of the xRBM register area----------------------------------------------*/


/* start of SMC register area-------------------------------------------------*/
#define	SMC_SYSCON_RESET_CTL		0x80000000
#define		SSRC_RST_REG			BIT(0)
#define	SMC_SYSCON_CLK_CTL		0x80000004
#define		SSCC_CK_DIS			BIT(0)
#define		SSCC_CKEN			BIT(24)
/*
 * cg ind registers are accessed via smc indirect space + SMC_CG_IND_START.
 * This defines the cg addresses in the smc address space which will be used
 * by the microcontroller to program cg.
 */
#define SMC_CG_IND_START		0xc0030000
#define SMC_CG_IND_END			0xc0040000

/*
 * the following regs are in the above indirect smc space, probably not a
 * xrbm feedback, but uvd
 */
#define	SMC_CG_CGTT_LOCAL_0		0x400
#define	SMC_CG_CGTT_LOCAL_1		0x401
/* end of SMC register area---------------------------------------------------*/

/* start of software SMC register area----------------------------------------*/
#define SMC_SW_MCLK_CHG_TIMEOUT		0x0
#define SMC_SW_DELAY_VREG		0xc
#define SMC_SW_DELAY_EMERGENCY		0x28
#define SMC_SW_SEQ_IDX			0x5c
#define SMC_SW_MVDD_CHG_TIME		0x60
#define SMC_SW_MCLK_SWITCH_LIM		0x70
#define SMC_SW_WATERMARK_THRESHOLD	0x78
#define SMC_SW_PHASE_SHEDDING_DELAY	0x88
#define SMC_SW_ULV_VOLT_CHANGE_DELAY	0x8c
#define SMC_SW_MC_BLK_DELAY		0x98
#define SMC_SW_TICKS_PER_US             0xa8
#define SMC_SW_CRTC_IDX			0xc4
#define SMC_SW_MCLK_CHANGE_BLK_CP_MIN	0xc8
#define SMC_SW_MCLK_CHANGE_BLK_CP_MAX	0xcc
#define SMC_SW_NON_ULV_PCIE_LINK_WIDTH	0xf4
#define SMC_SW_TDR_IS_ABOUT_TO_HAPPEN	0xfc
#define SMC_SW_VR_HOT_GPIO		0x100
/* end of software SMC register area------------------------------------------*/
#endif
