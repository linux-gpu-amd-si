#ifndef GPU_H
#define GPU_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct gpu_cfg {
	u8 hw_ctxs_n;
	u8 ses_n;
	u8 se_shs_n;
	u8 se_rbs_n;
	u8 sh_cus_n;

	u32 sc_prim_fifo_sz_frontend;
	u32 sc_prim_fifo_sz_backend;
	u32 sc_hiz_tile_fifo_sz;
	u32 sc_earlyz_tile_fifo_sz;
};
void gpu_defaults(struct pci_dev *dev, u32 addr_cfg, u32 mem_row_sz_kb);
void gpu_soft_reset(struct pci_dev *dev);
void gpu_cfg_init(struct pci_dev *dev);

void gpu_mgcg_ena(struct pci_dev *dev);
void gpu_mgcg_dis(struct pci_dev *dev);

void gpu_lb_pw_dis(struct pci_dev *dev);

void gpu_grdbm_int_reset(struct pci_dev *dev);
#endif
