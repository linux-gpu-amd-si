#ifndef FENCE_H
#define FENCE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#define FENCE_TIMEOUT 906

#define FENCE_FROM_DMA_0 0
#define FENCE_FROM_DMA_1 1
#define FENCE_FROM_MAX_N 2
struct fence {
	atomic_t bottom;
	atomic_t top;
	u32 __iomem *cpu_addr;
	wait_queue_head_t wait_queue; /* for synchronous signaling */
};
void fence_init_once(struct fence *f);
void fence_init(struct fence *f, u32 __iomem *cpu_addr);
#define FENCE_SIGNALED 0
long fence_wait(struct fence *f, u32 seq_n, u32 timeouts_max_n, u32 timeout_us);
u32 fence_seq_n_get(struct fence *f);
#endif
