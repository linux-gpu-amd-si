#ifndef BIF_REGS_H
#define BIF_REGS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define	BIF_HDP_ENA				0x5490
#define		BHE_HDP_RD_ENA				BIT(0)
#define		BHE_HDP_WR_ENA				BIT(1)
#endif
