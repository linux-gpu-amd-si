#ifndef RINGS_H
#define RINGS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#define RING_WAIT_TIMEOUT 907

struct ring {
	struct pci_dev *dev;
	u32 pf_dw_mask;
	u32 ring_dws_n;
	u32 (*rptr_dw_get)(struct pci_dev *dev);
	u32 (*wptr_dw_get)(struct pci_dev *dev);
};
long ring_wait(struct ring *r, u32 dws_n, u32 timeouts_max_n, u32 timeout_us);
#endif
