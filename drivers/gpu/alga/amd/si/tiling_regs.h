#ifndef TILING_REGS_H
#define TILING_REGS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
/* start of configuration register area: 0x8000-0xb000----------------------------------*/
#define	GB_TILE_MODE_00				0x9910
#define		GTM_MICRO_TILE_MODE			0x00000003
#define			GTM_ADDR_SURF_DISP_MICRO_TILING		0
#define			GTM_ADDR_SURF_THIN_MICRO_TILING		1
#define			GTM_ADDR_SURF_DEPTH_MICRO_TILING	2
#define		GTM_ARRAY_MODE				0x0000001c
#define			GTM_ARRAY_LINEAR_GENERAL		0
#define			GTM_ARRAY_LINEAR_ALIGNED		1
#define			GTM_ARRAY_1D_TILED_THIN1		2
#define			GTM_ARRAY_2D_TILED_THIN1		4
#define		GTM_PIPE_CFG				0x000003c0
#define			GTM_ADDR_SURF_P2			0
#define			GTM_ADDR_SURF_P4_8x16			4
#define			GTM_ADDR_SURF_P4_16x16			5
#define			GTM_ADDR_SURF_P4_16x32			6
#define			GTM_ADDR_SURF_P4_32x32			7
#define			GTM_ADDR_SURF_P8_16x16_8x16		8
#define			GTM_ADDR_SURF_P8_16x32_8x16		9
#define			GTM_ADDR_SURF_P8_32x32_8x16		10
#define			GTM_ADDR_SURF_P8_16x32_16x16		11
#define			GTM_ADDR_SURF_P8_32x32_16x16		12
#define			GTM_ADDR_SURF_P8_32x32_16x32		13
#define			GTM_ADDR_SURF_P8_32x64_32x32		14
#define		GTM_TILE_SPLIT				0x00003800
#define			GTM_ADDR_SURF_TILE_SPLIT_64B		0
#define			GTM_ADDR_SURF_TILE_SPLIT_128B		1
#define			GTM_ADDR_SURF_TILE_SPLIT_256B		2
#define			GTM_ADDR_SURF_TILE_SPLIT_512B		3
#define			GTM_ADDR_SURF_TILE_SPLIT_1KB		4
#define			GTM_ADDR_SURF_TILE_SPLIT_2KB		5
#define			GTM_ADDR_SURF_TILE_SPLIT_4KB		6
#define		GTM_BANK_W				0x0000c000
#define			GTM_ADDR_SURF_BANK_W_1			0
#define			GTM_ADDR_SURF_BANK_W_2			1
#define			GTM_ADDR_SURF_BANK_W_4			2
#define			GTM_ADDR_SURF_BANK_W_8			3
#define		GTM_BANK_H				0x00030000
#define			GTM_ADDR_SURF_BANK_H_1			0
#define			GTM_ADDR_SURF_BANK_H_2			1
#define			GTM_ADDR_SURF_BANK_H_4			2
#define			GTM_ADDR_SURF_BANK_H_8			3
#define		GTM_MACRO_TILE_ASPECT			0x000c0000
#define			GTM_ADDR_SURF_MACRO_ASPECT_1		0
#define			GTM_ADDR_SURF_MACRO_ASPECT_2		1
#define			GTM_ADDR_SURF_MACRO_ASPECT_4		2
#define			GTM_ADDR_SURF_MACRO_ASPECT_8		3
#define		GTM_BANKS_N				0x00300000
#define			GTM_ADDR_SURF_02_BANK			0
#define			GTM_ADDR_SURF_04_BANK			1
#define			GTM_ADDR_SURF_08_BANK			2
#define			GTM_ADDR_SURF_16_BANK			3
#define	GB_TILE_MODE_01				0x9914
#define	GB_TILE_MODE_02				0x9918
#define	GB_TILE_MODE_03				0x991c
#define	GB_TILE_MODE_04				0x9920
#define	GB_TILE_MODE_05				0x9924
#define	GB_TILE_MODE_06				0x9928
#define	GB_TILE_MODE_07				0x992c
#define	GB_TILE_MODE_08				0x9930
#define	GB_TILE_MODE_09				0x9934
#define	GB_TILE_MODE_0A				0x9938
#define	GB_TILE_MODE_0B				0x993c
#define	GB_TILE_MODE_0C				0x9940
#define	GB_TILE_MODE_0D				0x9944
#define	GB_TILE_MODE_0E				0x9948
#define	GB_TILE_MODE_0F				0x994c
#define	GB_TILE_MODE_10				0x9950
#define	GB_TILE_MODE_11				0x9954
#define	GB_TILE_MODE_12				0x9958
#define	GB_TILE_MODE_13				0x995c
#define	GB_TILE_MODE_14				0x9960
#define	GB_TILE_MODE_15				0x9964
#define	GB_TILE_MODE_16				0x9968
#define	GB_TILE_MODE_17				0x996c
#define	GB_TILE_MODE_18				0x9970
#define	GB_TILE_MODE_19				0x9974
#define	GB_TILE_MODE_1A				0x9978
#define	GB_TILE_MODE_1B				0x997c
#define	GB_TILE_MODE_1C				0x9980
#define	GB_TILE_MODE_1D				0x9984
#define	GB_TILE_MODE_1E				0x9988
#define	GB_TILE_MODE_1F				0x998c
/* end of configuration register area: 0x8000-0xb000--------------------------*/
#endif
