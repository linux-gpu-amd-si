#ifndef BUS_BA_PRIVATE_H
#define BUS_BA_PRIVATE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define PTE_SZ sizeof(u64)

/* pte attributes */
#define PTE_VALID	(1 << 0)
#define PTE_SYSTEM	(1 << 1) /* ("host") system (via bus), non-system ("board local") (vram) */
#define PTE_SNOOPED	(1 << 2) /* snoop is cache coherent */
#define PTE_READABLE	(1 << 5)
#define PTE_WRITEABLE	(1 << 6)

void pte_mmio_regs_install(struct pci_dev *dev, u64 pte_addr, u64 bus_addr);
void tlb_flush(struct pci_dev *dev);
#endif
