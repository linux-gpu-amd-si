#ifndef HDP_H
#define HDP_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
void hdp_init(struct pci_dev *dev);

void hdp_mgcg_ena(struct pci_dev *dev);
void hdp_mgcg_dis(struct pci_dev *dev);

void hdp_ls_ena(struct pci_dev *dev);
void hdp_ls_dis(struct pci_dev *dev);

void hdp_cache_flush(struct pci_dev *dev);
#endif
