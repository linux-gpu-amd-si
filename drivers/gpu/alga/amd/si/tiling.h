#ifndef GPU_TILING_H
#define GPU_TILING_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define TILE_MODE_STATES_N 32
void tiling_modes_tbl_init(struct pci_dev *dev, u32 mem_row_sz_kb);
#endif
