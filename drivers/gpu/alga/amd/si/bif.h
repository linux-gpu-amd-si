#ifndef BIF_H
#define BIF_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
void bif_hdp_ena(struct pci_dev *dev);
void bif_hdp_dis(struct pci_dev *dev);

void bif_mgls_ena(struct pci_dev *dev);
void bif_mgls_dis(struct pci_dev *dev);

#define BIF_PCIE_GEN_1 0
#define BIF_PCIE_GEN_2 1
#define BIF_PCIE_GEN_3 2
u8 bif_pcie_gen_get(struct pci_dev *dev);
#define BIF_PCIE_ROOT_GEN_1 BIT(0)
#define BIF_PCIE_ROOT_GEN_2 BIT(1)
#define BIF_PCIE_ROOT_GEN_3 BIT(2)
long bif_pcie_root_speeds_get(struct pci_dev *dev, u8 *mask);
u8 bif_pcie_lanes_n_get(struct pci_dev *dev);
#endif
