#ifndef CM_H
#define CM_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
void cg_ena(struct pci_dev *dev);
void cg_dis(struct pci_dev *dev);

long gpu_aux_clk_get(struct pci_dev *dev, u16 *gpu_aux_clk);
#endif
