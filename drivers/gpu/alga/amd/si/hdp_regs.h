#ifndef HDP_REGS_H
#define HDP_REGS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define	HDP_HOST_PATH_CTL			0x2c00
#define		HHPC_CG_DIS				BIT(23)
#define	HDP_BASE				0x2c04
#define	HDP_INFO				0x2c08
#define	HDP_SZ					0x2c0c

#define HDP_ADDR_CFG 				0x2f48
#define HDP_MISC_CTL				0x2f4c
#define		HMC_HDP_FLUSH_INVALIDATE_CACHE		BIT(0)

#define HDP_MEM_PWR_LS				0x2f50
#define		HMPL_HDP_LS_ENA				BIT(0)

#define HDP_MEM_COHERENCY_FLUSH_CTL		0x5480 

#define HDP_REG_COHERENCY_FLUSH_CTL		0x54a0

/*
 * HDP data
 * 0x2c14 - 0x2f27 32 blocks of 0x18 bytes
 */
#endif
