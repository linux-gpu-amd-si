#ifndef ATB_H
#define ATB_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct g_ctx
{
	/* workspace data */
	u32 divmul[2];
	u32 fb_wnd;
	u16 data_blk;
	u16 regs_blk;
	u16 io_attr;
	u8 shift;
	u16 io_mode;

	/* compare opcodes */
	u8 equal;
	u8 above;

	/* parameter space management */
	u32 *ps_top;
	u32 ps_dws;
};

struct atombios
{
	struct mutex mutex;

	struct atb_dev adev;
	struct rom_hdr *hdr;
	u32 *scratch;
	u32 scratch_sz;

	/*
	 * This is an array of 256 pointer indexes on indirect io programs.
	 * The first part 0-127 is for read programs, the second part is
	 * for write programs... in theory.
	 */
	u16 *iio;

	struct g_ctx g_ctx;

	u8 name[512];
};
#endif /* _ATB_H */
