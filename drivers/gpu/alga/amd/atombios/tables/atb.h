#ifndef TABLES_ATB_H
#define TABLES_ATB_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#ifndef __aligned 
#	error "no byte alignment cpp macro defined for your compiler"
#endif
struct common_tbl_hdr
{
	__le16 sz;
	u8 tbl_fmt_rev;		/* change it when the parser is not backward
				   compatible */
	u8 tbl_content_rev;	/* change it only when the table needs to change
				   but the firmware image can't be updated,
				   while driver needs to carry the new table! */
} __packed;

struct rom_hdr
{
	struct common_tbl_hdr hdr;

	u8 firmware_signature[4];	/* signature to distinguish between
					   atombios and non-atombios, atombios
					   should init it as "ATOM", don't
					   change the position */
	u8 unused0[8];

	__le16 bios_bootup_msg_of;

	u8 unused1[12];

	__le16 master_cmd_tbl_of;	/* offset for sw to get all command
					   table offsets, don't change the
					   position */
	__le16 master_data_tbl_of;	/* offset for sw to get all data table
					   offsets, don't change the position */
	u8 unused2[2];
} __packed;

#define DFPx_MAX 6
#endif
