#ifndef TABLES_I2C_H
#define TABLES_I2C_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

struct gpio_i2c_assignment {
	__le16 clk_mask_reg_idx;
	__le16 clk_en_reg_idx;
	__le16 clk_y_reg_idx;
	__le16 clk_a_reg_idx;
	__le16 data_mask_reg_idx;
	__le16 data_en_reg_idx;
	__le16 data_y_reg_idx;
	__le16 data_a_reg_idx;
	u8 i2c_id;
	u8 clk_mask_shift;
	u8 clk_en_shift;
	u8 clk_y_shift;
	u8 clk_a_shift;
	u8 data_mask_shift;
	u8 data_en_shift;
	u8 data_y_shift;
	u8 data_a_shift;
	u8 rsvd1;
	u8 rsvd2;
} __packed;

/* struct atb_gpio_i2c_assignment.i2c_id bits */
#define I2C_HW_CAPABLE		0xf0
#define I2C_HW_ENG_ID_MASK	0x70
#define I2C_HW_ENG_ID_SHIFT	4
#define I2C_LINE_MUX_MASK	0x0f

#define I2C_MM_ENG_ID		0x02

struct gpio_i2c_info { 
	struct common_tbl_hdr hdr;
	struct gpio_i2c_assignment info[];
} __packed;

struct hw_i2c_one_data_byte_wr_params {
	u16 unused[6];
} __packed;
#endif
