#ifndef TABLES_ENC_CTL_H
#define TABLES_ENC_CTL_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct enc_ctl_params {
	__le16 pixel_clk; 	/* in 10kHz units */
	u8 cfg;
#define ENC_CTL_CFG_LINK_RATE_MASK		0x03
#define ENC_CTL_CFG_LINK_RATE_SHIFT		0
#define		ENC_CTL_CFG_LINK_RATE_1_62_GHZ		0
#define		ENC_CTL_CFG_LINK_RATE_2_70_GHZ		1
#define		ENC_CTL_CFG_LINK_RATE_5_40_GHZ		2
#define		ENC_CTL_CFG_LINK_RATE_3_24_GHZ		3
#define ENC_CTL_CFG_ENC_MASK			0x70
#define ENC_CTL_CFG_ENC_SHIFT			4
	u8 action;
#define ENC_CTL_ACTION_DISABLE				0x00
#define ENC_CTL_ACTION_ENA				0x01
#define ENC_CTL_ACTION_DP_LINK_TRAINING_START		0x08
#define ENC_CTL_ACTION_DP_LINK_TRAINING_PATTERN_1	0x09
#define ENC_CTL_ACTION_DP_LINK_TRAINING_PATTERN_2	0x0a
#define ENC_CTL_ACTION_DP_LINK_TRAINING_PATTERN_3	0x13
#define ENC_CTL_ACTION_DP_LINK_TRAINING_COMPLETE	0x0b
#define ENC_CTL_ACTION_DP_VIDEO_OFF			0x0c
#define ENC_CTL_ACTION_DP_VIDEO_ON			0x0d
#define ENC_CTL_ACTION_QUERY_DP_LINK_TRAINING_STATUS	0x0e
#define ENC_CTL_ACTION_SETUP				0x0f
#define ENC_CTL_ACTION_SETUP_PANEL_MODE			0x10
	union {
		u8 enc_mode;	/* dp is mode 0 */
		u8 panel_mode;	/* valid when action =
				   ENC_ACTION_SETUP_PANEL_MODE */
#define ENC_CTL_PANEL_MODE_EXTERNAL_DP_MODE	0x00
#define ENC_CTL_PANEL_MODE_INTERNAL_DP2_MODE	0x01
#define ENC_CTL_PANEL_MODE_INTERNAL_DP1_MODE	0x11
	};
	u8 lanes_n;
	u8 bpc;			/* bit per color component, valid for dp mode
				   when action = ENC_ACTION_SETUP */
#define ENC_CTL_BPC_UNDEFINE		0x00
#define ENC_CTL_6BITS_PER_COLOR		0x01 
#define ENC_CTL_8BITS_PER_COLOR		0x02
#define ENC_CTL_10BITS_PER_COLOR	0x03
#define ENC_CTL_12BITS_PER_COLOR	0x04
#define ENC_CTL_16BITS_PER_COLOR	0x05
	u8 hpd;			/* 1-6, with 0 meaning skipping */
} __packed;
#endif
