#ifndef RECORDS_H
#define RECORDS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
/* struct rec_hdr->type */
#define REC_TYPE_I2C				1         
#define REC_TYPE_HPD_INT			2
#define REC_TYPE_OUTPUT_PROTECTION		3
#define REC_TYPE_CONN_DEV_TAG			4
#define	REC_TYPE_CONN_DVI_EXT_INPUT		5	/* use GPIO_CTL */
#define REC_TYPE_ENCODER_FPGA_CTL		6	/* use GPIO_CTL */
#define REC_TYPE_CONN_CVTV_SHARE_DIN		7
#define REC_TYPE_JTAG				8	/* use GPIO_CTL */
#define REC_TYPE_OBJ_GPIO_CTL			9
#define REC_TYPE_ENCODER_DVO_CF			10
#define REC_TYPE_CONN_CF			11
#define	REC_TYPE_CONN_HARDCODE_DTD		12
#define REC_TYPE_CONN_PCIE_SUBCONN		13
#define REC_TYPE_ROUTER_DDC_PATH_SELECT		14
#define REC_TYPE_ROUTER_DATA_CLK_PATH_SELECT	15
#define REC_TYPE_CONN_HPDPIN_LUT		16	/* for connectors
							   unknown in object
							   table */
#define REC_TYPE_CONN_AUXDDC_LUT		17	/* see above */
#define REC_TYPE_OBJ_LINK			18	/* this object is linked
							   to another object
							   described by the
							   record */
#define REC_TYPE_CONN_REMOTE_CAPS		19
#define REC_TYPE_ENCODER_CAPS			20
#define REC_TYPE_MAX				REC_TYPE_ENCODER_CAPS

struct rec_hdr
{
	u8 type;
	u8 sz;	/* whole record in bytes */
} __packed;

/* struct encoder_caps->flgs */
#define ENCODER_CAPS_DP_HBR2 0x0001

struct encoder_caps
{
	struct rec_hdr hdr;
	__le16 flgs;         
} __packed;

#define I2C_ID_HW_CAPABLE	0x01
#define I2C_ID_ENG_ID_MASK	0x70
#define I2C_ID_ENG_ID_SHIFT	4
#define I2C_ID_LINE_MUX_MASK	0x0f
#define I2C_ID_LINE_MUX_SHIFT	0
struct i2c_info
{
	struct rec_hdr hdr;
	u8 id; 
	u8 addr; /* slave addr which is 0 for connector ddc */
} __packed;

struct hpd_int
{
	struct rec_hdr hdr;
	u8 gpio_id;
	u8 plugged_pinstate;
} __packed;
#endif
