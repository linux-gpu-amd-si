#ifndef TABLES_PIXEL_CLK_SET_H
#define TABLES_PIXEL_CLK_SET_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct pixel_clk_set_params {
	union {
		__le32 disp_eng_clk;
		__le32 crtc_pixel_clk;	/* Target the pixel clock to drive the
					   CRTC timing. 0 means disable PPLL.
					   10kHz units */
#define CRTC_MASK	0xff000000
#define CRTC_SHIFT	24
#define PIXEL_CLK_MASK	0x00ffffff
#define PIXEL_CLK_SHIFT 0
	};
	__le16 fb_div;			/* feedback divider integer part */
	u8 post_div;			/* post divider */
	u8 ref_div;			/* reference divider */
	u8 ppll;			/* PPLL0/PPLL1/PPLL2 */
#define PIXEL_CLK_SET_PPLL1		0
#define PIXEL_CLK_SET_PPLL2		1
#define PIXEL_CLK_SET_PPLL0		2
#define PIXEL_CLK_SET_EXT_PLL1		8
#define PIXEL_CLK_SET_EXT_PLL2		9
#define PIXEL_CLK_SET_EXT_CLK		10
#define PIXEL_CLK_SET_PPLL_INVALID	0xff
	u8 trans_id;			/* transmitter object id */
	u8 enc_mode;			/* encoder mode, 0 for dp */
	u8 misc_info;			/* [0]   Force program PPLL 
					   [1]   when VGA timing is used
					   [3:2] HDMI panel bit depth
					         0: 24bpp 
					         1: 36bpp 
					         2: 30bpp
					         3: 48bpp
					   [4]   RefClock source for PPLL
					         0: XTLAIN (default mode)
					         1: other external clock source,
					            which is pre-defined by
					            VBIOS depend on the feature
					            required.
					   [7:5] reserved */
	__le32 fb_div_dec_frac;		/* 20bits feedback divider decimal
					   fraction part, range from 1~999999
					   (0.000001 to 0.999999) */
} __packed;
#endif
