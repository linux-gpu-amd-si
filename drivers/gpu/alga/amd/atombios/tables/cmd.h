#ifndef TABLES_CMD_H
#define TABLES_CMD_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/* structures used in cmd.mtb */
struct cmd_tbls_list
{
	__le16 asic_init;
	u16 unused0[3];
	__le16 enc_ctl;
	u16 unused1[5];
	__le16 eng_clk_set;
	__le16 mem_clk_set;
	__le16 pixel_clk_set;
	__le16 crtc_pair_pwr_gate;
	u16 unused2[19];
	__le16 crtc_scaler;
	__le16 crtc_blank;
	__le16 crtc;
	u16 unused3[4];
	__le16 crtc_overscan;
	u16 unused4;
	__le16 enc_crtc_src;
	u16 unused5;
	__le16 crtc_db_regs;
	u16 unused6[2];
	__le16 mem_clk_get;
	__le16 eng_clk_get;
	__le16 crtc_timing;
	u16 unused7[10];
	__le16 eng_pll_compute;
	u16 unused8[2];
	__le16 dyn_mem_settings;
	u16 unused9[3];
	__le16 volt;
	u16 unused10[2];
	__le16 mem_pll_compute;
	u16 unused11[5];
	__le16 trans_ctl;
	u16 unused12;
	__le16 dp_aux;
	u16 unused13[2];
} __packed;

struct master_cmd_tbl
{ 
	u8 unused[4];
	struct cmd_tbls_list list;
} __packed;

/*
 * common header for all command tables
 * every table pointed by master_cmd_tbl has this common header and the pointer
 * actually points to this header 
 */
struct common_cmd_tbl_hdr
{
	struct common_tbl_hdr hdr;
	u8 ws_dws;
	u8 ps_frame_sz;	/* [8] updated by utility
			   [7:0] bytes (multiple of dwords) */
} __packed;

#define ATTRIB_PS_SZ_MASK		0x7f
#define ATTRIB_UPDATED_BY_UTILITY	(1 << 7) /* ps_sz byte */
#endif
