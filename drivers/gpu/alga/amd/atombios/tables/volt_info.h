#ifndef TABLES_VOLT_INFO_H
#define TABLES_VOLT_INFO_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

struct volt_info_obj_hdr {
	/* volt src: vddc, mvddc, mvddq or mvddci */
	u8 type;	
	/* volt ctl mode: init/set volt/lkge/set phase */
	u8 mode;
	/* size of object */
	__le16 sz;	
} __packed;

/*----------------------------------------------------------------------------*/
/* volt_info_obj_hdr.mode */

/* volt and gpio lut */
#define VOLT_OBJ_GPIO_LUT			0x00
/* volt regulator init sequence through i2c */
#define VOLT_OBJ_VR_I2C_INIT_SEQ		0x03        
/* set vregulator phase lookup table */
#define VOLT_OBJ_GPIO_PHASE_SHED_LUT		0x04        
/* volt ctl by SVID2 */
#define VOLT_OBJ_SVID2				0x07        
/* pwrboost volt and lkge_id lut */
#define	VOLT_OBJ_PWRBOOST_LKGE_LUT		0x10     
/* high volt state volt and lkge_id lut */
#define	VOLT_OBJ_HIGH_STATE_LKGE_LUT		0x11     
/* high1 volt_state volt and lkge_id lut */
#define VOLT_OBJ_HIGH1_STATE_LKGE_LUT		0x12     
/*----------------------------------------------------------------------------*/

struct volt_info_obj_gpio_lut_entry {
	__le32 volt_id;		/* for gpio programming */
	__le16 volt_val;	/* volt value, mV units  */
} __packed;

struct volt_info_obj_gpio {
	/* volt mode = VOLT_OBJ_GPIO_LUT or VOLT_OBJ_GPIO_PHASE_SHED_LUT */
	struct volt_info_obj_hdr hdr;
	/* default is 0 which indicate control through cg vid mode  */
	u8 volt_gpio_ctl_id;
	/* the number of entries in votlage/gpio lookup table */
	u8 lut_entries_n;
	/* phase delay in us */
	u8 phase_delay;
	u8 rsvd;
	__le32 gpio_mask;
	struct volt_info_obj_gpio_lut_entry lut_entries[];
} __packed;

union volt_info_obj {
	struct volt_info_obj_gpio gpio;
	/* many other types of object possible, see volt_info_obj_hdr.mode */
} __packed;

struct volt_info {
	struct common_tbl_hdr hdr;
	union volt_info_obj objs[];
} __packed;
#endif
