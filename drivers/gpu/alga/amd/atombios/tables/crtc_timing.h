#ifndef TABLES_CRTC_TIMING_H
#define TABLES_CRTC_TIMING_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define INFO_H_CUTOFF		0x01
#define INFO_HSYNC_POLARITY	0x02/* 0: high/positive, 1: low/negative */
#define INFO_VSYNC_POLARITY	0x04/* 0: high/positive, 1: low/negative */
#define INFO_V_CUTOFF		0x08
#define INFO_H_REPLICATIONBY2	0x10
#define INFO_V_REPLICATIONBY2	0x20
#define INFO_COMPOSITESYNC	0x40
#define INFO_INTERLACE		0x80
#define INFO_DBL_CLK_MODE	0x100
#define INFO_RGB888_MODE	0x200

/* horizontal as pixels, vertical as lines */
struct crtc_timing_params {
	__le16 h;
	__le16 h_bl;
	__le16 v;
	__le16 v_bl;
	__le16 h_so;
	__le16 h_spw;
	__le16 v_so;
	__le16 v_spw;
	__le16 info;
	u8 h_b;
	u8 v_b;
	u8 crtc;
	u8 pad[3];
} __packed;
#endif
