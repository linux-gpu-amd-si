#ifndef TABLES_CRTC_OVERSCAN_H
#define TABLES_CRTC_OVERSCAN_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct crtc_overscan_params {
	__le16 right;
	__le16 left;
	__le16 bottom;
	__le16 top;
	u8 crtc;
	u8 pad[3];
} __packed;
#endif
