#ifndef TABLES_SS_INFO_H
#define TABLES_SS_INFO_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/*
 * ss_info_entry.clk_id, use the ids below to search if ss is required/enabled
 * on a clock branch/signal type.
 * ss is not required or enabled if a match is not found.
 */
#define SS_INFO_ENTRY_CLK_ID_MEM	1
#define SS_INFO_ENTRY_CLK_ID_ENG	2
#define SS_INFO_ENTRY_CLK_ID_UVD	3
#define SS_INFO_ENTRY_CLK_ID_TMDS	4
#define SS_INFO_ENTRY_CLK_ID_HDMI	5
#define SS_INFO_ENTRY_CLK_ID_LVDS	6
#define SS_INFO_ENTRY_CLK_ID_DP		7
#define SS_INFO_ENTRY_CLK_ID_DCPLL	8
#define SS_INFO_ENTRY_CLK_ID_DP_CLK	9
#define SS_INFO_ENTRY_CLK_ID_VCE	10
#define SS_INFO_ENTRY_CLK_ID_GPUPLL	11

struct ss_info_entry {
	/*
	 * 10 kHz units
	 * For mem/engine/uvd, clock out frequence (vco).
	 * For tmds/hdmi/lvds, it is pixel clock.
	 * For dp, it is link clock.
	 */
	__le32 tgt_clk_rng;

	/* 0.01 % units */
	__le16 ss_percentage;

	/* modulation frequence, 10 ***Hz*** units */
	__le16 spread_rate;

	u8 clk_id;

	/*
	 * [0]=0: down spread
	 * [0]=1: center spread
	 * [1]=0: internal ss
	 * [1]=1: external ss
	 */
	u8 mode;

	u8 rsvd[2];
} __packed;

struct ss_info {
	struct common_tbl_hdr hdr;
	struct ss_info_entry entries[];
} __packed;
#endif
