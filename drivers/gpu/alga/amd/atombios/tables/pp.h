#ifndef TABLES_PP_H
#define TABLES_PP_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/*----------------------------------------------------------------------------*/
#define PP_FAN_PARAMS_TACHOMETER_PULSES_PER_REVOLUTION_MASK	0x0f
#define PP_FAN_PARAMS_NOFAN					0x80

/* pp_thermal_ctrler.type */
#define PP_THERMAL_CTRLER_NONE		0
#define PP_THERMAL_CTRLER_LM63		1 /* not used by pplib */
#define PP_THERMAL_CTRLER_ADM1032	2 /* not used by pplib */
#define PP_THERMAL_CTRLER_ADM1030	3 /* not used by pplib */
#define PP_THERMAL_CTRLER_MUA6649	4 /* not used by pplib */
#define PP_THERMAL_CTRLER_LM64		5
#define PP_THERMAL_CTRLER_F75375	6 /* not used by pplib */
#define PP_THERMAL_CTRLER_RV6xx		7
#define PP_THERMAL_CTRLER_RV770		8
#define PP_THERMAL_CTRLER_ADT7473	9
#define PP_THERMAL_CTRLER_KONG		10
#define PP_THERMAL_CTRLER_EXTERNAL_GPIO	11
#define PP_THERMAL_CTRLER_EVERGREEN	12
#define PP_THERMAL_CTRLER_EMC2103	13  /* only fan control */
#define PP_THERMAL_CTRLER_SUMO		14  /* sumo type, used internally */
#define PP_THERMAL_CTRLER_NI		15
#define PP_THERMAL_CTRLER_SI		16
#define PP_THERMAL_CTRLER_LM96163	17
#define PP_THERMAL_CTRLER_CI		18
#define PP_THERMAL_CTRLER_KAVERI	19

/*
 * Thermal controller 'combo type' to use an external controller for fan
 * control and an internal controller for thermal. We probably should reserve
 * the bit 0x80 for this use. To keep the number of these types low we should
 * also use the same code for all asics (i.e. do not distinguish rv6xx and
 * rv7xx internal here). The driver can pick the correct internal controller
 * based on the asic.
 */

/* adt7473 fan control + internal thermal controller */
#define PP_THERMAL_CTRLER_ADT7473_WITH_INTERNAL	0x89
/* emc2103 fan control + internal thermal controller */
#define PP_THERMAL_CTRLER_EMC2103_WITH_INTERNAL	0x8d

struct pp_thermal_ctrler {
	u8 type;
	u8 i2c_line;	/* dal i2c */
	u8 i2c_addr;
	u8 fan_params;
	u8 fan_min_rpm;	/* rpm hundred units -- for display purposes only */
	u8 fan_wax_rpm; /* rpm hundred units -- for display purposes only */
	u8 rsvd;
	u8 flgs;
} __packed;
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
#define PP_PLATFORM_CAPS_BACKBIAS			BIT(0)
#define PP_PLATFORM_CAPS_PP				BIT(1)
#define PP_PLATFORM_CAPS_SBIOS_PWR_SRC			BIT(2)
#define PP_PLATFORM_CAPS_ASPM_L0s			BIT(3)
#define PP_PLATFORM_CAPS_ASPM_L1			BIT(4)
#define PP_PLATFORM_CAPS_HW_DC				BIT(5)
#define PP_PLATFORM_CAPS_GEMINI_PRIMARY			BIT(6)
#define PP_PLATFORM_CAPS_STEP_VDDC			BIT(7)
/* XXX:should not be used, use volt_info tbl */
#define PP_PLATFORM_CAPS_VOLT_CTL			BIT(8)
#define PP_PLATFORM_CAPS_SIDEPORT_CTL			BIT(9)
#define PP_PLATFORM_CAPS_TURNOFFPLL_ASPML1		BIT(10)
#define PP_PLATFORM_CAPS_HTLINK_CTL			BIT(11)
/* XXX:should not be used, use volt_info tbl */
#define PP_PLATFORM_CAPS_MVDD_CTL			BIT(12)
/* ho to boot state on alerts (e.g. on an AC->DC transition) */
#define PP_PLATFORM_CAPS_GOTO_BOOT_ON_ALERT		BIT(13)
/* Do not wait for vblank during an alert (e.g. AC->DC transition) */
#define _PP_PLATFORM_CAPS_DONT_WAIT_FOR_VBLANK_ON_ALERT	BIT(14)
/*
 * does the driver control vddci independently from vddc
 * XXX:should not be used, use volt_info tbl
 */
#define PP_PLATFORM_CAPS_VDDCI_CTL			BIT(15)
/* enable the 'regulator hot' feature */
#define PP_PLATFORM_CAPS_REGULATOR_HOT			BIT(16)
/* does the driver supports baco state */
#define PP_PLATFORM_CAPS_BACO				BIT(17)
/* does the driver supports new cac volt tbl */
#define PP_PLATFORM_CAPS_NEW_CAC_VOLT			BIT(18)
/* does the driver supports revert gpio5 polarity */
#define PP_PLATFORM_CAPS_REVERT_GPIO5_POLARITY		BIT(19)
/* does the driver supports thermal2gpio17 */
#define PP_PLATFORM_CAPS_OUTPUT_THERMAL2GPIO17		BIT(20)
/* does the driver supports vr hot gpio Configurable */
#define PP_PLATFORM_CAPS_VRHOT_GPIO_CONFIGURABLE	BIT(21)
/* does the driver supports temp tnversion feature */
#define PP_PLATFORM_CAPS_TEMP_INVERSION			BIT(22)
#define PP_PLATFORM_CAPS_EVV				BIT(23)

struct pp0 {
	struct common_tbl_hdr hdr;
	u8 data_rev;			/* XXX: use tbl_sz instead */

	u8 states_n;

	u8 state_entry_sz;
	u8 clk_sz;
	u8 desc_sz;

	__le16 state_array_of;
	__le16 clk_array_of;
	__le16 desc_array_of;

	__le16 back_bias_time;		/* microseconds */
	__le16 volt_time;		/* microseconds */
	__le16 tbl_sz;			/* used to selection proper revision */

	__le32 platform_caps;

	struct pp_thermal_ctrler thermal_ctrler;

	__le16 boot_clk_of;
	__le16 boot_non_clk_of;
} __packed;
/*----------------------------------------------------------------------------*/

struct pp1 {
	struct pp0 pp0;
	u8 unused0;
	__le16 unused1;
} __packed;

struct pp2 {
	struct pp1 pp1;
	__le16 unused0[3];
} __packed;

/*----------------------------------------------------------------------------*/
struct volt_on_clk_dep_tbl_entry {
	__le16 clk_low;
	u8 clk_hi;
	__le16 volt_id; /* mV units or lkge idx */
} __packed;

struct volt_on_clk_dep_tbl {
	u8 entries_n;
	struct volt_on_clk_dep_tbl_entry entries[];
} __packed;

struct vddc_phase_shed_limits_tbl_entry {
	__le16 volt_mv; /* mV units only */
	__le16 sclk_low;
	u8 sclk_hi;
	__le16 mclk_low;
	u8 mclk_hi;
} __packed;

struct vddc_phase_shed_limits_tbl {
	u8 entries_n;
	struct vddc_phase_shed_limits_tbl_entry entries[];
} __packed;

struct pp3 {
	struct pp2 pp2;
	__le32 unused0[2];
	__le16 vddc_dep_on_sclk_tbl_of;
	__le16 vddci_dep_on_mclk_tbl_of;
	__le16 vddc_dep_on_mclk_tbl_of;
	__le16 clk_volt_max_on_dc_tbl_of;
	__le16 vddc_phase_shed_limits_tbl_of;
	__le16 mvdd_dep_on_mclk_tbl_of;  
} __packed;
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
struct cac_lkge_tbl_entry {
	/*
	 * We use this field for the "fake" standardized vddc for
	 * power calculations. For ci and newer, we use this as the
	 * real vddc value.  In CI we read it as std_volt_hi_sidd.
	 */
	__le16 vddc_mv;
	/*
	 * For ci and newer we use this as the "fake" standardized
	 * vddc value.  In ci we read it as std_volt_lo_sidd.
	 */
	__le32 lkge;
} __packed;

struct cac_lkge_tbl {
	u8 entries_n;
	struct cac_lkge_tbl_entry entries[];
} __packed;

struct pp4 {
	struct pp3 pp3;
	__le32 tdp_limit;
	__le32 near_tdp_limit;
	__le32 sq_ramping_threshold;	/* units of 10 kHz */
	__le16 cac_lkge_tbl_of;
	__le32 cac_lkge;		/* for driver calculated cac lkge tbl */
	__le16 unused0;
	__le16 load_line_slope;		/* mOhms * 100 */
} __packed;
/*----------------------------------------------------------------------------*/

union pp {
	struct pp0 pp0;
	struct pp1 pp1;
	struct pp2 pp2;
	struct pp3 pp3;
	struct pp4 pp4;
} __packed;

#define PP_STATE_LVL_N_MAX 5
/* array of sub pwr lvls for that a major state */
struct pp_state {
	u8 lvls_n;
	u8 desc_idx;
	u8 clk_idxes[];
} __packed;

/* array of major pwr major states */
struct pp_state_array {
	u8 n;
	u8 states[];/* struct pp_state */
} __packed;

#define PP_DESC_CLASS_UI_MASK					0x0007
#define		PP_DESC_CLASS_UI_NONE					0
#define		PP_DESC_CLASS_UI_BATTERY				1
#define		PP_DESC_CLASS_UI_BALANCED				3
#define		PP_DESC_CLASS_UI_PERFORMANCE				5
/* 2, 4, 6, 7 are reserved */
#define PP_DESC_CLASS_0_BOOT					BIT(3)
#define PP_DESC_CLASS_0_THERMAL					BIT(4)
#define PP_DESC_CLASS_0_LIMITED_PWR_SRC_0			BIT(5)
#define PP_DESC_CLASS_0_REST					BIT(6)
#define PP_DESC_CLASS_0_FORCED					BIT(7)
#define PP_DESC_CLASS_0_3DPERFORMANCE				BIT(8)
#define PP_DESC_CLASS_0_OVERDRIVETEMPLATE			BIT(9)
#define PP_DESC_CLASS_0_UVDSTATE				BIT(10)
#define PP_DESC_CLASS_0_3DLOW					BIT(11)
/* was defined for acpi */
#define PP_DESC_CLASS_0_EMERGENCY				BIT(12)
#define PP_DESC_CLASS_0_HD2STATE				BIT(13)
#define PP_DESC_CLASS_0_HDSTATE					BIT(14)
#define PP_DESC_CLASS_0_SDSTATE					BIT(15)
#define PP_DESC_CLASS_1_LIMITED_PWR_SRC_1			BIT(0)
#define PP_DESC_CLASS_1_ULV					BIT(1)
/* multi-view codec (blu-ray 3d) */
#define PP_DESC_CLASS_1_MVC					BIT(2)

#define PP_DESC_CAPS_SETTINGS_SINGLE_DISP_ONLY			0x00000001
#define PP_DESC_CAPS_SETTINGS_SUPPORTS_VIDEO_PLAYBACK		0x00000002
struct pp_desc {
	__le16 class_0;
	u8 temp_min;
	u8 temp_max;
	__le32 caps_settings;
	u8 required_pwr;
	__le16 class_1;		/* bitwise continuation of class_0 member */
	__le32 v_clk;
	__le32 d_clk;
	u8 unused[5];
} __packed;

struct pp_desc_array {
	u8 n;
	u8 entry_sz;
	u8 descs[];/* struct pp_desc */
} __packed;

struct pp_clk {
	/* 10 kHz units */
	__le16 eng_clk_low;
	u8 eng_clk_high;

	/* 10 kHz units */
	__le16 mem_clk_low;
	u8 mem_clk_high;

	__le16 vddc_id;		/* mV units or lkge idx */
	__le16 vddci_mv;	/* mV units only */
	u8 pcie_gen;
	u8 unused;

	__le32 flgs;
} __packed;

struct pp_clk_array {
	u8 n;
	u8 entry_sz;
	u8 clks[];/* struct pp_clk */
} __packed;
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
#define VOLT_TYPE_VDDC	1
#define VOLT_TYPE_MVDDC	2
#define VOLT_TYPE_MVDDQ	3
#define VOLT_TYPE_VDDCI	4

#define VOLT_ACTION_SET			0
#define VOLT_ACTION_INIT_REGULATOR	3
#define VOLT_ACTION_SET_PHASE		4
#define VOLT_ACTION_GET			6/* get from virtual volt id */
struct volt_params {
	u8 type;
	u8 action;
	__le16 lvl;
	struct hw_i2c_one_data_byte_wr_params internal;
} __packed;
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
#define MEM_ENG_SET_CLK			0x00ffffff
#define MEM_ENG_COMPUTE_OP		0x03000000	
#define		COMPUTE_MEMORY_PLL		1
#define		COMPUTE_ENGINE_PLL		2
#define		ADJUST_MC_SETTING		3
struct mem_eng_pll_compute_params {
	__le32 mem_clk;
	u8 unused1[4];
} __packed;

struct mem_eng_clk_set_params {
	__le32 clk;/* 10 kHz units */
	struct mem_eng_pll_compute_params mem_eng_pll_compute_params;
} __packed;

struct mem_eng_clk_get_params {
	__le32 clk;/* 10 kHz units */
} __packed;
/*----------------------------------------------------------------------------*/
#endif
