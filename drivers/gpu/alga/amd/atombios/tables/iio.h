#ifndef TABLES_IIO_H
#define TABLES_IIO_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#define IIO_NOP		0
#define IIO_START	1
#define IIO_READ	2
#define IIO_WRITE	3
#define IIO_CLR		4
#define IIO_SET		5
#define IIO_MOVE_IDX	6
#define IIO_MOVE_ATTR	7
#define IIO_MOVE_DATA	8
#define IIO_END		9

struct iio_nop
{
	u8 opcode;
} __packed;

struct iio_start
{
	u8 opcode;
	u8 prog_idx;
} __packed;

struct iio_readwrite
{
	u8 opcode;
	__le16 reg_u32_idx; /* index of the u32 dword */
} __packed;

struct iio_clr
{
	u8 opcode;
	u8 bits;	/* number of bits to clear */
	u8 pos;		/* position at which start those bits to clear */
} __packed;

struct iio_set
{
	u8 opcode;
	u8 bits;	/* number of bits to set */
	u8 pos;		/* position at which starts those bits to set */
} __packed;

/* same for: iio_move_idx, iio_move_data and iio_move_attr */
struct iio_move_bits
{
	u8 opcode;
	u8 sz;		/* size in bits of the index */
	u8 src_pos;	/* position of bits in the source 32 bits dword */
	u8 dst_pos;	/* position of bits in the destination 32 bits dword */
} __packed;

struct iio_end
{
	u8 opcode;
	u8 unknown[2];
} __packed;
#endif
