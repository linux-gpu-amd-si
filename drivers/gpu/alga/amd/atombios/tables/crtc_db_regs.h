#ifndef TABLES_CRTC_DB_REGS_H
#define TABLES_CRTC_DB_REGS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
 */
#define CRTC_UNLOCK	0
#define CRTC_LOCK	1
struct crtc_db_regs_params {
	u8 crtc;
	u8 state;
	u8 pad[2];
} __packed;
#endif
