#ifndef TABLES_ENC_CRTC_SRC_H
#define TABLES_ENC_CRTC_SRC_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct enc_crtc_src_params {
	u8 crtc;
	u8 enc_id;
#define ENC_CRTC_SRC_ENC_0_ID	0x03
#define ENC_CRTC_SRC_ENC_1_ID	0x09
#define ENC_CRTC_SRC_ENC_2_ID	0x0a
#define ENC_CRTC_SRC_ENC_3_ID	0x0b
#define ENC_CRTC_SRC_ENC_4_ID	0x0c
#define ENC_CRTC_SRC_ENC_5_ID	0x0d
	u8 mode;
#define ENC_CRTC_SRC_MODE_DP 0
	u8 pad;
} __packed;
#endif
