#ifndef TABLES_CRTC_SCALER_H
#define TABLES_CRTC_SCALER_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#define SCALER_DISABLE					0   
#define SCALER_CENTER					1   
#define SCALER_EXPANSION				2   
#define SCALER_MULTI_EX					3   
#define SCALER_BYPASS_AUTO_CENTER_NO_REPLICATION	0
#define SCALER_BYPASS_AUTO_CENTER_AUTO_REPLICATION	1
#define SCALER_ENA_2TAP_ALPHA_MODE			2
#define SCALER_ENA_MULTITAP_MODE			3

struct crtc_scaler_params {
	u8 crtc;
	u8 mode;
	u8 tv;
	u8 pad;
} __packed;
#endif
