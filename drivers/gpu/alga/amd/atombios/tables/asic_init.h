#ifndef TABLES_ASIC_INIT_H
#define TABLES_ASIC_INIT_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

struct asic_init_params {
	__le32 default_eng_clk;/* in 10kHz unit */
	__le32 default_mem_clk;/* in 10kHz unit */
} __packed;
#endif
