#ifndef TABLES_PLL_COMPUTE_H
#define TABLES_PLL_COMPUTE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/* common to eng_mem_pll_compute and mem_pll_compute */

/* fractional-n pll */
struct fb_div {
	__le16 frac;
	__le16 integer;
} __packed;
#endif
