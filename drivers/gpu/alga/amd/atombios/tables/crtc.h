#ifndef TABLES_CRTC_H
#define TABLES_CRTC_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define CRTC_OFF	0
#define CRTC_ON		1	
struct crtc_params {
	u8 crtc;
	u8 state;
	u8 pad[2];
} __packed;
#endif

