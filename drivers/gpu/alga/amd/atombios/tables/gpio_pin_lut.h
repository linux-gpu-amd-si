#ifndef GPIO_PIN_LUT_H
#define GPIO_PIN_LUT_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct gpio_pin_assignment
{
	__le16 reg_idx;
	u8 bit_shift;
	u8 id;
} __packed;

struct gpio_pin_lut
{
	struct common_tbl_hdr hdr;
	struct gpio_pin_assignment pin;
} __packed;
#endif
