#ifndef TABLES_MEM_PLL_COMPUTE_H
#define TABLES_MEM_PLL_COMPUTE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

struct mem_pll_compute_params {
	union {
		__le32 clk;		/* input */
		struct fb_div fb_div;	/* output */
	} __packed u1;
	u8 dll_speed;			/* output */
	u8 post_div;			/* output */
	union {
		u8 input_flgs;
		u8 pll_ctl;		/* output */
	} __packed u2;
	u8 bw_ctl;                       
} __packed;

/* mem_pll_compute_params.u2.input_flgs */
#define INPUT_FLGS_STROBE_MODE_ENA	BIT(0)

/* mem_pll_compute_params.u2.pll_ctl */
#define PLL_CTL_VCO_MODE		0x00000003 
#define PLL_CTL_BYPASS_DQ_PLL		BIT(3)
#define PLL_CTL_QDR_ENA			BIT(4)
#define PLL_CTL_AD_HALF_RATE		BIT(5)
#endif
