#ifndef TABLES_FIRMWARE_VRAM_USAGE
#define TABLES_FIRMWARE_VRAM_USAGE
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/*
 * note1: from rv770, the memory is more than 32bits addressable, so we will
 *        change from tbl_fmt_rev = 1 tbl_content_rev = 4, the structure remains
 *        exactly same as 1.1 and 1.2 (1.3 is never in use), but
 *        start_addr (in offset to start of memory address) is kB aligned
 *        instead of byte aligned.
 *
 * note2: actually it's in kernel RAM and not VRAM. VRAM comes from the fact
 *        it is actually in VRAM when in POST real mode.
 */
struct firmware_vram_reserve_info
{
	__le32 start_addr;	/* byte aligned */
	__le16 sz;		/* in kB */
	u16 rsvd;
} __packed;

struct firmware_vram_usage
{
	struct common_tbl_hdr hdr;  
	struct firmware_vram_reserve_info info;
} __packed;
#endif
