#ifndef TABLES_FIRMWARE_INFO_H
#define TABLES_FIRMWARE_INFO_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/* firmware_info_v2_2.caps */
#define FIRMWARE_INFO_CAPS_FIRMWARE_POSTED	BIT(0)
#define FIRMWARE_INFO_CAPS_DUALCRTC		BIT(1)
#define FIRMWARE_INFO_EXTENTED_DESKTOP		BIT(2)
#define FIRMWARE_INFO_MEM_SS			BIT(3)
#define FIRMWARE_INFO_ENG_SS			BIT(4)
#define FIRMWARE_INFO_GPU_CTL_BL		BIT(5)
#define FIRMWARE_INFO_WMI			BIT(6)
#define FIRMWARE_INFO_PP_MODE_ASSIGNED		BIT(7)
#define FIRMWARE_INFO_HYPER_MEM			BIT(8)
#define FIRMWARE_INFO_HYPER_MEM_SZ		0x01e0
#define FIRMWARE_INFO_POST_WITHOUT_MODESET	BIT(13)
#define FIRMWARE_INFO_SCL2_REDEFINED		BIT(14)

struct firmware_info_v2_2 {
	struct common_tbl_hdr hdr;
	__le32 firmware_rev;
	__le32 default_eng_clk;			/* in 10kHz unit */
	__le32 default_mem_clk;			/* in 10kHz unit */
	__le32 rsvd0[4];
	__le32 unused[2];
	__le32 default_disp_clk_freq;		/* in 10 kHz unit */
	u8 rsvd1;
	u8 unused1;
	__le16 default_vddc;			/* in mV unit */
	__le16 unused2[2];
	__le32 rsvd2;
	__le32 unused3;
	u8 unused4;
	u8 rsvd3[3];
	__le32 rsvd4[2];
	__le16 rvsd5;
	__le16 unused5[2];
	__le16 default_vddci;			/* in mV unit */
	__le16 caps;
	__le16 core_ref_clk;			/* in 10 kHz unit */
	__le16 mem_ref_clk;			/* in 10 kHz unit */
	__le16 uniphy_dp_mode_ext_clk_freq;	/* in 10kHz unit, if 0; dp uses
						   internal pll as input clock,
						   else dp uses an external
						   clock */
	u8 unused6;
	u8 rsvd6[3];
	__le16 default_mvddc;			/* in mV unit */
	__le16 rsvd7;
	__le32 rsvd8[3];
} __packed;
#endif
