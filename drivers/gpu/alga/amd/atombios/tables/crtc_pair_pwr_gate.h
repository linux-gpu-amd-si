#ifndef TABLES_CRTC_PAIR_POWERGATE_H
#define TABLES_CRTC_PAIR_POWERGATE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/*
 * works on crtc pair
 * 0 and 1
 * 2 and 3
 * 4 and 5
 */
#define CRTC_PAIR_PWR_GATE_OFF	0
#define CRTC_PAIR_PWR_GATE_ON	1
struct crtc_pair_pwr_gate_params {
	u8 crtc;
	u8 state;
	u8 pad[2];
} __packed;
#endif
