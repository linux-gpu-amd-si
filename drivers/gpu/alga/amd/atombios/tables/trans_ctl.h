#ifndef TABLES_TRANS_CTL_H
#define TABLES_TRANS_CTL_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct trans_ctl_params {
	__le16 symbol_clk;	/* link_clock/10 in 10kHz units */
	u8 id;
	u8 action;
#define TRANS_CTL_ACTION_DISABLE		0
#define TRANS_CTL_ACTION_ENA			1
#define TRANS_CTL_ACTION_LCD_BLOFF		2
#define TRANS_CTL_ACTION_LCD_BLON		3
#define TRANS_CTL_ACTION_BL_BRIGHTNESS_CTL	4
#define TRANS_CTL_ACTION_LCD_SELFTEST_START	5
#define TRANS_CTL_ACTION_LCD_SELFTEST_STOP	6
#define TRANS_CTL_ACTION_INIT			7
#define TRANS_CTL_ACTION_DISABLE_OUTPUT		8
#define TRANS_CTL_ACTION_ENA_OUTPUT		9
#define TRANS_CTL_ACTION_SETUP			10
#define TRANS_CTL_ACTION_SETUP_VSEMPH		11
#define TRANS_CTL_ACTION_PWR_ON			12
#define TRANS_CTL_ACTION_PWR_OFF		13
	u8 lanes_n;		/* lane number 1-8 */
	u8 conn_sub_id;		/* connector sub id */
	u8 mode;		/* mode, dp mode is 0 */
	u8 cfg;
#define TRANS_CTL_CFG_HPD_MASK		0x70
#define TRANS_CTL_CFG_HPD_SHIFT		4
#define		TRANS_CTL_CFG_NO_HPD		0
#define		TRANS_CTL_CFG_HPD0		1
#define		TRANS_CTL_CFG_HPD1		2
#define		TRANS_CTL_CFG_HPD2		3
#define		TRANS_CTL_CFG_HPD3		4
#define		TRANS_CTL_CFG_HPD4		5
#define		TRANS_CTL_CFG_HPD5		6
#define TRANS_CTL_CFG_CLK_REF_ID_MASK	0x0c
#define TRANS_CTL_CFG_CLK_REF_ID_SHIFT	2
#define		TRANS_CTL_CFG_PPLL1		0
#define		TRANS_CTL_CFG_PPLL2		1
#define		TRANS_CTL_CFG_PPLL0		2  
#define		TRANS_CTL_CFG_EXT		3
#define TRANS_CTL_CFG_COHERENT_MODE	BIT(1)
	u8 enc;			/* encoder id */
#define TRANS_CTL_ENC_0_ID	BIT(0)
#define TRANS_CTL_ENC_1_ID	BIT(1)
#define TRANS_CTL_ENC_2_ID	BIT(2)
#define TRANS_CTL_ENC_3_ID	BIT(3)
#define TRANS_CTL_ENC_4_ID	BIT(4)
#define TRANS_CTL_ENC_5_ID	BIT(5)
#define TRANS_CTL_ENC_6_ID	BIT(6)
	u8 lanes_set;
#define TRANS_CTL_LANES_SET_V_MASK	0x07
#define TRANS_CTL_LANES_SET_V_SHIFT	0
#define 	TRANS_CTL_LANES_SET_0_4V	0
#define 	TRANS_CTL_LANES_SET_0_6V	1
#define 	TRANS_CTL_LANES_SET_0_8V	2
#define 	TRANS_CTL_LANES_SET_1_2V	3
#define TRANS_CTL_LANES_SET_DB_MASK	0x18
#define TRANS_CTL_LANES_SET_DB_SHIFT	3
#define		TRANS_CTL_LANES_SET_0_0DB	0
#define		TRANS_CTL_LANES_SET_3_5DB	1
#define		TRANS_CTL_LANES_SET_6_0DB	2
#define		TRANS_CTL_LANES_SET_9_5DB	3
	u8 rsvd[2];
} __packed;
#endif
