#ifndef TABLES_CRTC_BLANK_H
#define TABLES_CRTC_BLANK_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#define CRTC_BLANK_OFF	0
#define CRTC_BLANK_ON	1	
struct crtc_blank_params {
	u8 crtc;
	u8 state;
	__le16 black_color_rcr;
	__le16 black_color_gy;
	__le16 black_color_bcb;
} __packed;
#endif
