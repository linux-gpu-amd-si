#ifndef TABLES_DATA_H
#define TABLES_DATA_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/* structures used in data.mtb */
struct data_tbls_list
{
	u16 unused0[4];

	__le16 firmware_info;

	u16 unused1[6];

	__le16 firmware_vram_usage;
	__le16 gpio_pin_lut;

	u16 unused2[2];

	__le16 pp_info;

	u16 unused3[6];

	__le16 obj_hdr;
	__le16 iio;

	u16 unused4[2];

	__le16 ss_info;

	u16 unused5;

	__le16 vram_info;

	u16 unused6[3];

	u16 volt_info;

	u16 unused7;
} __packed;

struct master_data_tbl
{ 
	u8 unused[4];
	struct data_tbls_list list;
} __packed;
#endif
