#ifndef TABLES_DP_AUX_H
#define TABLES_DP_AUX_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

struct dp_aux_params {/* DCE4 */
	__le16 req;
	__le16 reply_data_of;
	u8 i2c_id;
	union {
		u8 reply_status;
		u8 delay;
	};
	u8 reply_data_sz;
	u8 hpd;
} __packed;
#endif
