#ifndef TABLES_VRAM_INFO_H
#define TABLES_VRAM_INFO_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

#define REG_SETS_N_MAX		20
#define REGS_N_MAX		32

#define VRAM_MODULES_N_MAX	16
#define MEM_PART_NUM_MAX	20

#define REG_SET_MISC_MODULE_IDX	0xff000000
#define REG_SET_MISC_CLK_MAX	0x00ffffff
#define REG_SET_MISC_TERMINATOR	0x00000000

struct reg_set_partial {
	__le32 misc;
	/*
	 * tbl of __le32 values for registers defined in the idxs array
	 * see MISC_REG_VAL_FROM_TBL
	 */
} __packed;

#define REG_IDX_MISC_TERMINATOR		0x80
#define REG_IDX_MISC_REG_VAL_LOCATION	0x0f
/* same value then in the previous set */
#define REG_IDX_MISC_REG_VAL_FROM_PREV_VAL	0x0
/* value sequentially from the tbl of __le32 values from the current set */
#define REG_IDX_MISC_REG_VAL_FROM_TBL		0x4
struct reg_idx {
	__le16 idx;		/* this is the dword index of the register */
	u8 misc;
} __packed;

struct reg_tbl_partial {
	/* size of reg_idx array including terminating element size*/
	__le16 idxs_sz;		
	__le16 set_sz;	/* size of struct set */
	/* packed struct reg_idx array with a terminating element */
	/* packed struct set array */
} __packed;

struct vram_module {/* ver 7 */

	/* design specific values */

	__le32 chan_map_cfg;	/* mm MC_SHARED_CHREMAP */
	__le16 module_sz;	/* struct size */
	/* MC_ARB_RAMCFG (includes BANKS_N, RANKS_N, ROWS_N, COLS_N */
	__le16 priv_rsvd;	
	__le16 chans_ena;	/* bit vector which channels are enabled */
	u8 ext_mem_id;		/* mem module id */
	u8 mem_type;		/* ddr2/ddr3/gddr3/gddr5 */
	/* number of mem channels supported by this module */	
	u8 chans_n;
	u8 chans_w;		/* width, 16bits, 32bits, 64bits */
	u8 density;		/* 8mx32, 16mx32, 16m16, 32m16 */
	u8 rsvd0;
	u8 misc;		/* rank of this memory module */
	u8 unused;
	/* Round trip delay (MC_SEQ_CAS_TIMING [28:24]:TCL=CL+NPL_RT-2). Always 2. */
	u8 npl_rt;
	/* MC_SEQ_CAS_TIMING [7:4] write preamble, [3:0] read preamble */
	u8 preamble;
	/* in 16MB units, for CFG_MEM_SZ, bits[23:0] zeros */
	u8 mem_sz;
	__le16 seq_setting_of;
	u8 rsvd1;

	/* memory module specific values */

	__le16 emrs2_val;	/* emrs2/mr2 value */
	__le16 emrs3_val;	/* emrs3/mr3 value */
	u8 mem_vendor_id;	/* [7:4] revision, [3:0] vendor code */
	/* [1:0] refresh factor, 00=8ms, 01=16ms, 10=32ms, 11=64ms */
	u8 refresh_rate_factor;
	/* fifo depth can be detected during vendor detection, hardcoded here */
	u8 fifo_depth;
	/* [7:4] write cdr bandwidth, [3:0] read cdr bandwidth */
	u8 cdr_bandwidth;
	/* 0 terminated part number string */
	u8 mem_part_numr[MEM_PART_NUM_MAX];
} __packed;

struct vram_info {/* ver 2.1 */
	struct common_tbl_hdr hdr;
	/*
	 * offset of reg_tbl structure for memory vendor specific memory
	 * controller adjust setting
	 */
	__le16 mem_adjust_tbl_of;	
	/*
	* offset of reg_tbl structure for memory clk specific memory
	* controller setting
	*/
	__le16 mem_clk_patch_tbl_of;
	/*
	 * offset of reg_tbl structure for per byte offset preset
	 * settings
	 */
	__le16 per_byte_preset_of;
	u16 rsvd0[3];
	u8 modules_n;
	/* version of memory ac timing register list */
	u8 mem_clk_patch_tbl_ver;
	u8 module_ver;	/* should be 7 here */
	u8 rsvd1;
	struct vram_module modules[VRAM_MODULES_N_MAX];
} __packed;
#endif
