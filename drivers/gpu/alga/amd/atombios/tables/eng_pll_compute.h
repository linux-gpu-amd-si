#ifndef TABLES_ENG_PLL_COMPUTE_H
#define TABLES_ENG_PLL_COMPUTE_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/* upstream v3 */
struct eng_pll_compute_params {
	union {
		__le32 clk;		/* input */
		struct fb_div fb_div;	/* output */
	} __packed u;
	u8 ref_div;			/* output */
	u8 post_div; 			/* output */
	u8 flgs;			/* output */
	u8 rsvd;
} __packed;

/* eng_pll_compute_params.flgs */
#define PLL_POST_DIV_ENA	BIT(0)	
#define MEM_PLL_VCO_MODE	BIT(1)
#define FRAC_DIS		BIT(2)
#define ENG_PLL_ISPARE_9	BIT(3)
#endif
