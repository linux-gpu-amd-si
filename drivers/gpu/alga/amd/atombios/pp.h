#ifndef PP_H
#define PP_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
 */
long pp_platform_caps_get(struct atombios *atb, u32 *platform_caps);
#endif
