/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/* mainly used for the messages which are too verbose */
#ifdef CONFIG_ALGA_AMD_ATOMBIOS_LOG_VERBOSE

#  define DEV_INFOC(dev,fmt,...) \
dev_info((dev), "atombios:" fmt "\n", ##__VA_ARGS__)

#else

#  define DEV_INFOC(dev,fmt,...)

#endif
