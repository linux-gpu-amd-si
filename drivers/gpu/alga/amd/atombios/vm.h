#ifndef VM_H
#define VM_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long volt_get(struct atombios *atb, u8 type, u16 id, u16 *mv);
#endif
