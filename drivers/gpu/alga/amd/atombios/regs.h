#ifndef REGS_H
#define REGS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#define SET(n, x) (((x) << n##_SHIFT) & n##_MASK)
#define GET(n, x) (((x) & n##_MASK) >> n##_SHIFT)

/* scratch pad registers */
#define S0	0x1724
#define S1	0x1728
#define S2	0x172c
#define S3	0x1730
#define S4	0x1734
#define S5	0x1738
#define S6	0x173c
#define S7	0x1740
#endif
