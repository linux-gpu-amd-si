#ifndef SCRATCH_PADS_H
#define SCRATCH_PADS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~scratch pad 0~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define S0_DFP0	0x00010000L
#define S0_DFP1	0x00020000L
#define S0_LCD1	0x00040000L
#define S0_LCD2	0x00080000L
#define S0_DFP5	0x00100000L
#define S0_DFP2	0x00200000L
#define S0_DFP3	0x00400000L
#define S0_DFP4	0x00800000L

static u32 vals_s0_dfp[DFPx_MAX] __attribute__ ((unused)) = {
	S0_DFP0,
	S0_DFP1,
	S0_DFP2,
	S0_DFP3,
	S0_DFP4,
	S0_DFP5
};
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~scratch pad 0 END~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~scratch pad 2~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define S2_TV1_STANDARD_MASK	0x0000000fL
#define S2_CURRENT_BL_LVL_MASK	0x0000ff00L
#define S2_CURRENT_BL_LVL_SHIFT	8

#define S2_FORCED_LOW_PWR_MODE_STATE_MASK		0x0c000000L
#define S2_FORCED_LOW_PWR_MODE_STATE_MASK_SHIFT		26
#define S2_FORCED_LOW_PWR_MODE_STATE_CHANGE		0x10000000L

#define S2_DEV_DPMS_STATE		0x00010000L
#define S2_VRI_BRIGHT_ENA		0x20000000L

#define S2_DISP_ROTATION_0_DEGREE		0x0
#define S2_DISP_ROTATION_90_DEGREE		0x1
#define S2_DISP_ROTATION_180_DEGREE		0x2
#define S2_DISP_ROTATION_270_DEGREE		0x3
#define S2_DISP_ROTATION_DEGREE_SHIFT		30
#define S2_DISP_ROTATION_ANGLE_MASK		0xc0000000L

/* byte aligned definitions for bios usage */
#define S2_B0_TV1_STANDARD_MASK		0x0f
#define S2_B1_CURRENT_BL_LEVEL_MASK	0xff
#define S2_B2_DEVICE_DPMS_STATE		0x01

#define S2_W1_DEV_DPMS_MASK				0x3ff
#define S2_B3_FORCED_LOW_PWR_MODE_STATE_MASK		0x0C
#define S2_B3_FORCED_LOW_PWR_MODE_STATE_CHANGE		0x10
#define S2_B3_VRI_BRIGHT_ENA				0x20
#define S2_B3_ROTATION_STATE_MASK			0xC0
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~scratch pad 2 END~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~scratch pad 3~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define S3_CRT1_ACTIVE	0x00000001L
#define S3_LCD1_ACTIVE	0x00000002L
#define S3_TV1_ACTIVE	0x00000004L
#define S3_DFP0_ACTIVE	0x00000008L
#define S3_CRT2_ACTIVE	0x00000010L
#define S3_LCD2_ACTIVE	0x00000020L
#define S3_DFP5_ACTIVE	0x00000040L
#define S3_DFP1_ACTIVE	0x00000080L
#define S3_CV_ACTIVE	0x00000100L
#define S3_DFP2_ACTIVE	0x00000200L
#define S3_DFP3_ACTIVE	0x00000400L
#define S3_DFP4_ACTIVE	0x00000800L

static u32 vals_s3_dfp[DFPx_MAX] __attribute__ ((unused)) = {
	S3_DFP0_ACTIVE,
	S3_DFP1_ACTIVE,
	S3_DFP2_ACTIVE,
	S3_DFP3_ACTIVE,
	S3_DFP4_ACTIVE,
	S3_DFP5_ACTIVE
};

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~scratch pad 3 END~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~scratch pad 5~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* scrach pad 5 is used by firmware only */
#define S5_B0_DOS_REQ_CRT1	0x01
#define S5_B0_DOS_REQ_LCD1	0x02
#define S5_B0_DOS_REQ_TV1	0x04
#define S5_B0_DOS_REQ_DFP1	0x08
#define S5_B0_DOS_REQ_CRT2	0x10
#define S5_B0_DOS_REQ_LCD2	0x20
#define S5_B0_DOS_REQ_DFP6	0x40
#define S5_B0_DOS_REQ_DFP2	0x80
#define S5_B1_DOS_REQ_CV	0x01
#define S5_B1_DOS_REQ_DFP3	0x02
#define S5_B1_DOS_REQ_DFP4	0x04
#define S5_B1_DOS_REQ_DFP5	0x08

#define S5_W0_DOS_REQ_DEV	0x0fff

#define S5_DOS_REQ_CRT1	0x0001
#define S5_DOS_REQ_LCD1	0x0002
#define S5_DOS_REQ_TV1	0x0004
#define S5_DOS_REQ_DFP1	0x0008
#define S5_DOS_REQ_CRT2	0x0010
#define S5_DOS_REQ_LCD2	0x0020
#define S5_DOS_REQ_DFP6	0x0040
#define S5_DOS_REQ_DFP2	0x0080
#define S5_DOS_REQ_CV	0x0100
#define S5_DOS_REQ_DFP3	0x0200
#define S5_DOS_REQ_DFP4	0x0400
#define S5_DOS_REQ_DFP5	0x0800

#define S5_B2_DOS_FORCE_CRT1	S5_B0_DOS_REQ_CRT1
#define S5_B2_DOS_FORCE_TV1	S5_B0_DOS_REQ_TV1
#define S5_B2_DOS_FORCE_CRT2	S5_B0_DOS_REQ_CRT2
#define S5_B3_DOS_FORCE_CV	S5_B1_DOS_REQ_CV
#define S5_W1_DOS_FORCE_DEV	(S5_B2_DOS_FORCE_CRT1 \
				+ S5_B2_DOS_FORCE_TV1 \
				+ S5_B2_DOS_FORCE_CRT2 \
				+ (S5_B3_DOS_FORCE_CV << 8))
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~scratch pad 5 END~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~scracth pad 6~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define S6_DEV_CHANGE				0x00000001L
#define S6_SCALER_CHANGE			0x00000002L
#define S6_LID_CHANGE				0x00000004L
#define S6_DOCKING_CHANGE			0x00000008L
#define S6_ACC_MODE				0x00000010L
#define S6_EXT_DESKTOP_MODE			0x00000020L
#define S6_LID_STATE				0x00000040L
#define S6_DOCK_STATE				0x00000080L
#define S6_CRITICAL_STATE			0x00000100L
#define S6_HW_I2C_BUSY_STATE			0x00000200L
#define S6_THERMAL_STATE_CHANGE			0x00000400L
#define S6_INTERRUPT_SET_BY_BIOS		0x00000800L
/* normal expansion request bit for lcd */
#define S6_REQ_LCD_EXPANSION_FULL		0x00001000L
/* aspect ratio expansion request bit for lcd */
#define S6_REQ_LCD_EXPANSION_ASPEC_RATIO	0x00002000L

/*
 * this bit is recycled when ATB_BIOS_INFO_BIOS_SCRATCH6_SCL2_REDEFINE is set,
 * previously it's scl2_h_expansion
 */
#define S6_DISP_STATE_CHANGE			0x00004000L
/*
 * this bit is recycled when ATB_BIOS_INFO_BIOS_SCRATCH6_SCL2_REDEFINE is set,
 * previously it's scl2_v_expansion
 */
#define S6_I2C_STATE_CHANGE			0x00008000L

#define S6_ACC_REQ_CRT1				0x00010000L
#define S6_ACC_REQ_LCD1				0x00020000L
#define S6_ACC_REQ_TV1				0x00040000L
#define S6_ACC_REQ_DFP0				0x00080000L
#define S6_ACC_REQ_CRT2				0x00100000L
#define S6_ACC_REQ_LCD2				0x00200000L
#define S6_ACC_REQ_DFP5				0x00400000L
#define S6_ACC_REQ_DFP1				0x00800000L
#define S6_ACC_REQ_CV				0x01000000L
#define S6_ACC_REQ_DFP2				0x02000000L
#define S6_ACC_REQ_DFP3				0x04000000L
#define S6_ACC_REQ_DFP4				0x08000000L

static u32 vals_s6_dfp[DFPx_MAX] __attribute__ ((unused)) = {
	S6_ACC_REQ_DFP0,
	S6_ACC_REQ_DFP1,
	S6_ACC_REQ_DFP2,
	S6_ACC_REQ_DFP3,
	S6_ACC_REQ_DFP4,
	S6_ACC_REQ_DFP5
};

#define S6_ACC_REQ_MASK				0x0fff0000L
#define S6_SYSTEM_PWR_MODE_CHANGE		0x10000000L
#define S6_ACC_BLK_DISP_SWITCH			0x20000000L
#define S6_VRI_BRIGHTNESS_CHANGE		0x40000000L
#define S6_CFG_DISP_CHANGE_MASK			0x80000000L

/* byte aligned definitions for bios usage */
#define S6_DEV_CHANGE_B0			0x01
#define S6_SCALER_CHANGE_B0			0x02
#define S6_LID_CHANGE_B0			0x04
#define S6_DOCKING_CHANGE_B0			0x08
#define S6_ACC_MODE_B0				0x10
#define S6_EXT_DESKTOP_MODE_B0			0x20
#define S6_LID_STATE_B0				0x40
#define S6_DOCK_STATE_B0			0x80
#define S6_B1_CRITICAL_STATE			0x01
#define S6_B1_HW_I2C_BUSY_STATE			0x02
#define S6_B1_THERMAL_STATE_CHANGE		0x04
#define S6_B1_INTERRUPT_SET_BY_BIOS		0x08
#define S6_B1_REQ_LCD_EXPANSION_FULL		0x10
#define S6_B1_REQ_LCD_EXPANSION_ASPEC_RATIO	0x20

#define S6_B2_ACC_REQ_CRT1	0x01
#define S6_B2_ACC_REQ_LCD1	0x02
#define S6_B2_ACC_REQ_TV1	0x04
#define S6_B2_ACC_REQ_DFP1	0x08
#define S6_B2_ACC_REQ_CRT2	0x10
#define S6_B2_ACC_REQ_LCD2	0x20
#define S6_B2_ACC_REQ_DFP6	0x40
#define S6_B2_ACC_REQ_DFP2	0x80
#define S6_B3_ACC_REQ_CV	0x01
#define S6_B3_ACC_REQ_DFP3	0x02
#define S6_B3_ACC_REQ_DFP4	0x04
#define S6_B3_ACC_REQ_DFP5	0x08

#define S6_W1_ACC_REQ_DEVw1		S5_W0_DOS_REQ_DEV
#define S6_B3_SYS_PWR_MODE_CHANGE	0x10
#define S6_B3_ACC_BLK_DISP_SWITCH	0x20
#define S6_B3_VRI_BRIGHTNESS_CHANGE	0x40
#define S6_B3_CFG_DISP_CHANGE		0x80

#define S6_DEV_CHANGE_SHIFT		0
#define S6_SCALER_CHANGE_SHIFT		1
#define S6_LID_CHANGE_SHIFT		2
#define S6_DOCKING_CHANGE_SHIFT		3
#define S6_ACC_MODE_SHIFT		4
#define S6_EXT_DESKTOP_MODE_SHIFT	5
#define S6_LID_STATE_SHIFT		6
#define S6_DOCK_STATE_SHIFT		7
#define S6_CRITICAL_STATE_SHIFT		8
#define S6_HW_I2C_BUSY_STATE_SHIFT	9
#define S6_THERMAL_STATE_CHANGE_SHIFT	10
#define S6_INTERRUPT_SET_BY_BIOS_SHIFT	11
#define S6_REQ_SCALER_SHIFT		12
#define S6_REQ_SCALER_ARATIO_SHIFT	13
#define S6_DISP_STATE_CHANGE_SHIFT	14
#define S6_I2C_STATE_CHANGE_SHIFT	15
#define S6_SYS_PWR_MODE_CHANGE_SHIFT	28
#define S6_ACC_BLK_DISP_SWITCH_SHIFT	29
#define S6_VRI_BRIGHTNESS_CHANGE_SHIFT	30
#define S6_CFG_DISP_CHANGE_SHIFT	31
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~scratch pad 6 END~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#endif
