#ifndef _IIO_INTERPRETER_H
#define _IIO_INTERPRETER_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
 */
int iio_setup(struct atombios *atb);
u32 iio_interpret(struct ictx *ictx, u16 iio_prog, u32 idx, u32 data);
#endif /* _IIO_INTERPRETER_H */
