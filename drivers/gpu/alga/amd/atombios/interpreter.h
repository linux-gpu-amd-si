#ifndef INTERPRETER_H
#define INTERPRETER_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

struct ictx
{
	struct atombios *atb;
	u16 tbl;
	u32 ps_frame;
	u32 ps_frame_dws;
	u32 *ws;
	u8 abort;
};

#define IO_MM			0	/* AMD port: direct */
#define IO_PCI			1
#define IO_SYSIO		2
#define IO_IIO			0x80	/* AMD port: indirect */

long interpret(struct atombios *atb, u16 tbl, u32 ps_frame, u8 idx);
#endif /* _INTERPRETER_H  */
