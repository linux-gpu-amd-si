#ifndef SYSFS_H
#define SYSFS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long sysfs_add(struct dce6 *dce, u8 i);
void sysfs_remove(struct dce6 *dce, u8 i);
#endif
