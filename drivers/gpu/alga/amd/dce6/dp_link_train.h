#ifndef DP_LINK_TRAIN_H
#define DP_LINK_TRAIN_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long dp_link_train(struct dce6 *dce, u8 i);
#endif
