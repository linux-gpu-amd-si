#ifndef HPD_H
#define HPD_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
void hpds_init(struct dce6 *dce);
void hpd_polarity_rearm(struct dce6 *dce, u8 hpd, u8 connected);
void hpds_polarity_refresh(struct dce6 *dce);
u8 hpd_sense(struct dce6 *dce, u8 hpd);
long hpd_irq(struct dce6 *dce, u8 hpd);
void hpds_off(struct dce6 *dce);
#endif
