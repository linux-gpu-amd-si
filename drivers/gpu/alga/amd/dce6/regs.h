#ifndef REGS_H
#define REGS_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/

/*
 * Many masks are too large because we need the info from AMD
 * or need to deduce it from other values in the register.
 * Components of a register are prefix with the initals of that register in
 * order to get a kind of unicity
 */

static inline u32 set(u32 mask, u32 v)
{
	u8 shift;

	shift = ffs(mask) - 1;
	return (v << shift) & mask;
}

static inline u32 get(u32 mask, u32 v)
{
	u8 shift;

	shift = ffs(mask) - 1;
	return (v & mask) >> shift;
}

#define	VGA_RENDER_CTL				0x300
#define		VRC_VGA_VSTATUS_CTL_MASK		0x00030000
#define		VRC_VGA_VSTATUS_CTL_CLR			0xfffcffff

#define VGA_MEM_BASE_ADDR			0x310
#define VGA_MEM_BASE_ADDR_HIGH			0x324

#define	VGA_HDP_CTL				0x328
#define		VHC_VGA_MEM_PAGE_SELECT_ENA		BIT(0)
#define		VHC_VGA_MEM_DIS				BIT(4)
#define		VHC_VGA_RBBM_LOCK_DIS			BIT(8)
#define		VHC_VGA_SOFT_RESET			BIT(16)

#define	D0VGA_CTL				0x330
#define		DC_DVGA_CTL_MODE_ENA			BIT(0)
#define		DC_DVGA_CTL_TIMING_SELECT		BIT(8)
#define		DC_DVGA_CTL_SYNC_POLARITY_SELECT	BIT(9)
#define		DC_DVGA_CTL_OVERSCAN_TIMING_SELECT	BIT(10)
#define		DC_DVGA_CTL_OVERSCAN_COLOR_ENA		BIT(16)
#define		DC_DVGA_CTL_ROTATE			BIT(24)
#define D1VGA_CTL				0x338
#define		DC_DVGA_CTL_MODE_ENA			BIT(0)
#define		DC_DVGA_CTL_TIMING_SELECT		BIT(8)
#define		DC_DVGA_CTL_SYNC_POLARITY_SELECT	BIT(9)
#define		DC_DVGA_CTL_OVERSCAN_TIMING_SELECT	BIT(10)
#define		DC_DVGA_CTL_OVERSCAN_COLOR_ENA		BIT(16)
#define		DC_DVGA_CTL_ROTATE			BIT(24)
#define D2VGA_CTL                         	0x3e0
#define		DC_DVGA_CTL_MODE_ENA			BIT(0)
#define		DC_DVGA_CTL_TIMING_SELECT		BIT(8)
#define		DC_DVGA_CTL_SYNC_POLARITY_SELECT	BIT(9)
#define		DC_DVGA_CTL_OVERSCAN_TIMING_SELECT	BIT(10)
#define		DC_DVGA_CTL_OVERSCAN_COLOR_ENA		BIT(16)
#define		DC_DVGA_CTL_ROTATE			BIT(24)
#define D3VGA_CTL                         	0x3e4
#define		DC_DVGA_CTL_MODE_ENA			BIT(0)
#define		DC_DVGA_CTL_TIMING_SELECT		BIT(8)
#define		DC_DVGA_CTL_SYNC_POLARITY_SELECT	BIT(9)
#define		DC_DVGA_CTL_OVERSCAN_TIMING_SELECT	BIT(10)
#define		DC_DVGA_CTL_OVERSCAN_COLOR_ENA		BIT(16)
#define		DC_DVGA_CTL_ROTATE			BIT(24)
#define D4VGA_CTL                         	0x3e8
#define		DC_DVGA_CTL_MODE_ENA			BIT(0)
#define		DC_DVGA_CTL_TIMING_SELECT		BIT(8)
#define		DC_DVGA_CTL_SYNC_POLARITY_SELECT	BIT(9)
#define		DC_DVGA_CTL_OVERSCAN_TIMING_SELECT	BIT(10)
#define		DC_DVGA_CTL_OVERSCAN_COLOR_ENA		BIT(16)
#define		DC_DVGA_CTL_ROTATE			BIT(24)
#define D5VGA_CTL                         	0x3ec
#define		DC_DVGA_CTL_MODE_ENA			BIT(0)
#define		DC_DVGA_CTL_TIMING_SELECT		BIT(8)
#define		DC_DVGA_CTL_SYNC_POLARITY_SELECT	BIT(9)
#define		DC_DVGA_CTL_OVERSCAN_TIMING_SELECT	BIT(10)
#define		DC_DVGA_CTL_OVERSCAN_COLOR_ENA		BIT(16)
#define		DC_DVGA_CTL_ROTATE			BIT(24)

#define DMIF_ADDR_CFG				0xbd4
#define DMIF_ADDR_CALC				0xc00

/*----------------------------------------------------------------------------*/
#define HPD0_INT_STATUS				0x601c
#define		HIS_STATUS				BIT(0)
#define		HIS_STATUS_SENSE			BIT(1)
#define		HIS_STATUS_RX				BIT(8)
#define HPD0_INT_CTL				0x6020
#define		HIC_INT_ACK				BIT(0)
#define		HIC_INT_POLARITY			BIT(8)
#define		HIC_INT_ENA				BIT(16)
#define		HIC_RX_INT_ACK				BIT(20)
#define		HIC_RX_INT_ENA				BIT(24)
#define HPD0_CTL				0x6024
/* timers are micro seconds */
#define		HC_CONN_TIMER			0xffffffff
#define		HC_RX_INT_TIMER			0xffff0000
#define		HC_ENA				BIT(28)

#define HPD1_INT_STATUS				0x6028
#define		HIS_STATUS				BIT(0)
#define		HIS_STATUS_SENSE			BIT(1)
#define		HIS_STATUS_RX				BIT(8)
#define HPD1_INT_CTL				0x602c
#define		HIC_INT_ACK				BIT(0)
#define		HIC_INT_POLARITY			BIT(8)
#define		HIC_INT_ENA				BIT(16)
#define		HIC_RX_INT_ACK				BIT(20)
#define		HIC_RX_INT_ENA				BIT(24)
#define HPD1_CTL				0x6030
/* timers are micro seconds */
#define		HC_CONN_TIMER			0xffffffff
#define		HC_RX_INT_TIMER			0xffff0000
#define		HC_ENA				BIT(28)

#define HPD2_INT_STATUS				0x6034
#define		HIS_STATUS				BIT(0)
#define		HIS_STATUS_SENSE			BIT(1)
#define		HIS_STATUS_RX				BIT(8)
#define HPD2_INT_CTL				0x6038
#define		HIC_INT_ACK				BIT(0)
#define		HIC_INT_POLARITY			BIT(8)
#define		HIC_INT_ENA				BIT(16)
#define		HIC_RX_INT_ACK				BIT(20)
#define		HIC_RX_INT_ENA				BIT(24)
#define HPD2_CTL				0x603c
/* timers are micro seconds */
#define		HC_CONN_TIMER			0xffffffff
#define		HC_RX_INT_TIMER			0xffff0000
#define		HC_ENA				BIT(28)

#define HPD3_INT_STATUS				0x6040
#define		HIS_STATUS				BIT(0)
#define		HIS_STATUS_SENSE			BIT(1)
#define		HIS_STATUS_RX				BIT(8)
#define HPD3_INT_CTL				0x6044
#define		HIC_INT_ACK				BIT(0)
#define		HIC_INT_POLARITY			BIT(8)
#define		HIC_INT_ENA				BIT(16)
#define		HIC_RX_INT_ACK				BIT(20)
#define		HIC_RX_INT_ENA				BIT(24)
#define HPD3_CTL				0x6048
/* timers are micro seconds */
#define		HC_CONN_TIMER			0xffffffff
#define		HC_RX_INT_TIMER			0xffff0000
#define		HC_ENA				BIT(28)

#define HPD4_INT_STATUS				0x604c
#define		HIS_STATUS				BIT(0)
#define		HIS_STATUS_SENSE			BIT(1)
#define		HIS_STATUS_RX				BIT(8)
#define HPD4_INT_CTL				0x6050
#define		HIC_INT_ACK				BIT(0)
#define		HIC_INT_POLARITY			BIT(8)
#define		HIC_INT_ENA				BIT(16)
#define		HIC_RX_INT_ACK				BIT(20)
#define		HIC_RX_INT_ENA				BIT(24)
#define HPD4_CTL				0x6054
/* timers are micro seconds */
#define		HC_CONN_TIMER			0xffffffff
#define		HC_RX_INT_TIMER			0xffff0000
#define		HC_ENA				BIT(28)

#define HPD5_INT_STATUS				0x6058
#define		HIS_STATUS				BIT(0)
#define		HIS_STATUS_SENSE			BIT(1)
#define		HIS_STATUS_RX				BIT(8)
#define HPD5_INT_CTL				0x605c
#define		HIC_INT_ACK				BIT(0)
#define		HIC_INT_POLARITY			BIT(8)
#define		HIC_INT_ENA				BIT(16)
#define		HIC_RX_INT_ACK				BIT(20)
#define		HIC_RX_INT_ENA				BIT(24)
#define HPD5_CTL				0x6060
/* timers are micro seconds */
#define		HC_CONN_TIMER			0xffffffff
#define		HC_RX_INT_TIMER			0xffff0000
#define		HC_ENA				BIT(28)
/*----------------------------------------------------------------------------*/

#define DISP0_INT_STATUS			0x60f4
#define		DIS_LB_D0_VLINE_INT			BIT(2)
#define		DIS_LB_D0_VBLANK_INT			BIT(3)
#define		DIS_HPD0_INT				BIT(17)
#define		DIS_HPD0_RX_INT				BIT(18)
#define		DIS_DACA_AUTODETECT_INT			BIT(22)
#define		DIS_DACB_AUTODETECT_INT			BIT(23)
#define		DIS_I2C_SW_DONE_INT			BIT(24)
#define		DIS_I2C_HW_DONE_INT			BIT(25)
#define DISP1_INT_STATUS			0x60f8
#define		DIS_LB_D1_VLINE_INT			BIT(2)
#define		DIS_LB_D1_VBLANK_INT			BIT(3)
#define		DIS_HPD1_INT				BIT(17)
#define		DIS_HPD1_RX_INT				BIT(18)
#define		DIS_DISP_TIMER_INT			BIT(24)
#define DISP2_INT_STATUS			0x60fc
#define		DIS_LB_D2_VLINE_INT			BIT(2)
#define		DIS_LB_D2_VBLANK_INT			BIT(3)
#define		DIS_HPD2_INT				BIT(17)
#define		DIS_HPD2_RX_INT				BIT(18)
#define DISP3_INT_STATUS			0x6100
#define		DIS_LB_D3_VLINE_INT			BIT(2)
#define		DIS_LB_D3_VBLANK_INT			BIT(3)
#define		DIS_HPD3_INT				BIT(17)
#define		DIS_HPD3_RX_INT				BIT(18)
#define DISP4_INT_STATUS			0x614c
#define		DIS_LB_D4_VLINE_INT			BIT(2)
#define		DIS_LB_D4_VBLANK_INT			BIT(3)
#define		DIS_HPD4_INT				BIT(17)
#define		DIS_HPD4_RX_INT				BIT(18)
#define DISP5_INT_STATUS			0x6150
#define		DIS_LB_D5_VLINE_INT			BIT(2)
#define		DIS_LB_D5_VBLANK_INT			BIT(3)
#define		DIS_HPD5_INT				BIT(17)
#define		DIS_HPD5_RX_INT				BIT(18)

#define GPIO_HPD_MASK				0x65b0
#define GPIO_HPD_A				0x65b4
#define GPIO_HPD_ENA				0x65b8
#define GPIO_HPD_Y				0x65bc

#define	DAC_AUTODETECT_INT_CTL			0x67c8

/*----------------------------------------------------------------------------*/
/* display controller offsets used for crtc/cur/lut/grph/viewport/etc. */
#define CRTC0_REG_OF		(0x6df0 - 0x6df0)
#define CRTC1_REG_OF		(0x79f0 - 0x6df0)
/* many regs here */
#define CRTC2_REG_OF		(0x105f0 - 0x6df0)
#define CRTC3_REG_OF		(0x111f0 - 0x6df0)
#define CRTC4_REG_OF		(0x11df0 - 0x6df0)
#define CRTC5_REG_OF		(0x129f0 - 0x6df0)
/*----------------------------------------------------------------------------*/

/* grph blocks at 0x6800, 0x7400, 0x10000, 0x10c00, 0x11800, 0x12400 */
#define GRPH_ENA				0x6800
#define GRPH_CTL				0x6804
#define		GC_DEPTH				0x00000003
#define			GC_DEPTH_8BPP				0
#define			GC_DEPTH_16BPP				1
#define			GC_DEPTH_32BPP				2
#define		GC_BANKS_N				0x0000000c
#define			GC_ADDR_SURF_2_BANK			0
#define			GC_ADDR_SURF_4_BANK			1
#define			GC_ADDR_SURF_8_BANK			2
#define			GC_ADDR_SURF_16_BANK			3
#define		GC_Z					0x00000030
#define		GC_BANK_W				0x000000c0
#define			GC_ADDR_SURF_BANK_WIDTH_1		0
#define			GC_ADDR_SURF_BANK_WIDTH_2		1
#define			GC_ADDR_SURF_BANK_WIDTH_4		2
#define			GC_ADDR_SURF_BANK_WIDTH_8		3
#define		GC_FMT					0x00000700
			/* 8 bpp */
#define			GC_FMT_INDEXED				0
			/* 16 bpp */
#define			GC_FMT_ARGB1555				0
#define			GC_FMT_ARGB565				1
#define			GC_FMT_ARGB4444				2
#define			GC_FMT_AI88				3
#define			GC_FMT_MONO16				4
#define			GC_FMT_BGRA5551				5
			/* 32 bpp */
#define			GC_FMT_ARGB8888				0
#define			GC_FMT_ARGB2101010			1
#define			GC_FMT_32BPP_DIG			2
#define			GC_FMT_8B_ARGB2101010			3
#define			GC_FMT_BGRA1010102			4
#define			GC_FMT_8B_BGRA1010102			5
#define			GC_FMT_RGB111110			6
#define			GC_FMT_BGR101111			7
#define		GC_BANK_H				0x00001800
#define			GC_ADDR_SURF_BANK_HEIGHT_1		0
#define			GC_ADDR_SURF_BANK_HEIGHT_2		1
#define			GC_ADDR_SURF_BANK_HEIGHT_4		2
#define			GC_ADDR_SURF_BANK_HEIGHT_8		3
#define		GC_TILE_SPLIT				0x0000e000
#define			GC_ADDR_SURF_TILE_SPLIT_64B		0
#define			GC_ADDR_SURF_TILE_SPLIT_128B		1
#define			GC_ADDR_SURF_TILE_SPLIT_256B		2
#define			GC_ADDR_SURF_TILE_SPLIT_512B		3
#define			GC_ADDR_SURF_TILE_SPLIT_1KB		4
#define			GC_ADDR_SURF_TILE_SPLIT_2KB		5
#define			GC_ADDR_SURF_TILE_SPLIT_4KB		6
#define		GC_MACRO_TILE_ASPECT			0x000c0000
#define			GC_ADDR_SURF_MACRO_TILE_ASPECT_1	0
#define			GC_ADDR_SURF_MACRO_TILE_ASPECT_2	1
#define			GC_ADDR_SURF_MACRO_TILE_ASPECT_4	2
#define			GC_ADDR_SURF_MACRO_TILE_ASPECT_8	3
#define		GC_ARRAY_MODE				0x00700000
#define			GC_ARRAY_LINEAR_GENERAL			0
#define			GC_ARRAY_LINEAR_ALIGNED			1
#define			GC_ARRAY_1D_TILED_THIN1			2
#define			GC_ARRAY_2D_TILED_THIN1			4
#define		GC_PIPE_CFG				0x1f000000
#define			GC_ADDR_SURF_P2				0
#define			GC_ADDR_SURF_P4_8x16			4
#define			GC_ADDR_SURF_P4_16x16			5
#define			GC_ADDR_SURF_P4_16x32			6
#define			GC_ADDR_SURF_P4_32x32			7
#define			GC_ADDR_SURF_P8_16x16_8x16		8
#define			GC_ADDR_SURF_P8_16x32_8x16		9
#define			GC_ADDR_SURF_P8_32x32_8x16		10
#define			GC_ADDR_SURF_P8_16x32_16x16		11
#define			GC_ADDR_SURF_P8_32x32_16x16		12
#define			GC_ADDR_SURF_P8_32x32_16x32		13
#define			GC_ADDR_SURF_P8_32x64_32x32		14

#define GRPH_SWAP_CTL				0x680c
#define		GSC_ENDIAN_SWAP			0x00000003
#define			GSC_ENDIAN_NONE			0
#define			GSC_ENDIAN_8IN16		1
#define			GSC_ENDIAN_8IN32		2
#define			GSC_ENDIAN_8IN64		3
#define		GSC_RED_CROSSBAR		0x00000030
#define			GSC_RED_SEL_R			0
#define			GSC_RED_SEL_G			1
#define			GSC_RED_SEL_B			2
#define			GSC_RED_SEL_A			3
#define		GSC_GREEN_CROSSBAR		0x000000c0
#define			GSC_GREEN_SEL_G			0
#define			GSC_GREEN_SEL_B			1
#define			GSC_GREEN_SEL_A			2
#define			GSC_GREEN_SEL_R			3
#define		GSC_BLUE_CROSSBAR		0x00000300
#define			GSC_BLUE_SEL_B			0
#define			GSC_BLUE_SEL_A			1
#define			GSC_BLUE_SEL_R			2
#define			GSC_BLUE_SEL_G			3
#define		GSC_ALPHA_CROSSBAR		0x00000c00
#define			GSC_ALPHA_SEL_A			0
#define			GSC_ALPHA_SEL_R			1
#define			GSC_ALPHA_SEL_G			2
#define			GSC_ALPHA_SEL_B			3
#define GRPH_PRIMARY_SURF_ADDR			0x6810
#define		GPSA_DFQ_ENA				BIT(0)
#define		GPSA_SURF_ADDR_MASK			0xffffff00
#define GRPH_SECONDARY_SURF_ADDR		0x6814
#define		GSSA_DFQ_ENA				BIT(0)
#define		GSSA_SURF_ADDR_MASK			0xffffff00
#define GRPH_PITCH				0x6818
#define GRPH_PRIMARY_SURF_ADDR_HIGH		0x681c
#define GRPH_SECONDARY_SURF_ADDR_HIGH		0x6820
#define GRPH_SURF_OF_X				0x6824
#define GRPH_SURF_OF_Y				0x6828
#define GRPH_X_START				0x682c
#define GRPH_Y_START				0x6830
#define GRPH_X_END				0x6834
#define GRPH_Y_END				0x6838

#define INPUT_GAMMA_CTL				0x6840
#define		IGC_MODE_GRPH				0x00000003
#define		IGC_MODE_OVL				0x00000030
#define			IGC_USE_LUT				0
#define			IGC_BYPASS				1
#define			IGC_SRGB_24				2
#define			IGC_XVYCC_222				3
#define GRPH_UPDATE				0x6844
#define		GU_SURF_UPDATE_PENDING			BIT(2)
#define		GU_UPDATE_LOCK				BIT(16)
#define GRPH_FLIP_CTL				0x6848
#define		GFC_SURF_UPDATE_H_RETRACE_ENA		BIT(0)

/* 0x6858 ... */
#define GRPH_INT_STATUS				0x6858
#define		GIS_PFLIP_INT_OCCURRED			BIT(0)
#define		GIS_PFLIP_INT_CLR			BIT(8)
/* 0x685c ... */
#define	GRPH_INT_CTL				0x685c
#define		GIC_PFLIP_INT_MASK			BIT(0)
#define		GIC_PFLIP_INT_TYPE			BIT(8)
#define DEGAMMA_CTL                             0x6960
#define		DC_MODE_GRPH				0x00000003
#define		DC_MODE_OVL				0x00000030
#define		DC_MODE_ICON				0x00000300
#define		DC_MODE_CURSOR				0x00003000
#define			DC_BYPASS				0
#define			DC_SRGB_24				1
#define			DC_XVYCC_222				2
#define GAMUT_REMAP_CTL				0x6964
#define		GRC_MODE_GRPH				0x00000003
#define		GRC_MODE_OVL				0x00000030
#define			GRC_BYPASS				0
#define			GRC_PROG_COEFF				1
#define			GRC_PROG_SHARED_MATRIXA			2
#define			GRC_PROG_SHARED_MATRIXB			3

#define REGAMMA_CTL				0x6a80
#define		RC_MODE_GRPH				0x00000007
#define		RC_MODE_OVL				0x00000070
#define			RC_BYPASS				0
#define			RC_SRGB_24				1
#define			RC_XVYCC_222				2
#define			RC_PROG_A				3
#define			RC_PROG_B				4

#define GRPH_PRESCALE_CTL			0x68b4
#define 	GPC_PRESCALE_BYPASS			BIT(4)

#define OVL_PRESCALE_CTL                        0x68c4
#define		OPC_PRESCALE_BYPASS			BIT(4)

#define INPUT_CSC_CTL				0x68d4
#define		ICC_MODE_GRPH				0x00000003
#define		ICC_MODE_OVL				0x00000030
#define			ICC_BYPASS				0
#define			ICC_PROG_COEFF				1
#define			ICC_PROG_SHARED_MATRIXA			2

#define OUTPUT_CSC_CTL				0x68f0
#define		OCC_MODE_GRPH				0x00000007
#define		OCC_MODE_OVL				0x00000070
#define			OCC_BYPASS				0
#define			OCC_TV_RGB				1
#define			OCC_YCBCR_601				2
#define			OCC_YCBCR_709				3
#define			OCC_PROG_COEFF				4
#define			OCC_PROG_SHARED_MATRIXB			5

#define GAMMA_UNKNOWN				0x6940

/* 0x69e0 ... */
#define LUT_RW_MODE				0x69e0
#define LUT_RW_IDX				0x69e4
#define LUT_30_COLOR				0x69f0
#define LUT_WR_ENA_MASK				0x69f8
#define LUT_CTL					0x6a00
#define LUT_BLACK_OF_BLUE			0x6a04
#define LUT_BLACK_OF_GREEN			0x6a08
#define LUT_BLACK_OF_RED			0x6a0c
#define LUT_WHITE_OF_BLUE			0x6a10
#define LUT_WHITE_OF_GREEN			0x6a14
#define LUT_WHITE_OF_RED			0x6a18

/* 0x6b04 ... */
#define DESKTOP_HEIGHT				0x6b04
#define VLINE_START_END				0x6b08

/* 0x6b40 ... */
#define INT_MASK				0x6b40
#define		IM_VBLANK_INT_MASK			BIT(0)
#define		IM_VLINE_INT_MASK			BIT(4)

/* 0x6bb8 ... */
#define VLINE_STATUS				0x6bb8
#define		VS_OCCURRED				BIT(0)
#define		VS_ACK					BIT(4)
#define		VS_STAT					BIT(12)
#define		VS_INT					BIT(16)
#define		VS_INT_TYPE				BIT(17)

/* 0x6bbc ... */
#define VBLANK_STATUS				0x6bbc
#define		VS_OCCURRED				BIT(0)
#define		VS_ACK					BIT(4)
#define		VS_STAT					BIT(12)
#define		VS_INT					BIT(16)
#define		VS_INT_TYPE				BIT(17)

/* watermarks */
#define	DPG_PIPE_ARBITRATION_CTL_3		0x6cc8
#define		DPAC_LATENCY_WATERMARK_MASK		0xffff0000
#define	DPG_PIPE_LATENCY_CTL			0x6ccc
#define		DPLC_LATENCY_LOW_WATERMARK_MASK		0xffffffff
#define		DPLC_LATENCY_HIGH_WATERMARK_MASK	0xffff0000

#define VIEWPORT_START				0x6d70
#define VIEWPORT_SZ				0x6d74

/* 0x6e34 ... */
#define CRTC_VBLANK_START_END			0x6e34
/* VSTART is the first line of VBLANK */
#define		CVSE_VSTART				0x00001fff
/* VEND is the first line after VBLANK space ??? */
#define		CVSE_VEND				0x1fff0000

/* 0x6e70 ... */
#define CRTC_CTL				0x6e70
#define		CC_MASTER_ENA				BIT(0)
#define		CC_VBLANK				BIT(1)
#define		CC_DISP_READ_REQ_DIS			BIT(24)
#define CRTC_STATUS_POS				0x6e90
#define		CSP_VPOS				0x00001fff
#define		CSP_HPOS				0x1fff0000
#define CRTC_STATUS_FRAME_CNT			0x6e98
#define CRTC_UPDATE_LOCK			0x6ed4
#define MASTER_UPDATE_MODE			0x6ef8


static u32 regs_crtc_status_pos[CRTCS_N_MAX] __attribute__ ((unused)) = {
	CRTC_STATUS_POS + CRTC0_REG_OF,
	CRTC_STATUS_POS + CRTC1_REG_OF,
	CRTC_STATUS_POS + CRTC2_REG_OF,
	CRTC_STATUS_POS + CRTC3_REG_OF,
	CRTC_STATUS_POS + CRTC4_REG_OF,
	CRTC_STATUS_POS + CRTC5_REG_OF
};

static u32 regs_crtc_vblank_start_end[CRTCS_N_MAX] __attribute__ ((unused)) = {
	CRTC_VBLANK_START_END + CRTC0_REG_OF,
	CRTC_VBLANK_START_END + CRTC1_REG_OF,
	CRTC_VBLANK_START_END + CRTC2_REG_OF,
	CRTC_VBLANK_START_END + CRTC3_REG_OF,
	CRTC_VBLANK_START_END + CRTC4_REG_OF,
	CRTC_VBLANK_START_END + CRTC5_REG_OF
};

static u32 regs_lut_30_color[CRTCS_N_MAX] __attribute__ ((unused)) = {
	LUT_30_COLOR + CRTC0_REG_OF,
	LUT_30_COLOR + CRTC1_REG_OF,
	LUT_30_COLOR + CRTC2_REG_OF,
	LUT_30_COLOR + CRTC3_REG_OF,
	LUT_30_COLOR + CRTC4_REG_OF,
	LUT_30_COLOR + CRTC5_REG_OF
};

static u32 regs_lut_rw_idx[CRTCS_N_MAX] __attribute__ ((unused)) = {
	LUT_RW_IDX + CRTC0_REG_OF,
	LUT_RW_IDX + CRTC1_REG_OF,
	LUT_RW_IDX + CRTC2_REG_OF,
	LUT_RW_IDX + CRTC3_REG_OF,
	LUT_RW_IDX + CRTC4_REG_OF,
	LUT_RW_IDX + CRTC5_REG_OF
};

static u32 regs_lut_write_ena_mask[CRTCS_N_MAX] __attribute__ ((unused)) = {
	LUT_WR_ENA_MASK + CRTC0_REG_OF,
	LUT_WR_ENA_MASK + CRTC1_REG_OF,
	LUT_WR_ENA_MASK + CRTC2_REG_OF,
	LUT_WR_ENA_MASK + CRTC3_REG_OF,
	LUT_WR_ENA_MASK + CRTC4_REG_OF,
	LUT_WR_ENA_MASK + CRTC5_REG_OF
};

static u32 regs_lut_rw_mode[CRTCS_N_MAX] __attribute__ ((unused)) = {
	LUT_RW_MODE + CRTC0_REG_OF,
	LUT_RW_MODE + CRTC1_REG_OF,
	LUT_RW_MODE + CRTC2_REG_OF,
	LUT_RW_MODE + CRTC3_REG_OF,
	LUT_RW_MODE + CRTC4_REG_OF,
	LUT_RW_MODE + CRTC5_REG_OF
};

static u32 regs_lut_white_of_red[CRTCS_N_MAX] __attribute__ ((unused)) = {
	LUT_WHITE_OF_RED + CRTC0_REG_OF,
	LUT_WHITE_OF_RED + CRTC1_REG_OF,
	LUT_WHITE_OF_RED + CRTC2_REG_OF,
	LUT_WHITE_OF_RED + CRTC3_REG_OF,
	LUT_WHITE_OF_RED + CRTC4_REG_OF,
	LUT_WHITE_OF_RED + CRTC5_REG_OF
};

static u32 regs_lut_white_of_green[CRTCS_N_MAX] __attribute__ ((unused)) = {
	LUT_WHITE_OF_GREEN + CRTC0_REG_OF,
	LUT_WHITE_OF_GREEN + CRTC1_REG_OF,
	LUT_WHITE_OF_GREEN + CRTC2_REG_OF,
	LUT_WHITE_OF_GREEN + CRTC3_REG_OF,
	LUT_WHITE_OF_GREEN + CRTC4_REG_OF,
	LUT_WHITE_OF_GREEN + CRTC5_REG_OF
};

static u32 regs_lut_white_of_blue[CRTCS_N_MAX] __attribute__ ((unused)) = {
	LUT_WHITE_OF_BLUE + CRTC0_REG_OF,
	LUT_WHITE_OF_BLUE + CRTC1_REG_OF,
	LUT_WHITE_OF_BLUE + CRTC2_REG_OF,
	LUT_WHITE_OF_BLUE + CRTC3_REG_OF,
	LUT_WHITE_OF_BLUE + CRTC4_REG_OF,
	LUT_WHITE_OF_BLUE + CRTC5_REG_OF
};

static u32 regs_lut_black_of_red[CRTCS_N_MAX] __attribute__ ((unused)) = {
	LUT_BLACK_OF_RED + CRTC0_REG_OF,
	LUT_BLACK_OF_RED + CRTC1_REG_OF,
	LUT_BLACK_OF_RED + CRTC2_REG_OF,
	LUT_BLACK_OF_RED + CRTC3_REG_OF,
	LUT_BLACK_OF_RED + CRTC4_REG_OF,
	LUT_BLACK_OF_RED + CRTC5_REG_OF
};

static u32 regs_lut_black_of_green[CRTCS_N_MAX] __attribute__ ((unused)) = {
	LUT_BLACK_OF_GREEN + CRTC0_REG_OF,
	LUT_BLACK_OF_GREEN + CRTC1_REG_OF,
	LUT_BLACK_OF_GREEN + CRTC2_REG_OF,
	LUT_BLACK_OF_GREEN + CRTC3_REG_OF,
	LUT_BLACK_OF_GREEN + CRTC4_REG_OF,
	LUT_BLACK_OF_GREEN + CRTC5_REG_OF
};

static u32 regs_lut_black_of_blue[CRTCS_N_MAX] __attribute__ ((unused)) = {
	LUT_BLACK_OF_BLUE + CRTC0_REG_OF,
	LUT_BLACK_OF_BLUE + CRTC1_REG_OF,
	LUT_BLACK_OF_BLUE + CRTC2_REG_OF,
	LUT_BLACK_OF_BLUE + CRTC3_REG_OF,
	LUT_BLACK_OF_BLUE + CRTC4_REG_OF,
	LUT_BLACK_OF_BLUE + CRTC5_REG_OF
};

static u32 regs_lut_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	LUT_CTL + CRTC0_REG_OF,
	LUT_CTL + CRTC1_REG_OF,
	LUT_CTL + CRTC2_REG_OF,
	LUT_CTL + CRTC3_REG_OF,
	LUT_CTL + CRTC4_REG_OF,
	LUT_CTL + CRTC5_REG_OF
};

static u32 regs_master_update_mode[CRTCS_N_MAX] __attribute__ ((unused)) = {
	MASTER_UPDATE_MODE + CRTC0_REG_OF,
	MASTER_UPDATE_MODE + CRTC1_REG_OF,
	MASTER_UPDATE_MODE + CRTC2_REG_OF,
	MASTER_UPDATE_MODE + CRTC3_REG_OF,
	MASTER_UPDATE_MODE + CRTC4_REG_OF,
	MASTER_UPDATE_MODE + CRTC5_REG_OF
};

static u32 regs_grph_flip_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_FLIP_CTL + CRTC0_REG_OF,
	GRPH_FLIP_CTL + CRTC1_REG_OF,
	GRPH_FLIP_CTL + CRTC2_REG_OF,
	GRPH_FLIP_CTL + CRTC3_REG_OF,
	GRPH_FLIP_CTL + CRTC4_REG_OF,
	GRPH_FLIP_CTL + CRTC5_REG_OF
};

static u32 regs_viewport_sz[CRTCS_N_MAX] __attribute__ ((unused)) = {
	VIEWPORT_SZ + CRTC0_REG_OF,
	VIEWPORT_SZ + CRTC1_REG_OF,
	VIEWPORT_SZ + CRTC2_REG_OF,
	VIEWPORT_SZ + CRTC3_REG_OF,
	VIEWPORT_SZ + CRTC4_REG_OF,
	VIEWPORT_SZ + CRTC5_REG_OF
};

static u32 regs_viewport_start[CRTCS_N_MAX] __attribute__ ((unused)) = {
	VIEWPORT_START + CRTC0_REG_OF,
	VIEWPORT_START + CRTC1_REG_OF,
	VIEWPORT_START + CRTC2_REG_OF,
	VIEWPORT_START + CRTC3_REG_OF,
	VIEWPORT_START + CRTC4_REG_OF,
	VIEWPORT_START + CRTC5_REG_OF
};

static u32 regs_desktop_height[CRTCS_N_MAX] __attribute__ ((unused)) = {
	DESKTOP_HEIGHT + CRTC0_REG_OF,
	DESKTOP_HEIGHT + CRTC1_REG_OF,
	DESKTOP_HEIGHT + CRTC2_REG_OF,
	DESKTOP_HEIGHT + CRTC3_REG_OF,
	DESKTOP_HEIGHT + CRTC4_REG_OF,
	DESKTOP_HEIGHT + CRTC5_REG_OF
};

static u32 regs_grph_ena[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_ENA + CRTC0_REG_OF,
	GRPH_ENA + CRTC1_REG_OF,
	GRPH_ENA + CRTC2_REG_OF,
	GRPH_ENA + CRTC3_REG_OF,
	GRPH_ENA + CRTC4_REG_OF,
	GRPH_ENA + CRTC5_REG_OF
};

static u32 regs_grph_pitch[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_PITCH + CRTC0_REG_OF,
	GRPH_PITCH + CRTC1_REG_OF,
	GRPH_PITCH + CRTC2_REG_OF,
	GRPH_PITCH + CRTC3_REG_OF,
	GRPH_PITCH + CRTC4_REG_OF,
	GRPH_PITCH + CRTC5_REG_OF
};

static u32 regs_grph_y_end[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_Y_END + CRTC0_REG_OF,
	GRPH_Y_END + CRTC1_REG_OF,
	GRPH_Y_END + CRTC2_REG_OF,
	GRPH_Y_END + CRTC3_REG_OF,
	GRPH_Y_END + CRTC4_REG_OF,
	GRPH_Y_END + CRTC5_REG_OF
};

static u32 regs_grph_x_end[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_X_END + CRTC0_REG_OF,
	GRPH_X_END + CRTC1_REG_OF,
	GRPH_X_END + CRTC2_REG_OF,
	GRPH_X_END + CRTC3_REG_OF,
	GRPH_X_END + CRTC4_REG_OF,
	GRPH_X_END + CRTC5_REG_OF
};

static u32 regs_grph_y_start[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_Y_START + CRTC0_REG_OF,
	GRPH_Y_START + CRTC1_REG_OF,
	GRPH_Y_START + CRTC2_REG_OF,
	GRPH_Y_START + CRTC3_REG_OF,
	GRPH_Y_START + CRTC4_REG_OF,
	GRPH_Y_START + CRTC5_REG_OF
};

static u32 regs_grph_x_start[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_X_START + CRTC0_REG_OF,
	GRPH_X_START + CRTC1_REG_OF,
	GRPH_X_START + CRTC2_REG_OF,
	GRPH_X_START + CRTC3_REG_OF,
	GRPH_X_START + CRTC4_REG_OF,
	GRPH_X_START + CRTC5_REG_OF
};

static u32 regs_grph_surf_of_y[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_SURF_OF_Y + CRTC0_REG_OF,
	GRPH_SURF_OF_Y + CRTC1_REG_OF,
	GRPH_SURF_OF_Y + CRTC2_REG_OF,
	GRPH_SURF_OF_Y + CRTC3_REG_OF,
	GRPH_SURF_OF_Y + CRTC4_REG_OF,
	GRPH_SURF_OF_Y + CRTC5_REG_OF
};

static u32 regs_grph_surf_of_x[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_SURF_OF_X + CRTC0_REG_OF,
	GRPH_SURF_OF_X + CRTC1_REG_OF,
	GRPH_SURF_OF_X + CRTC2_REG_OF,
	GRPH_SURF_OF_X + CRTC3_REG_OF,
	GRPH_SURF_OF_X + CRTC4_REG_OF,
	GRPH_SURF_OF_X + CRTC5_REG_OF
};

static u32 regs_grph_swap_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_SWAP_CTL + CRTC0_REG_OF,
	GRPH_SWAP_CTL + CRTC1_REG_OF,
	GRPH_SWAP_CTL + CRTC2_REG_OF,
	GRPH_SWAP_CTL + CRTC3_REG_OF,
	GRPH_SWAP_CTL + CRTC4_REG_OF,
	GRPH_SWAP_CTL + CRTC5_REG_OF
};

static u32 regs_grph_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_CTL + CRTC0_REG_OF,
	GRPH_CTL + CRTC1_REG_OF,
	GRPH_CTL + CRTC2_REG_OF,
	GRPH_CTL + CRTC3_REG_OF,
	GRPH_CTL + CRTC4_REG_OF,
	GRPH_CTL + CRTC5_REG_OF
};

static u32 regs_grph_update[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_UPDATE + CRTC0_REG_OF,
	GRPH_UPDATE + CRTC1_REG_OF,
	GRPH_UPDATE + CRTC2_REG_OF,
	GRPH_UPDATE + CRTC3_REG_OF,
	GRPH_UPDATE + CRTC4_REG_OF,
	GRPH_UPDATE + CRTC5_REG_OF
};

static u32 regs_crtc_status_frame_cnt[CRTCS_N_MAX]
						__attribute__ ((unused)) = {
	CRTC_STATUS_FRAME_CNT + CRTC0_REG_OF,
	CRTC_STATUS_FRAME_CNT + CRTC1_REG_OF,
	CRTC_STATUS_FRAME_CNT + CRTC2_REG_OF,
	CRTC_STATUS_FRAME_CNT + CRTC3_REG_OF,
	CRTC_STATUS_FRAME_CNT + CRTC4_REG_OF,
	CRTC_STATUS_FRAME_CNT + CRTC5_REG_OF
};

static u32 regs_crtc_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	CRTC_CTL + CRTC0_REG_OF,
	CRTC_CTL + CRTC1_REG_OF,
	CRTC_CTL + CRTC2_REG_OF,
	CRTC_CTL + CRTC3_REG_OF,
	CRTC_CTL + CRTC4_REG_OF,
	CRTC_CTL + CRTC5_REG_OF
};

static u32 regs_grph_primary_surf_addr_high[CRTCS_N_MAX]
						__attribute__ ((unused)) = {
	GRPH_PRIMARY_SURF_ADDR_HIGH + CRTC0_REG_OF,
	GRPH_PRIMARY_SURF_ADDR_HIGH + CRTC1_REG_OF,
	GRPH_PRIMARY_SURF_ADDR_HIGH + CRTC2_REG_OF,
	GRPH_PRIMARY_SURF_ADDR_HIGH + CRTC3_REG_OF,
	GRPH_PRIMARY_SURF_ADDR_HIGH + CRTC4_REG_OF,
	GRPH_PRIMARY_SURF_ADDR_HIGH + CRTC5_REG_OF
};

static u32 regs_grph_secondary_surf_addr_high[CRTCS_N_MAX]
						__attribute__ ((unused)) = {
	GRPH_SECONDARY_SURF_ADDR_HIGH + CRTC0_REG_OF,
	GRPH_SECONDARY_SURF_ADDR_HIGH + CRTC1_REG_OF,
	GRPH_SECONDARY_SURF_ADDR_HIGH + CRTC2_REG_OF,
	GRPH_SECONDARY_SURF_ADDR_HIGH + CRTC3_REG_OF,
	GRPH_SECONDARY_SURF_ADDR_HIGH + CRTC4_REG_OF,
	GRPH_SECONDARY_SURF_ADDR_HIGH + CRTC5_REG_OF
};

static u32 regs_grph_primary_surf_addr[CRTCS_N_MAX]
						__attribute__ ((unused)) = {
	GRPH_PRIMARY_SURF_ADDR + CRTC0_REG_OF,
	GRPH_PRIMARY_SURF_ADDR + CRTC1_REG_OF,
	GRPH_PRIMARY_SURF_ADDR + CRTC2_REG_OF,
	GRPH_PRIMARY_SURF_ADDR + CRTC3_REG_OF,
	GRPH_PRIMARY_SURF_ADDR + CRTC4_REG_OF,
	GRPH_PRIMARY_SURF_ADDR + CRTC5_REG_OF
};

static u32 regs_grph_secondary_surf_addr[CRTCS_N_MAX]
						__attribute__ ((unused)) = {
	GRPH_SECONDARY_SURF_ADDR + CRTC0_REG_OF,
	GRPH_SECONDARY_SURF_ADDR + CRTC1_REG_OF,
	GRPH_SECONDARY_SURF_ADDR + CRTC2_REG_OF,
	GRPH_SECONDARY_SURF_ADDR + CRTC3_REG_OF,
	GRPH_SECONDARY_SURF_ADDR + CRTC4_REG_OF,
	GRPH_SECONDARY_SURF_ADDR + CRTC5_REG_OF
};

static u32 regs_hpd_int_status[CRTCS_N_MAX] __attribute__ ((unused)) = {
	HPD0_INT_STATUS,
	HPD1_INT_STATUS,
	HPD2_INT_STATUS,
	HPD3_INT_STATUS,
	HPD4_INT_STATUS,
	HPD5_INT_STATUS
};

static u32 regs_hpd_int_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	HPD0_INT_CTL,
	HPD1_INT_CTL,
	HPD2_INT_CTL,
	HPD3_INT_CTL,
	HPD4_INT_CTL,
	HPD5_INT_CTL
};

static u32 regs_hpd_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	HPD0_CTL,
	HPD1_CTL,
	HPD2_CTL,
	HPD3_CTL,
	HPD4_CTL,
	HPD5_CTL
};

static u32 regs_crtc_int_mask[CRTCS_N_MAX] __attribute__ ((unused)) = {
	INT_MASK + CRTC0_REG_OF,
	INT_MASK + CRTC1_REG_OF,
	INT_MASK + CRTC2_REG_OF,
	INT_MASK + CRTC3_REG_OF,
	INT_MASK + CRTC4_REG_OF,
	INT_MASK + CRTC5_REG_OF
};

static u32 regs_crtc_grph_int_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_INT_CTL + CRTC0_REG_OF,
	GRPH_INT_CTL + CRTC1_REG_OF,
	GRPH_INT_CTL + CRTC2_REG_OF,
	GRPH_INT_CTL + CRTC3_REG_OF,
	GRPH_INT_CTL + CRTC4_REG_OF,
	GRPH_INT_CTL + CRTC5_REG_OF
};

static u32 regs_disp_int_status[CRTCS_N_MAX] __attribute__ ((unused)) = {
	DISP0_INT_STATUS,
	DISP1_INT_STATUS,
	DISP2_INT_STATUS,
	DISP3_INT_STATUS,
	DISP4_INT_STATUS,
	DISP5_INT_STATUS
};

static u32 regs_crtc_grph_int_status[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_INT_STATUS + CRTC0_REG_OF,
	GRPH_INT_STATUS + CRTC1_REG_OF,
	GRPH_INT_STATUS + CRTC2_REG_OF,
	GRPH_INT_STATUS + CRTC3_REG_OF,
	GRPH_INT_STATUS + CRTC4_REG_OF,
	GRPH_INT_STATUS + CRTC5_REG_OF
};

static u32 regs_crtc_vblank_status[CRTCS_N_MAX] __attribute__ ((unused)) = {
	VBLANK_STATUS + CRTC0_REG_OF,
	VBLANK_STATUS + CRTC1_REG_OF,
	VBLANK_STATUS + CRTC2_REG_OF,
	VBLANK_STATUS + CRTC3_REG_OF,
	VBLANK_STATUS + CRTC4_REG_OF,
	VBLANK_STATUS + CRTC5_REG_OF
};

static u32 regs_crtc_vline_status[CRTCS_N_MAX] __attribute__ ((unused)) = {
	VLINE_STATUS + CRTC0_REG_OF,
	VLINE_STATUS + CRTC1_REG_OF,
	VLINE_STATUS + CRTC2_REG_OF,
	VLINE_STATUS + CRTC3_REG_OF,
	VLINE_STATUS + CRTC4_REG_OF,
	VLINE_STATUS + CRTC5_REG_OF
};

static u32 regs_crtc_update_lock[CRTCS_N_MAX] __attribute__ ((unused)) = {
	CRTC_UPDATE_LOCK + CRTC0_REG_OF,
	CRTC_UPDATE_LOCK + CRTC1_REG_OF,
	CRTC_UPDATE_LOCK + CRTC2_REG_OF,
	CRTC_UPDATE_LOCK + CRTC3_REG_OF,
	CRTC_UPDATE_LOCK + CRTC4_REG_OF,
	CRTC_UPDATE_LOCK + CRTC5_REG_OF
};

static u32 vals_hpd_int[CRTCS_N_MAX] __attribute__ ((unused)) = {
	DIS_HPD0_INT,
	DIS_HPD1_INT,
	DIS_HPD2_INT,
	DIS_HPD3_INT,
	DIS_HPD4_INT,
	DIS_HPD5_INT
};

static u32 vals_lb_d_vblank_int[CRTCS_N_MAX] __attribute__ ((unused)) = {
	DIS_LB_D0_VBLANK_INT,
	DIS_LB_D1_VBLANK_INT,
	DIS_LB_D2_VBLANK_INT,
	DIS_LB_D3_VBLANK_INT,
	DIS_LB_D4_VBLANK_INT,
	DIS_LB_D5_VBLANK_INT
};

static u32 vals_lb_d_vline_int[CRTCS_N_MAX] __attribute__ ((unused)) = {
	DIS_LB_D0_VLINE_INT,
	DIS_LB_D1_VLINE_INT,
	DIS_LB_D2_VLINE_INT,
	DIS_LB_D3_VLINE_INT,
	DIS_LB_D4_VLINE_INT,
	DIS_LB_D5_VLINE_INT
};

static u32 regs_vga_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	D0VGA_CTL,
	D1VGA_CTL,
	D2VGA_CTL,
	D3VGA_CTL,
	D4VGA_CTL,
	D5VGA_CTL
};

static u32 regs_input_csc_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	INPUT_CSC_CTL + CRTC0_REG_OF,
	INPUT_CSC_CTL + CRTC1_REG_OF,
	INPUT_CSC_CTL + CRTC2_REG_OF,
	INPUT_CSC_CTL + CRTC3_REG_OF,
	INPUT_CSC_CTL + CRTC4_REG_OF,
	INPUT_CSC_CTL + CRTC5_REG_OF
};

static u32 regs_grph_prescale_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GRPH_PRESCALE_CTL + CRTC0_REG_OF,
	GRPH_PRESCALE_CTL + CRTC1_REG_OF,
	GRPH_PRESCALE_CTL + CRTC2_REG_OF,
	GRPH_PRESCALE_CTL + CRTC3_REG_OF,
	GRPH_PRESCALE_CTL + CRTC4_REG_OF,
	GRPH_PRESCALE_CTL + CRTC5_REG_OF
};

static u32 regs_ovl_prescale_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	OVL_PRESCALE_CTL + CRTC0_REG_OF,
	OVL_PRESCALE_CTL + CRTC1_REG_OF,
	OVL_PRESCALE_CTL + CRTC2_REG_OF,
	OVL_PRESCALE_CTL + CRTC3_REG_OF,
	OVL_PRESCALE_CTL + CRTC4_REG_OF,
	OVL_PRESCALE_CTL + CRTC5_REG_OF
};

static u32 regs_input_gamma_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	INPUT_GAMMA_CTL + CRTC0_REG_OF,
	INPUT_GAMMA_CTL + CRTC1_REG_OF,
	INPUT_GAMMA_CTL + CRTC2_REG_OF,
	INPUT_GAMMA_CTL + CRTC3_REG_OF,
	INPUT_GAMMA_CTL + CRTC4_REG_OF,
	INPUT_GAMMA_CTL + CRTC5_REG_OF
};

static u32 regs_degamma_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	DEGAMMA_CTL + CRTC0_REG_OF,
	DEGAMMA_CTL + CRTC1_REG_OF,
	DEGAMMA_CTL + CRTC2_REG_OF,
	DEGAMMA_CTL + CRTC3_REG_OF,
	DEGAMMA_CTL + CRTC4_REG_OF,
	DEGAMMA_CTL + CRTC5_REG_OF
};

static u32 regs_gamut_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GAMUT_REMAP_CTL + CRTC0_REG_OF,
	GAMUT_REMAP_CTL + CRTC1_REG_OF,
	GAMUT_REMAP_CTL + CRTC2_REG_OF,
	GAMUT_REMAP_CTL + CRTC3_REG_OF,
	GAMUT_REMAP_CTL + CRTC4_REG_OF,
	GAMUT_REMAP_CTL + CRTC5_REG_OF
};

static u32 regs_regamma_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	REGAMMA_CTL + CRTC0_REG_OF,
	REGAMMA_CTL + CRTC1_REG_OF,
	REGAMMA_CTL + CRTC2_REG_OF,
	REGAMMA_CTL + CRTC3_REG_OF,
	REGAMMA_CTL + CRTC4_REG_OF,
	REGAMMA_CTL + CRTC5_REG_OF
};

static u32 regs_output_csc_ctl[CRTCS_N_MAX] __attribute__ ((unused)) = {
	OUTPUT_CSC_CTL + CRTC0_REG_OF,
	OUTPUT_CSC_CTL + CRTC1_REG_OF,
	OUTPUT_CSC_CTL + CRTC2_REG_OF,
	OUTPUT_CSC_CTL + CRTC3_REG_OF,
	OUTPUT_CSC_CTL + CRTC4_REG_OF,
	OUTPUT_CSC_CTL + CRTC5_REG_OF
};

static u32 regs_gamma_unknown[CRTCS_N_MAX] __attribute__ ((unused)) = {
	GAMMA_UNKNOWN + CRTC0_REG_OF,
	GAMMA_UNKNOWN + CRTC1_REG_OF,
	GAMMA_UNKNOWN + CRTC2_REG_OF,
	GAMMA_UNKNOWN + CRTC3_REG_OF,
	GAMMA_UNKNOWN + CRTC4_REG_OF,
	GAMMA_UNKNOWN + CRTC5_REG_OF
};
#endif
