#ifndef ALGA_AMD_DCE6_SINK_H
#define ALGA_AMD_DCE6_SINK_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
struct sink_db_fb {
	u64 primary;
	u64 secondary;
	u32 pitch;
	u8 pixel_fmt;
	struct alga_timing *timing;
};

void sink_pixel_fmts(struct dce6 *dce, u8 i, u8 (*fmts)[ALGA_PIXEL_FMTS_MAX]);
long sink_mode_set(struct dce6 *dce, u8 i, struct sink_db_fb *db_fb);
#endif
