#ifndef ALGA_AMD_DCE6_I2C_H
#define ALGA_AMD_DCE6_I2C_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long dp_i2c_adapter_init(struct dce6 *dce, u8 i);
void dp_i2c_cleanup(struct dce6 *dce);
#endif
