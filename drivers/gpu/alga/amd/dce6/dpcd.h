#ifndef DPCD_H
#define DPCD_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long dpcd_wr(struct dce6 *dce, u8 i, u16 addr, u8 byte);
long dpcd_lanes_vs_pre_emph_wr(struct dce6 *dce, u8 i, u8 vs_pre_emph);
long dpcd_link_status(struct dce6 *dce, u8 i, u8 *link_status);
long dpcd_info(struct dce6 * dce, u8 i);
#endif
