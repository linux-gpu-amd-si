#ifndef CRTC_H
#define CRTC_H
/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
long crtc_fb(struct dce6 *dce, u8 i, struct sink_db_fb *db_fb);
void crtc_lut(struct dce6 *dce, u8 i);
void crtcs_intr_ena(struct dce6 *dce);
void crtcs_intr_reset(struct dce6 *dce);
void crtcs_atb_states_init(struct dce6 *dce);
#endif
