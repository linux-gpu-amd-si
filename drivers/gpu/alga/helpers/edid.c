/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#include <linux/module.h>
#include <linux/device.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <asm/byteorder.h>
#include <asm/unaligned.h>
#include <asm/uaccess.h>

#include <alga/alga.h>
#include <alga/timing.h>
#include <alga/edid.h>

#include "vesa_dmts.h"

/* NOTE: do not even try to understand the code without the EDID specs */

#define I2C_FETCH_RETRIES 4

static u8 has_valid_hdr(void *edid)
{
	static u8 valid_hdr[8] = {
		0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00};
	if (memcmp(edid, valid_hdr, sizeof(valid_hdr)) != 0)
		return 0;
	return 1;
}

static u8 is_v1_4(void *edid)
{
	u8 version;
	u8 revision;

	version = *(u8*)(edid + 0x12);
	if (version != 1)
		return 0;

	revision = *(u8*)(edid + 0x13);
	if (revision != 4)
		return 0;
	return 1;
}

static u8 v_refresh(struct alga_timing *t)
{
	u64 line_pixels;
	u64 frame_lines;
	u64 frame_pixels;

	line_pixels = t->h + t->h_bl;
	frame_lines = t->v + t->v_bl;
	frame_pixels = frame_lines * line_pixels;
	return (u8)DIV_ROUND_CLOSEST(t->pixel_clk * 1000, frame_pixels);
}

static void dtd_collect(void *dtd, struct alga_timing (*ts)[ALGA_TIMINGS_MAX])
{
	struct alga_timing *t;
	u8 *p;
	u32 tmp;
	u8 i;

	/* get to the first available slot of timings */
	for (i = 0; i < ALGA_TIMINGS_MAX; ++i)
		if ((*ts)[i].pixel_clk == 0)
			break;

	if (i < ALGA_TIMINGS_MAX)
		t = &(*ts)[i];
	else
		return; /* no room */

	p = (u8*)dtd;

	if ((p[17] & 0x80) != 0) /* do not support interlaced modes */
		return;
	
	t->pixel_clk = get_unaligned_le16(dtd) * 10;

	tmp = p[2];
	tmp |= (p[4] & 0xf0) << 4;
	t->h = tmp;

	tmp = p[3];
	tmp |= (p[4] & 0x0f) << 8;
	t->h_bl = tmp;

	tmp = p[5];
	tmp |= (p[7] & 0xf0) << 4;
	t->v = tmp;

	tmp = p[6];
	tmp |= (p[7] & 0x0f) << 8;
	t->v_bl = tmp;

	/* tmp is front porch from blanking */
	tmp = p[8];
	tmp |= (p[11] & 0xc0) << 2;
	t->h_so = t->h + tmp;

	tmp = p[9];
	tmp |= (p[11] & 0x30) << 4;
	t->h_spw = tmp;

	/* tmp is front porch from blanking */
	tmp = (p[10] & 0xf0) >> 4;
	tmp |= (p[11] & 0x0c) << 2;
	t->v_so = t->v + tmp;

	tmp = p[10] & 0x0f; 
	tmp |= (p[11] & 0x03) << 4;
	t->v_spw = tmp;

	/* only digital separated sync supported */
	t->h_sp = 0;
	t->v_sp = 0;
	if ((p[17] & 0x0e) == 0x0c)
		t->v_sp = 1;
	if ((p[17] & 0x09) == 0x08)
		t->h_sp = 1;

	snprintf(&t->mode[0], sizeof(t->mode), "%ux%u@%u", t->h, t->v, v_refresh(t));
	t->mode[sizeof(t->mode) - 1] = '\0';
}

static void dtds_collect(void *edid, struct alga_timing (*ts)[ALGA_TIMINGS_MAX])
{
	u8 i;
	void *dtd;

	for (i = 0, dtd = edid + 0x36; i < 4; ++i, dtd += 18) {
		u16 pixel_clk;

		pixel_clk = get_unaligned_le16(dtd);
		if (pixel_clk != 0)
			dtd_collect(dtd, ts);
	}
}

static void timings(void *edid, struct alga_timing (*ts)[ALGA_TIMINGS_MAX])
{
	dtds_collect(edid, ts);
	/* TODO: collect the timings defined in other ways */
}

long alga_edid_timings(struct device *dev, void *edid,
				struct alga_timing (*ts)[ALGA_TIMINGS_MAX])
{
	if (!is_v1_4(edid)) {
		dev_err(dev, "alga: edid version not supported\n");
		return -ALGA_ERR;
	}

	memset(ts, 0, sizeof(*ts));
	timings(edid, ts);
	return 0;
}
EXPORT_SYMBOL_GPL(alga_edid_timings);

/*
 * XXX:for now do not deal with the e-edid extension blocks
 * must not support edid bigger than PAGE_SIZE - 1 for sysfs attribute
 */
long alga_i2c_edid_fetch(struct device *dev, struct i2c_adapter *a, void **edid)
{
	u8 out;
	long r;
	u8 retry;
	struct i2c_msg msgs[] = {
		{
			.addr = 0x50,
			.flags = 0,
			.len = 1,
			.buf = &out
		},
		{
			.addr = 0x50,                                                                                                                                                             
			.flags = I2C_M_RD,                                                                                                                                                        
			.len = EDID_BLK_SZ,
			.buf = NULL
		}
	};

	*edid = kzalloc(EDID_BLK_SZ, GFP_KERNEL);
	if (*edid == NULL) {
		dev_err(dev, "alga: unable to allocate memory for EDID\n");
		goto err;
	}

	msgs[1].buf = *edid;
	out = 0; /* block 0 */

	for (retry = 0; retry < I2C_FETCH_RETRIES; ++retry) {
		r = i2c_transfer(a, msgs, 2);                                                                                                                 
		if (r == 2)
			break;
	}
	if (r != 2) {
		dev_err(dev, "alga: unable to fetch the edid over i2c\n");
		goto err_free_edid;
	}

	if (!has_valid_hdr(*edid)) {
		dev_err(dev, "alga: invalid edid header\n");
		goto err_free_edid;
	}
	return EDID_BLK_SZ;

err_free_edid:
	kfree(*edid);
	*edid = NULL;	
err:
	return -ALGA_ERR;
}
EXPORT_SYMBOL_GPL(alga_i2c_edid_fetch);

u8 alga_edid_bpc(struct device *dev, void *edid)
{
	u8 def;

	def = *(u8*)(edid + 0x14);

	if ((def & BIT(7)) == 0) {
		dev_warn(dev, "alga: edid video input definition is not "
								"digital\n");
		return 0;
	}

	def &= 0x70;

	switch (def) {
	case 0x10:
		return 6;
	case 0x20:
		return 8;
	case 0x30:
		return 10;
	case 0x40:
		return 12;
	case 0x50:
		return 14;
	case 0x60:
		return 16;
	case 0x70:
	case 0x00:
	default:
		return 0;
	}
}
EXPORT_SYMBOL_GPL(alga_edid_bpc);

u16 alga_edid_manufacturer(struct device *dev, void *edid)
{
	return be16_to_cpup((__be16 *)(edid + 0x08));
}
EXPORT_SYMBOL_GPL(alga_edid_manufacturer);

u16 alga_edid_product_code(struct device *dev, void *edid)
{
	return le16_to_cpup((__le16 *)(edid + 0x0a));
}
EXPORT_SYMBOL_GPL(alga_edid_product_code);

void alga_edid_monitor_name(struct device *dev, void *edid, u8 *name)
{
	u8 descriptor;
	u8 *c;
	u8 len;

	name[0] = 0;

	for (descriptor = 0; descriptor < 3; ++descriptor) {
		u8 *tag;

		tag = (u8 *)(edid + 0x36 + 18 * descriptor);

		if (tag[0] == 0x00 && tag[1] == 0x00 && tag[2] == 0x00
					&& tag[3] == 0xfc && tag[4] == 0x00)
			break;
	}
	if (descriptor == 3)
		return;

	c = (u8 *)(edid + 0x36 + 18 * descriptor + 5);
	len = 0;
	while ((*c != 0x0a) && (len <= (EDID_DESCRIPTOR_STR_SZ_MAX - 1))) {
		*name = *c;

		++name;
		++c;
		++len;
	}
	*name = 0;
}
EXPORT_SYMBOL_GPL(alga_edid_monitor_name);

void alga_edid_monitor_serial(struct device *dev, void *edid, u8 *serial)
{
	u8 descriptor;
	u8 *c;
	u8 len;

	serial[0] = 0;

	for (descriptor = 0; descriptor < 3; ++descriptor) {
		u8 *tag;

		tag =(u8 *)(edid + 0x36 + 18 * descriptor);

		if (tag[0] == 0x00 && tag[1] == 0x00 && tag[2] == 0x00
					&& tag[3] == 0xff && tag[4] == 0x00)
			break;
	}
	if (descriptor == 3) 
		return;

	c = (u8 *)(edid + 0x36 + 18 * descriptor + 5);
	len = 0;
	while ((*c != 0x0a) && (len <= (EDID_DESCRIPTOR_STR_SZ_MAX - 1))) {
		*serial = *c;

		++serial;
		++c;
		++len;
	}
	*serial = 0;
}
EXPORT_SYMBOL_GPL(alga_edid_monitor_serial);
