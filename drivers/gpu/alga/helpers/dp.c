/*
  author Sylvain Bertrand <sylvain.bertrand@gmail.com>
  Protected by linux GNU GPLv2
  Copyright 2012-2014
*/
#include <linux/module.h>
#include <linux/slab.h>

#include <alga/alga.h>
#include <alga/timing.h>
void alga_dp_safe_timing(struct alga_timing (*ts)[ALGA_TIMINGS_MAX])
{
	/* init the array, namely one mode and one terminator */
	memset(ts, 0, sizeof(*ts));

	/* timings from the VESA safe mode standard */
	(*ts)[0].h = 640;
	(*ts)[0].h_bl = 160; 
	(*ts)[0].h_so = 656;
	(*ts)[0].h_spw = 96;
	(*ts)[0].h_sp = 1;

	(*ts)[0].v = 480;
	(*ts)[0].v_bl = 45; 
	(*ts)[0].v_so = 490;
	(*ts)[0].v_spw = 2;
	(*ts)[0].v_sp = 1;

	(*ts)[0].pixel_clk = 25175;
}
EXPORT_SYMBOL_GPL(alga_dp_safe_timing);
